
package main

import (
    "os"
    "fmt"
    "flag"
    //"runtime"
    "strings"
    "errors"
    "time"
    "math/rand"
    "gitlab.com/galberti/dstarx/confbox"
    "gitlab.com/galberti/dstarx/satoshi"
    "gitlab.com/galberti/dstarx/dvrptrv2"
    "gitlab.com/galberti/dstarx/dv4mini"
    "gitlab.com/galberti/dstarx/dsnetwork"
    "gitlab.com/galberti/dstarx/dstar"
)

const (
    TYPE_UNKNOWN = iota
    TYPE_SATOSHI
    TYPE_DVRPTRV2
    TYPE_DV4MINI
    TYPE_DSNDEV
    TYPE_DEXTRA
    TYPE_DCS
)

var Cb confbox.ConfBox
var sat *satoshi.SatoshiCtx
var dvr2 *dvrptrv2.DvrPtrV2Ctx
var dv4 *dv4mini.Dv4miniCtx
var dsn *dsnetwork.DSNDevCtx
var ex *dstar.DExtra
var dcs *dstar.DCS
var netw *Network
var logfile *os.File
var tformat = "20060102-15:04:05 "

type Network struct {
    RouteTable map[string]byte
    Main map[string]string
    Links [][]string
    lastCommandTime *time.Time
}

func isIn(arr []string, str string) bool {
    for _, e := range arr {
        if e == str {
            return true
        }
    }
    return false
}

func setupNetwork(erc chan<- error) *Network {
    network := Network{
        RouteTable: make(map[string]byte),
        Main: make(map[string]string),
    }
    call := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,dstar.DEXTRA_FROM_LETTER)
    network.RouteTable[call] = TYPE_DEXTRA
    call = fmt.Sprintf("%-7s%s",Cb.ModulesOwner,dstar.DCS_FROM_LETTER)
    network.RouteTable[call] = TYPE_DCS

    if sat != nil {
        sm := sat.GetModules()
        for _, m := range sm {
            call = fmt.Sprintf("%-7s%s",Cb.ModulesOwner,m)
            network.RouteTable[call] = TYPE_SATOSHI
        }
    }

    if dvr2 != nil {
        dm := dvr2.GetModules()
        for _, m := range dm {
            call = fmt.Sprintf("%-7s%s",Cb.ModulesOwner,m)
            network.RouteTable[call] = TYPE_DVRPTRV2
        }
    }

    if dv4 != nil {
        dm := dv4.GetModules()
        for _, m := range dm {
            call = fmt.Sprintf("%-7s%s",Cb.ModulesOwner,m)
            network.RouteTable[call] = TYPE_DV4MINI
        }
    }

    if dsn != nil {
        dm := dsn.GetModules()
        for _, m := range dm {
            call = fmt.Sprintf("%-7s%s",Cb.ModulesOwner,m)
            network.RouteTable[call] = TYPE_DSNDEV
        }
    }

    if (sat != nil) && (Cb.LocalMains != "") {
        ms := strings.Split(Cb.LocalMains,",")
        for _, cp := range ms {
            s := strings.Split(cp,":")
            call := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,s[1])
            network.Main[s[0]] = call
            if s[1] == dstar.DEXTRA_FROM_LETTER {
                sat.SetMain(s[0],dstar.DEXTRA_MAIN_LETTER)
            } else if s[1] == dstar.DCS_FROM_LETTER {
                sat.SetMain(s[0],dstar.DCS_MAIN_LETTER)
            } else {
                sat.SetMain(s[0],s[1])
            }
        }
    }

    if (dvr2 != nil) && (Cb.LocalMains != "") {
        ms := strings.Split(Cb.LocalMains, ",")
        for _, cp := range ms {
            s := strings.Split(cp,":")
            call := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,s[1])
            network.Main[s[0]] = call
            if s[1] == dstar.DEXTRA_FROM_LETTER {
                dvr2.SetMain(s[0],dstar.DEXTRA_MAIN_LETTER)
            } else if s[1] == dstar.DCS_FROM_LETTER {
                dvr2.SetMain(s[0],dstar.DCS_MAIN_LETTER)
            } else {
                dvr2.SetMain(s[0],s[1])
            }
        }
    }

    if (dv4 != nil) && (Cb.LocalMains != "") {
        ms := strings.Split(Cb.LocalMains, ",")
        for _, cp := range ms {
            s := strings.Split(cp,":")
            call := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,s[1])
            network.Main[s[0]] = call
            if s[1] == dstar.DEXTRA_FROM_LETTER {
                dv4.SetMain(s[0],dstar.DEXTRA_MAIN_LETTER)
            } else if s[1] == dstar.DCS_FROM_LETTER {
                dv4.SetMain(s[0],dstar.DCS_MAIN_LETTER)
            } else {
                dv4.SetMain(s[0],s[1])
            }
        }
    }

    if (dsn != nil) && (Cb.LocalMains != "") {
        ms := strings.Split(Cb.LocalMains, ",")
        for _, cp := range ms {
            s := strings.Split(cp,":")
            call := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,s[1])
            network.Main[s[0]] = call
            if s[1] == dstar.DEXTRA_FROM_LETTER {
                dsn.SetMain(s[0],dstar.DEXTRA_MAIN_LETTER)
            } else if s[1] == dstar.DCS_FROM_LETTER {
                dsn.SetMain(s[0],dstar.DCS_MAIN_LETTER)
            } else {
                dsn.SetMain(s[0],s[1])
            }
        }
    }

    if (Cb.LocalMains == "") && Cb.Links != "" {
        links := strings.Split(Cb.Links,",")
        for _, l := range links {
            routes := strings.Split(l,":")
            var aroutes []string
            for _, r := range routes {
                call := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,r)
                aroutes = append(aroutes,call)
            }
            if len(aroutes) > 1 {
                network.Links = append(network.Links,aroutes)
            }
        }
    }
    erc <- errors.New(fmt.Sprintf("Destinations and links: %+v",network))
    return &network
}

func StdoutLog(format string, s ...interface{}) {
    now := time.Now().UTC()
    fmt.Printf(now.Format(tformat))
    fmt.Printf(format, s...)
    fmt.Printf("\n")
}

func Log(format string, s ...interface{}) {
    if Cb.LOG == "stdout" {
        StdoutLog(format, s...)
        return
    }
    var err error
    if logfile == nil {
        logfile, err = os.OpenFile(Cb.LOG,
            (os.O_APPEND|os.O_WRONLY|os.O_CREATE),0644)
        if err != nil {
            Cb.LOG = "stdout"
            StdoutLog(err.Error())
            StdoutLog("NOT logging to file")
            StdoutLog(format, s...)
            return
        }
    }
    now := time.Now().UTC()
    logfile.Write([]byte(now.Format(tformat)))
    logfile.Write([]byte(fmt.Sprintf(format,s...)))
    logfile.Write([]byte("\n"))
    fname := logfile.Name()
    fi, _ := os.Stat(fname)
    if fi.Size() > 1024*1024*5 {
        logfile.Close()
        logfile = nil
        os.Rename(fname,fname + "~")
    }
}

func Tx(f *dstar.DSXFrame, fbc chan<- string,
    erc chan<- error) {
    if Cb.RouteDebug == "yes" {
        erc <- errors.New(fmt.Sprintf("Route Tx(): TXing %+v",f))
    }
    t, ok := netw.RouteTable[f.Hdr.DestRPT]
    if ok {
        switch t {
            case TYPE_SATOSHI:
                if sat != nil {
                    sat.Tx(f,fbc,erc)
                }
            case TYPE_DVRPTRV2:
                if dvr2 != nil {
                    dvr2.Tx(f,fbc,erc)
                }
            case TYPE_DV4MINI:
                if dv4 != nil {
                    dv4.Tx(f,fbc,erc)
                }
            case TYPE_DSNDEV:
                if dsn != nil {
                    dsn.Tx(f,fbc,erc)
                }
            case TYPE_DEXTRA:
                ex.Tx(f)
            case TYPE_DCS:
                dcs.Tx(f)
        }
    } else if Cb.RouteDebug == "yes" {
        erc <- errors.New("Route Tx(): no route for " + f.Hdr.DestRPT)
    }
}

func defaultRoute(f *dstar.DSXFrame, fbc chan<- string, erc chan<- error) {
    if Cb.RouteDebug == "yes" {
        erc <- errors.New("Route defaultRoute(): function called")
    }
    // take care of satoshi duplex
    if (sat != nil) && (strings.TrimSpace(f.Hdr.YoCall) == "CQCQCQ") &&
        isIn(sat.GetModules(),f.FromMod) {
        tf := *f
        tf.Hdr.Nat(Cb.ModulesOwner,Cb.ModulesOwner,f.FromMod,f.FromMod)
        Tx(&tf,fbc,erc)
    }
    // .. and dvrptrv2
    if (dvr2 != nil) && (strings.TrimSpace(f.Hdr.YoCall) == "CQCQCQ") &&
        isIn(dvr2.GetModules(),f.FromMod) {
        tf := *f
        tf.Hdr.Nat(Cb.ModulesOwner,Cb.ModulesOwner,f.FromMod,f.FromMod)
        Tx(&tf,fbc,erc)
    }
    // normal mode
    if Cb.LocalMains != "" {
        maindest, ok := netw.Main[f.FromMod]
        if !ok {
            // no main
            if Cb.RouteDebug == "yes" {
                erc <- errors.New(
                    "Route defaultRoute(): No main for " + f.FromMod)
            }
        } else {
            if Cb.RouteDebug == "yes" {
                erc <- errors.New(fmt.Sprintf(
                    "Route defaultRoute(): %s has main %s",f.FromMod,maindest))
            }
            f.Hdr.DestRPT = maindest
            tf := *f
            Tx(&tf,fbc,erc)
        }
        return
    }
    // links mode
    frm := f.FromMod
    if frm == dstar.DEXTRA_MAIN_LETTER {
        frm = dstar.DEXTRA_FROM_LETTER
    } else if frm == dstar.DCS_MAIN_LETTER {
        frm = dstar.DCS_FROM_LETTER
    }
    localfrom := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,frm)
    for _, v := range netw.Links {
        if isIn(v,localfrom) {
            fakemod := ""
            for _, dest := range v {
                if fakemod == "" {
                    fakemod = dest
                }
                // avoid loops
                if dest == localfrom {
                    if Cb.RouteDebug == "yes" {
                        erc <- errors.New("Route defaultRoute(): Avoid loop.")
                    }
                    continue
                }
                if Cb.RouteDebug == "yes" {
                    erc <- errors.New("Route: defaultRoute(): TX to " + dest)
                }
                f.Hdr.DestRPT = dest
                f.FromMod = fakemod[7:8]
                tf := *f
                Tx(&tf,fbc,erc)
            }
        }
    }
}

func EstablishLink(ref,locmod,remmod string) string {
    if strings.HasPrefix(ref,"XRF") || strings.HasPrefix(ref,"LL") {
        return ex.EstablishLink(ref,locmod,remmod)
    } else if strings.HasPrefix(ref,"DCS") {
        return dcs.EstablishLink(ref,locmod,remmod)
    }
    return "Reflector not supported: " + ref
}

func deleteRoute(mod1, mod2 string) string {
    mod1 = strings.ToUpper(mod1)
    mod2 = strings.ToUpper(mod2)
    l1 := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,mod1)
    l2 := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,mod2)
    // remove departure module from everywhere
    remove := -1
    ret := ""
    for i := range netw.Links {
        in1 := isIn(netw.Links[i],l1)
        in2 := isIn(netw.Links[i],l2)
        if in1 && in2 {
            nl := []string{}
            for _, d := range netw.Links[i] {
                if d != l1 {
                    nl = append(nl, d)
                }
            }
            if len(nl) == 1 { //remove when empty
                remove = i
                ret = "Route destroyed."
            } else {
                netw.Links[i] = nl
                ret = "Route removed."
            }
            break
        }
    }
    if remove != -1 {
        newlink := make([][]string,0)
        newlink = append(newlink,netw.Links[:remove]...)
        newlink = append(newlink,netw.Links[remove+1:]...)
        netw.Links = newlink
    }
    return ret
}

func infoRoute(mod1 string) string {
    mod1 = strings.ToUpper(mod1)
    l1 := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,mod1)
    ret := ""
    for i := range netw.Links {
        if isIn(netw.Links[i],l1) {
            for j := range netw.Links[i] {
                ret = ret + netw.Links[i][j][7:8]
            }
            ret = ret + ","
        }
    }
    m, ok := netw.Main[mod1]
    if ok {
        ret = ret + "Main "+m[7:8]
    } else {
        if len(ret) > 0 {
            ret = ret[:len(ret)-1]
        } else {
            ret = "No route"
        }
    }
    return ret
}

func setupRoute(mod1,mod2 string) string {
    // mod1 departure mod, mod2 destination mod
    mod1 = strings.ToUpper(mod1)
    mod2 = strings.ToUpper(mod2)
    l1 := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,mod1)
    l2 := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,mod2)
    added := false
    ret := ""
    for i := range netw.Links {
        in1 := isIn(netw.Links[i],l1)
        in2 := isIn(netw.Links[i],l2)
        if in1 && in2 {
            // route is already there.
            added = true
            ret = "Route exists"
            break
        }
        if in1 {
            // let's add here the departure mod
            netw.Links[i] = append(netw.Links[i],l2)
            added = true
            ret = "Route added (2)"
            break
        }
        if in2 {
            // let's add here the arrival mod
            netw.Links[i] = append(netw.Links[i],l1)
            added = true
            ret = "Route added (1)"
            break
        }
    }
    if !added {
        // if I arrive here, I should add both
        netw.Links = append(netw.Links,[]string{l1,l2})
        // new link, pass the command
        ret = "Route created."
    }
    return ret
}

func localSetMain(mod1, mod2 string, mt int) {
    mod1 = strings.ToUpper(mod1)
    if (mt != -1) {
        mod2 = strings.ToUpper(mod2)
        l2 := fmt.Sprintf("%-7s%s",Cb.ModulesOwner,mod2)
        netw.Main[mod1] = l2
        // FIXME add dv4mini and dvrptrv2
        // mod1 is always a satoshi.
        // mod2 could be or not .. dont setup main automatically for it for now
        if mod2 == dstar.DEXTRA_FROM_LETTER {
            if mt == TYPE_SATOSHI {
                sat.SetMain(mod1,dstar.DEXTRA_MAIN_LETTER)
            } else if mt == TYPE_DVRPTRV2 {
                dvr2.SetMain(mod1,dstar.DEXTRA_MAIN_LETTER)
            } else if mt == TYPE_DV4MINI {
                dv4.SetMain(mod1,dstar.DEXTRA_MAIN_LETTER)
            } else if mt == TYPE_DSNDEV {
                dsn.SetMain(mod1,dstar.DEXTRA_MAIN_LETTER)
            }
        } else if mod2 == dstar.DCS_FROM_LETTER {
            if mt == TYPE_SATOSHI {
                sat.SetMain(mod1,dstar.DCS_MAIN_LETTER)
            } else if mt == TYPE_DVRPTRV2 {
                dvr2.SetMain(mod1,dstar.DCS_MAIN_LETTER)
            } else if mt == TYPE_DV4MINI {
                dv4.SetMain(mod1,dstar.DCS_MAIN_LETTER)
            } else if mt == TYPE_DSNDEV {
                dsn.SetMain(mod1,dstar.DCS_MAIN_LETTER)
            }
        } else {
            if mt == TYPE_SATOSHI {
                sat.SetMain(mod1,mod2)
            } else if mt == TYPE_DVRPTRV2 {
                dvr2.SetMain(mod1,mod2)
            } else if mt == TYPE_DV4MINI {
                dv4.SetMain(mod1,mod2)
            } else if mt == TYPE_DSNDEV {
                dsn.SetMain(mod1,mod2)
            }
        }
    }
}

func getModuleType(mod string) int {
    fromtype := -1
    if sat != nil {
        sm := sat.GetModules()
        if isIn(sm, mod) {
            fromtype = TYPE_SATOSHI
        }
    } else if dvr2 != nil {
        dm := dvr2.GetModules()
        if isIn(dm, mod) {
            fromtype = TYPE_DVRPTRV2
        }
    } else if dv4 != nil {
        dm := dv4.GetModules()
        if isIn(dm, mod) {
            fromtype = TYPE_DV4MINI
        }
    } else if dsn != nil {
        dm := dsn.GetModules()
        if isIn(dm, mod) {
            fromtype = TYPE_DSNDEV
        }
    }
    return fromtype
}

func interceptCommand(yocall, realfrommod string, fbc chan<- string) {
    now := time.Now()
    if (netw.lastCommandTime != nil) &&
        (netw.lastCommandTime.Add(2*time.Second).After(now)) {
        return
    }
    netw.lastCommandTime = &now
    fromtype := getModuleType(realfrommod)
    if strings.HasPrefix(yocall,"XRF") {
        localSetMain(realfrommod,dstar.DEXTRA_FROM_LETTER, fromtype)
    } else if strings.HasPrefix(yocall,"DCS") {
        localSetMain(realfrommod,dstar.DCS_FROM_LETTER, fromtype)
    } else if strings.HasPrefix(yocall,"LST") && sat != nil {
        lub := ""
        // dv4mini accepts no commands (no header :( )
        if fromtype == TYPE_SATOSHI {
            lub = sat.GetLastUsedBy(realfrommod)
        } else if fromtype == TYPE_DVRPTRV2 {
            lub = dvr2.GetLastUsedBy(realfrommod)
        }
        // when I transmit a feedback or last tranmission has come from
        // a module, avoid this
        if lub != "" && (strings.TrimSpace(lub[:6]) != Cb.ModulesOwner) {
            if strings.HasPrefix(lub,"XRF") {
                localSetMain(realfrommod,dstar.DEXTRA_FROM_LETTER, fromtype)
            } else if strings.HasPrefix(lub,"DCS") {
                localSetMain(realfrommod,dstar.DCS_FROM_LETTER, fromtype)
            }
            // format is always "XXXNNN M"
            // if lub is M0XXX it will be killed by EstablishLink
            EstablishLink(strings.TrimSpace(lub[:6]),lub[7:8],realfrommod)
            fbc <- fmt.Sprintf("%s:Main %s",realfrommod,lub)
        } else {
            fbc <- fmt.Sprintf("%s:No info",realfrommod)
        }
    }
}

func tryCommand(deparpt,yocall,realfrommod string, fbc chan<- string,
    erc chan<- error) {
    // FIXME check realfrom to be local?
    now := time.Now()
    if (netw.lastCommandTime != nil) &&
        (netw.lastCommandTime.Add(2*time.Second).After(now)) {
        return
    }
    netw.lastCommandTime = &now
    ref := yocall[:6]
    mod := yocall[6:7]
    command := yocall[7:8]

    // verify module exists
    _, ok := netw.RouteTable[fmt.Sprintf("%-7s%s",ref,mod)]
    ret := ""
    switch command {
        case "L":
            if !ok {
                erc <- errors.New("Route: No such module "+mod)
                ret = "No such module"
            } else {
                ret = setupRoute(realfrommod,mod)
            }
        case "U":
            if !ok {
                erc <- errors.New("Route: No such module "+mod)
                ret = "No such module"
            } else {
                ret = deleteRoute(realfrommod,mod)
            }
        case "I":
            ret = infoRoute(realfrommod)
        default:
            ret = "Unknown command"
    }
    fbc <- fmt.Sprintf("%s:%s",realfrommod,ret)
}

func main() {
    /* init/setup/conf crap */
    wfile := flag.Bool("w", false, "write initial conf")
    conf := flag.String("c", "", "path to conf")
    flag.Parse()
    if *conf == "" {
        fmt.Printf("DstarX %s\n",VERSION)
        fmt.Println("Usage: dstarx -c file [-w]")
        os.Exit(1)
    }

    Cb = confbox.ConfBox{}
    if *wfile {
        Cb.WriteSample(*conf)
        os.Exit(0)
    }
    Cb.Load(*conf)
    Log("DstarX %s",VERSION)

    /* Here starts the real work */
    //runtime.GOMAXPROCS(runtime.NumCPU())
    rand.Seed(int64(os.Getpid()))
    /* Error logging */
    erchan := make(chan error)
    go func() {
        for {
            Log("%s",<-erchan)
        }
    }()

    fbchan := make(chan string)
    /* DExtra init */
    Log("DExtra init..")
    ex = &dstar.DExtra{}
    ex.Cb = &Cb
    ex.ErChan = erchan
    ex.FbChan = fbchan
    err := ex.Init()
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }

    exd := make(chan *dstar.DSXFrame)
    go ex.StartServer(exd)

    /* DCS init */
    Log("DCS init..")
    dcs = &dstar.DCS{}
    dcs.Cb = &Cb
    dcs.ErChan = erchan
    dcs.FbChan = fbchan
    err = dcs.Init()
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    go dcs.StartServer(exd)

    /* autoconnect stuff */
    if Cb.AUTOCONNECT != "" {
        Log("Establishing links..")
        for _, ct := range strings.Split(Cb.AUTOCONNECT,",") {
            pars := strings.Split(ct,":")
            Log("%s",EstablishLink(pars[0],pars[1],pars[2]))
        }
    }

    /* Satoshi init */
    if Cb.SATOSHI_MODULES != "" {
        Log("Satoshi init..")
        sat = &satoshi.SatoshiCtx{}
        sat.Cb = &Cb
        err = sat.Init(erchan)
        //defer sctx.Close()
        if err != nil {
            Log("%s",err.Error())
        }
        sat.StartReceiving(exd)
        sat.PrepareTransmission()
    }

    /* DVRPTRV2 init */
    if Cb.DVRPTRV2_MODULES != "" {
        Log("DV-RPTR V2 init..")
	dvr2 = &dvrptrv2.DvrPtrV2Ctx{}
	dvr2.Cb = &Cb
	err = dvr2.Init(erchan)
        if err != nil {
            Log("%s",err.Error())
        }
        dvr2.StartReceiving(exd)
        dvr2.PrepareTransmission()
    }

    /* DV4MINI init */
    if Cb.DV4MINI_MODULES != "" {
        Log("DV4mini init..")
        dv4 = &dv4mini.Dv4miniCtx{}
        dv4.Cb = &Cb
        err = dv4.Init(erchan)
        if err != nil {
            Log("%s", err.Error())
        }
        dv4.StartReceiving(exd)
        dv4.PrepareTransmission()
    }

    /* DSNDevs init */
    if Cb.DSN_MODULES != "" {
        Log("DSN Devs init..")
        dsn = &dsnetwork.DSNDevCtx{}
        dsn.Cb = &Cb
        err = dsn.Init(erchan)
        if err != nil {
            Log("%s", err.Error())
        }
        dsn.StartReceiving(exd)
        dsn.PrepareTransmission()
    }

    Log("Setup network..")
    netw = setupNetwork(erchan)

    // program server
    Log("Spinning up server..")
    go Serve(erchan)
    // feedback loop
    Log("Setting up feedback loop..")
    go func() {
        for {
            // destmod:srcmod:message OR destmod:message
            m := <-fbchan
            time.Sleep(1*time.Second)
            ms := strings.Split(m,":")
            if (len(ms) != 2) && (len(ms) != 3) {
                // double check .. 
                erchan <- errors.New("Mmmhh, wrong feedback format..")
                continue
            }
            destmod := ms[0]
            message := ""
            fromreply := ""
            if len(ms) == 2 {
                gw, ok := netw.Main[ms[0]]
                if !ok {
                    erchan <- errors.New(fmt.Sprintf(
                        "I don't know where to send '%s' from module %s",
                        ms[1],ms[0]))
                    continue
                }
                // find out the destination's main
                message = ms[1]
                switch gw[7:8] {
                    case dstar.DEXTRA_FROM_LETTER:
                        fromreply = dstar.DEXTRA_MAIN_LETTER
                    case dstar.DCS_FROM_LETTER:
                        fromreply = dstar.DCS_MAIN_LETTER
                }
            } else {
                fromreply = ms[1]
                message = ms[2]
            }

            sm := dstar.GetSilentMessage(message)
            lp := len(sm)
            Log("FEEDBACK %s",message)
            for i := range sm {
                f := dstar.DSXFrame{}
                f.Hdr.YoCall = "CQCQCQ"
                f.Hdr.MyCall1 = Cb.ModulesOwner
                f.Hdr.DepaRPT = fmt.Sprintf("%-7sG",Cb.ModulesOwner)
                f.Hdr.DestRPT = fmt.Sprintf("%-7s%s",Cb.ModulesOwner,destmod)
                f.Hdr.SetCRC()

                f.Frm.Fill(sm[i])
                f.Seq = byte(i)
                f.FromMod = fromreply
                if i == (lp - 1) {
                    f.LastFrame = true
                } else {
                    f.LastFrame = false
                }
                Tx(&f,fbchan,erchan)
                time.Sleep(2*time.Millisecond)
            }

        }
    }()

    // main routing loop
    Log("Entering main routing loop.")
    for {
        localgw := fmt.Sprintf("%-7sG",Cb.ModulesOwner)
        f := <-exd
        if Cb.RouteDebug == "yes" {
            erchan <- errors.New(fmt.Sprintf("Route: %+v",f))
        }
        // Local satoshi routes commands (to ModulesOwner)
        if strings.TrimSpace(f.Hdr.YoCall[:6]) == Cb.ModulesOwner {
            if Cb.RouteDebug == "yes" {
                erchan <- errors.New("Route: ModulesOwner command.")
            }
            tryCommand(f.Hdr.DepaRPT,f.Hdr.YoCall,f.FromMod,fbchan,erchan)
            continue
        }
        // intercept net commands and autosetup route (to XRF*,DCS*)
        if strings.TrimSpace(f.Hdr.YoCall) != "CQCQCQ" &&
            Cb.LocalMains != "" {
            interceptCommand(f.Hdr.YoCall,f.FromMod,fbchan)
        }
        if Cb.LocalMains == "" {
            if Cb.RouteDebug == "yes" {
                erchan <- errors.New("Route: Network Routing")
            }
            if strings.TrimSpace(f.Hdr.YoCall) != "CQCQCQ" {
                erchan <- errors.New("Route: Commanding disabled in Net mode")
            } else {
                go defaultRoute(f,fbchan,erchan)
            }
        } else if strings.TrimSpace(f.Hdr.DestRPT) == "DIRECT" {
            // this shouldn't really happen..
            if Cb.RouteDebug == "yes" {
                erchan <- errors.New("Route: DIRECT")
            }
            go defaultRoute(f,fbchan,erchan)
        } else if (f.Hdr.DestRPT == localgw) {
            if Cb.RouteDebug == "yes" {
                erchan <- errors.New("Route: localgw (" + localgw + ")")
            }
            go defaultRoute(f,fbchan,erchan)
        } else if strings.HasPrefix(f.Hdr.DestRPT,Cb.ModulesOwner) {
            if Cb.RouteDebug == "yes" {
                erchan <- errors.New("Route: Explicit routing")
            }
            go Tx(f,fbchan,erchan)
        }
    }
}

