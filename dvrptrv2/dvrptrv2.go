
package dvrptrv2

import (
    "fmt"
    "net"
    "time"
    "errors"
    "strings"
    "strconv"
    "gitlab.com/galberti/dstarx/dstar"
    "gitlab.com/galberti/dstarx/devices"
//    "github.com/kylelemons/gousb/usb"
)

const (
    // commands
    DISABLE_RPTR_MODE = "HEADX9000"
    WRITE_HEADER = "HEADX0002"
    WRITE_DATA = "HEADZ"
    WRITE_CONFIG = "HEADX9001"

    // replies
    QUERY_REPLY = "HEADX9900"
    DATA_REPLY = "HEADZ"
    HDR_N_REPLY = "0001"
    CONF_N_REPLY = "9001"
    SPACE_N_REPLY = "9011"
    TIMEOUT_N_REPLY = "9021"

    // pkt sizes
    X_PKT_SIZE   = 105
    Y_PKT_SIZE = 10
    Z_PKT_SIZE = 20
    DATA_PKT_SIZE = 17

    // commands are HEAD?XXXX, ? is X, Y, Z, XXXX is 4 numbers
    CMD_PREFIX_SIZE = 5
    CMD_SUFFIX_SIZE = 4
    CMD_SIZE = CMD_PREFIX_SIZE + CMD_SUFFIX_SIZE
)

type DvrPtrV2 struct {
    devices.GenericDevice
    //dev *usb.Device
    hw DP2HW
    //version string
    ptt bool
    Seq byte
}

/* HW abstraction */
type DP2HW struct {
    conn *net.Conn
    ip string
    port string
}

func (d *DP2HW) Read(b []byte) (int, error) {
    return (*d.conn).Read(b)
}

func (d *DP2HW) Write(b []byte) (int, error) {
    return (*d.conn).Write(b)
}

func (d *DP2HW) Init(ip, port string) (net.Conn, error) {
    d.ip = ip
    d.port = port
    c, err := net.Dial("tcp", fmt.Sprintf("%s:%s",ip,port))
    if err != nil {
        return nil, err
    }
    d.conn = &c
    return c, err
}

/* DvrPtrV2 */

func (d *DvrPtrV2) IsDuplex() bool {
    addmods := strings.Split(d.Cb.DVRPTRV2_MODULES,",")
    for _, am := range addmods {
        a := strings.Split(am,":")
        if a[0] == d.hw.ip {
            return a[3] == "D"
        }
    }
    // TODO fix false also if module not found .. 
    return false
}

func (d *DvrPtrV2) GetModule() string {
    // conf is "IP:port:A:S:CALLSIGN:INVERT:MODLVL:TXDELAY,.."
    addmods := strings.Split(d.Cb.DVRPTRV2_MODULES,",")
    for _, am := range addmods {
        a := strings.Split(am,":")
        if a[0] == d.hw.ip {
            return a[2]
        }
    }
    return ""
}

// fake ptt implementation
func (d *DvrPtrV2) SetPTT(on bool) error {
    d.ptt = on
    return nil
}

func (d *DvrPtrV2) GetPTT() (bool, error) {
    return d.ptt, nil
}

func (d *DvrPtrV2) txTimeout(reset <-chan bool) {
    tot := 0
    for {
        select {
            case <-reset:
                tot = 0
            case <-time.After(100*time.Millisecond):
                tot = tot + 1
        }
        if tot == 10 { // after 1 sec, release PTT (just for safety..)
            d.FIFOClean() // this is just in case I havent got the lastframe..
            d.SetPTT(false)
        }
        now := time.Now()
        if d.UsedBy() != "" &&
            d.UsedByTime().Add(15*time.Second).Before(now) {
            d.UsedByMutexLock()
            d.SetUsedBy("")
            d.UsedByMutexUnlock()
        }
        if (d.LastHeader() != nil) &&
            (d.LastHeaderTime().Add(5*time.Second).Before(now)) {
            d.SetLastHeader(nil)
        }
    }
}

func (d *DvrPtrV2) txLoop(txt chan<- bool) {
    for {
        f := <-d.Dch
        ptt, err := d.GetPTT()
        if (err == nil) && (ptt == false) {
            time.Sleep(50*time.Millisecond)
            if d.Cb.DVrptrv2Debug == "yes" {
                d.Erc <- errors.New(fmt.Sprintf(
                    "DvrPtrV2 TxLoop: TX HDR: %+v",f.Hdr))
            }
            d.writeHeader(f.Hdr.Get())
            time.Sleep(1*time.Millisecond)
            d.SetPTT(true)
            time.Sleep(20*time.Millisecond)
        }
        err = d.writeData(f.Frm.Get())
        if err != nil {
            // if you see this in the logs, increase the 20ms above
            d.Erc <- errors.New("Satoshi TxLoop: " + err.Error())
        }
        txt <- true
        /*
        for {
            free, _ := s.GetSpace()
            if free < 12 {
                time.Sleep(10*time.Millisecond)
            } else {
                break
            }
        }
        */
    }
}

func (d *DvrPtrV2) processData(b []byte, ret chan<- *dstar.DSXFrame) {
    // 9 (voice) + 3 (data) + 3 (2 session, 1 count). See DExtra for more info
    dsf := dstar.DSFrame{}
    err := dsf.Fill(b[:dstar.DSTAR_FRAME_SIZE])
    if err == nil {
        if d.Cb.DVrptrv2Debug == "yes" {
            d.Erc <- errors.New(fmt.Sprintf(
                "DVrptrv2 processData(): Good Frame: %x",dsf))
        }
        if d.LastHeader() != nil {
            now := time.Now()
            last := (b[15] & 0x40) == 0x40
            dsxf := dstar.DSXFrame{
                Hdr: *d.LastHeader(),
                Frm: dsf,
                FromMod: d.Module(),
                LastFrame: last,
                Seq: d.Seq,
            }
            ret <- &dsxf
            d.SetUsedBy(fmt.Sprintf("%-7s%s",d.Cb.ModulesOwner,d.Module()))
            d.SetUsedByTime(now)
            d.SetLastHeaderTime(now)
            d.Seq = d.Seq + 1
            if d.Seq == 21 {
                d.Seq = 0
            }
        }
    } else {
        if d.Cb.DVrptrv2Debug == "yes" {
            d.Erc <- errors.New("DVrptrv2 processData(): Fill() " + err.Error())
        }
    }
}

func (d *DvrPtrV2) processHeader(b []byte) {
    // 96, but only 39 used (41 dstar header - 2 crc)
    dsh := dstar.DSHeader{}
    err := dsh.Fill(b[:dstar.DSTAR_HEADER_SIZE])
    if err != nil {
        if d.Cb.DVrptrv2Debug == "yes" {
            d.Erc <- errors.New("DVrptrv2 processHeader(): Fill() " +
                err.Error())
        }
    } else {
        d.SetModule(d.GetModule())
        if d.Cb.DVrptrv2Debug == "yes" {
            d.Erc <- errors.New(fmt.Sprintf(
                "DVrptrv2 processHeader(): Good Header: %+v",dsh))
        }
        if strings.TrimSpace(dsh.DepaRPT) == "DIRECT" {
            dsh.Nat(d.Cb.ModulesOwner,d.Cb.ModulesOwner,d.Module(),"G")
        }
        d.SetLastHeader(&dsh)
        d.SetLastHeaderTime(time.Now())
        d.Seq = 0
    }
}

func (d *DvrPtrV2) StartReadLoops(data chan<- *dstar.DSXFrame) error {
    go func() {
        r := make([]byte,1024)
        rx := 0
        used := 0
        for {
            for {
                l, err := d.hw.Read(r[rx:])
                rx = rx + l
                if err != nil {
                    if (err.Error() == "EOF") || (l == 0) {
                        n := 0
                        for {
                            if (n % (120)) == 0 {
                                d.Erc <- errors.New(
                                    "DvrPtrV2 Read(): conn closed..")
                            }
                            _, e := d.hw.Init(d.hw.ip,d.hw.port)
                            n = n + 1
                            if e == nil {
                                e2 := d.unsetRptr()
                                if e2 == nil {
                                    break
                                }
                            }
                            time.Sleep(5*time.Second)
                        }
                    } else {
                        d.Erc <- errors.New(fmt.Sprintf(
                            "DvrPtrV2 Read(): %v (l=%v)",err.Error(),l))
                    }
                }
                // TODO wrong alignment? does it even happen?
                if rx >= 10 {
                    break
                }
            }
            // process data
            s := string(r[:CMD_SIZE])
            used = 0
            if s[:CMD_PREFIX_SIZE] == DATA_REPLY {
                // HEADZdata..
                if rx < Z_PKT_SIZE {
                    continue
                }
                d.processData(r[CMD_PREFIX_SIZE:], data)
                used = Z_PKT_SIZE
            } else if s[CMD_PREFIX_SIZE:CMD_SIZE] == HDR_N_REPLY {
                // HEADXNNNN
                if rx < X_PKT_SIZE {
                    continue
                }
                if r[104] == 0x01 {
                    d.processHeader(r[9:])
                } else {
                    // why should that be ONLY 0x01 ?
                    d.Erc <- errors.New(fmt.Sprintf(
                        "DvrPtrV2 Read(): r[104] is %x",r[104]))
                }
                used = X_PKT_SIZE
            } else if s[CMD_PREFIX_SIZE:CMD_SIZE] == CONF_N_REPLY {
                d.Erc <- errors.New("DvrPtrV2 Read(): config sent OK")
            } else if s[CMD_PREFIX_SIZE:CMD_SIZE] == SPACE_N_REPLY {
                // FIXME
            } else if s[CMD_PREFIX_SIZE:CMD_SIZE] == TIMEOUT_N_REPLY {
                // FIXME
            } else {
                // FIXME
            }
            tmp := make([]byte, 1024)
            copy(tmp[:],r[used:])
            r = tmp
            rx = 0
        }
    }()
    return nil
}

func (d *DvrPtrV2) GetVersion() (string, error) {
    return "Unknown", nil
}

func (d *DvrPtrV2) unsetRptr() error {
    p := make([]byte, X_PKT_SIZE)
    copy(p,[]byte(DISABLE_RPTR_MODE))
    d.hw.Write(p)

    b := make([]byte, 105)
    _, e := d.hw.Read(b)
    if e != nil {
        return e
    }
    // I should save version here, according to G4KLX serial is b[9:13]
    // I don't bother for now.
    //if string(b[:6]) == QUERY_REPLY {
    //}
    return nil
}

func (d *DvrPtrV2) Close() error {
    // FIXME disconnect maybe?
    return nil
}

func (d *DvrPtrV2) writeHeader(hdr []byte) {
    b := make([]byte, X_PKT_SIZE)
    copy(b[:CMD_SIZE],[]byte(WRITE_HEADER))
    copy(b[CMD_SIZE:],hdr[:dstar.DSTAR_HEADER_SIZE-2])
    d.hw.Write(b)
}

func (d *DvrPtrV2) writeData(data []byte) error {
    b := make([]byte, DATA_PKT_SIZE)
    copy(b[:CMD_PREFIX_SIZE],[]byte(WRITE_DATA))
    copy(b[CMD_PREFIX_SIZE:],data)
    d.hw.Write(b)
    return nil
}

func (d *DvrPtrV2) setConfig(erc chan<- error) {
    // IP:port:A:S:CALLSIGN:INVERT:MODLVL:TXDELAY,..
    confs := strings.Split(d.Cb.DVRPTRV2_MODULES,",")
    callsign := ""
    txInvert := ""
    modLevel := 0
    txDelay := 0
    for _, c := range confs {
        subc := strings.Split(c,":")
        if subc[0] == d.hw.ip {
            callsign = subc[4]
            var e error
            txInvert = subc[5]
            modLevel, e = strconv.Atoi(subc[6])
            if e != nil {
                erc <- errors.New("DvrPtrV2 setConfig(): wrong modLevel")
                return
            }
            txDelay, e = strconv.Atoi(subc[7])
            if e != nil {
                erc <- errors.New("DvrPtrV2 setConfig(): wrong txDelay")
                return
            }
            break
        }
    }

    if callsign == "" {
        erc <- errors.New(fmt.Sprintf(
            "DvrPtrV2 setConfig(): can't find config for %v",d.hw.ip))
        return
    }

    b := make([]byte, X_PKT_SIZE)
    copy(b[:CMD_SIZE],WRITE_CONFIG)
    copy(b[CMD_SIZE:CMD_SIZE+8],fmt.Sprintf("%-8s",callsign))
    if d.IsDuplex() {
        b[65] = 0x03
    } else {
        b[65] = 0x00
    }
    if txInvert[0:1] == "Y" || txInvert[0:1] == "y" {
        b[66] = 0x01
    } else {
        b[66] = 0x00
    }
    b[73] = byte(modLevel * 256 / 100)

    // Internal and external speaker on (wtf)
    b[86] = 0x03
    b[87] = 0x01

    if txDelay < 100 {
        txDelay = 100
    } else if txDelay > 850 {
        txDelay = 850
    }
    b[89] = byte(txDelay / 10)
    d.hw.Write(b)
}

func (d *DvrPtrV2) StartWriteLoops() error {
    b := make(chan bool)
    // FIXME do I need this buffered?
    d.Dch = make(chan *dstar.DSXFrame, 50) // 50 pkts = 1 sec
    go d.txTimeout(b)
    go d.txLoop(b)
    return nil
}

type DvrPtrV2Ctx struct {
    devices.GenericCtx
}

func (dctx *DvrPtrV2Ctx) Init(erc chan<- error) error {
    devs := strings.Split(dctx.Cb.DVRPTRV2_MODULES,",")
    dd := make([]devices.IDevice,0)
    for _, dev := range devs {
        pars := strings.Split(dev,":")
        hw := DP2HW{}
        erc <- errors.New(fmt.Sprintf("connecting %s:%s",pars[0],pars[1]))
        _, err := hw.Init(pars[0],pars[1])
        if err != nil {
            erc <- errors.New(fmt.Sprintf("DVrptrv2: %+v", pars[0],err))
            continue
        }
        d := DvrPtrV2{
            hw: hw,
            GenericDevice: devices.GenericDevice{ Thr: 5, Cb: dctx.Cb, Erc: erc},
        }
        d.FIFOInit()
        d.SetPTT(false)
        d.unsetRptr()
        d.setConfig(erc)
        dd = append(dd,&d)
    }
    dctx.SetSs(dd)
    return nil
}

