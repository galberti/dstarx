
package satoshi

import (
    "fmt"
    "time"
    "errors"
    "strings"
    //"bytes"
    "gitlab.com/galberti/dstarx/dstar"
    "gitlab.com/galberti/dstarx/devices"
    "github.com/kylelemons/gousb/usb"
)

const (
    // commands
    SET_AD_INIT     = 0x00
    SET_PTT         = 0x05
    PUT_DATA        = 0x10
    GET_DATA        = 0x11
    GET_HEADER      = 0x21
    GET_AD_STATUS   = 0x30
    SET_MyCALL1     = 0x40
    SET_MyCALL2     = 0x41
    SET_YoCALL      = 0x42
    SET_DestRPT     = 0x43
    SET_DepaRPT     = 0x44
    SET_FLAGS       = 0x45
    GET_REMAINSPACE = 0x50
    GET_VERSION     = 0xff

    // status bitmasks
    COS_STAT        = 0x02
    CRC_ERROR       = 0x04
    LAST_FRAME      = 0x08
    PTT_STAT        = 0x20

    // IDs
    VENDOR_ID       = 0x04d8
)

type Satoshi struct {
    devices.GenericDevice
    dev *usb.Device
    version string
    Seq byte
}

/* Satoshi */

func (s *Satoshi) SetPTT(on bool) error {
    var err error
    if on {
        _, err = s.dev.Control(0x40, SET_PTT, 1, 0, make([]byte,0))
    } else {
        _, err = s.dev.Control(0x40, SET_PTT, 0, 0, make([]byte,0))
    }
    return err
}

func (s *Satoshi) GetPTT() (bool, error) {
    status := make([]byte,1)
    _, err := s.dev.Control(0xc0, GET_AD_STATUS, 0, 0, status)
    if err != nil {
        return false, err
    }
    if (status[0] & PTT_STAT) == PTT_STAT {
        return true, nil
    }
    return false, nil
}

func (s *Satoshi) GetVersion() (string, error) {
    if s.version != "" {
        return s.version, nil
    }
    version := make([]byte, 8)
    l, err := s.dev.Control(0xc0, GET_VERSION, 0, 0, version)
    if err != nil {
        return "", err
    }
    if l < 8 {
        return "", errors.New("WARNING: version is shorter than 8")
    }
    s.version = string(version)
    return s.version, nil
}

func (s *Satoshi) Close() error {
    s.dev.Close()
    return nil
}

func (s *Satoshi) readData() ([]byte, bool, error) {
    rx := 0
    last := false
    status := make([]byte,1)
    data := make([]byte, dstar.DSTAR_FRAME_SIZE)
    for rx < dstar.DSTAR_FRAME_SIZE {
        l, err := s.dev.Control(0xc0, GET_DATA, 0, 0,
            data[rx:dstar.DSTAR_FRAME_SIZE])
        if err != nil {
            return nil, false, err
        }
        if l == 0 {
            _, err = s.dev.Control(0xc0, GET_AD_STATUS, 0, 0, status)
            if err != nil {
                return nil, false, err
            }
            if (status[0] & LAST_FRAME) == LAST_FRAME {
                last = true
                break
            }
        } else {
            rx = rx + l
        }
        time.Sleep(5*time.Millisecond)
    }
    return data[:rx], last, nil
}

func (s *Satoshi) readHeader() ([]byte, error) {
    rx := 0
    status := make([]byte,1)
    data := make([]byte, dstar.DSTAR_HEADER_SIZE + 8)
    for rx < dstar.DSTAR_HEADER_SIZE {
        l, err := s.dev.Control(0xc0, GET_HEADER, 0, 0, data[rx:rx+8])
        if err != nil {
            return nil, err
        }
        if l == 0 {
            l, err = s.dev.Control(0xc0, GET_AD_STATUS, 0, 0, status)
            if err != nil {
                return nil, err
            }
            if ((status[0]) & COS_STAT) == COS_STAT {
                rx = 0
            }
        } else {
            rx = rx + l
        }
        time.Sleep(10*time.Millisecond)
    }

    _, err := s.dev.Control(0xc0, GET_AD_STATUS, 0, 0, status)
    if err != nil {
        return nil, err
    }
    if ((status[0] & CRC_ERROR) == CRC_ERROR) {
        return nil, errors.New("CRC_ERROR")
    }

    return data[:dstar.DSTAR_HEADER_SIZE], nil
}

func (s *Satoshi) readHdrCont(exit bool) {
    for {
        d, err := s.readHeader()
        if err != nil {
            s.Erc <- errors.New(fmt.Sprintf(
                "Satoshi readHdrCont(): " + err.Error()))
        } else {
            dsh := dstar.DSHeader{}
            err = dsh.Fill(d)
            if err != nil {
                if s.Cb.SatoshiDebug == "yes" {
                    s.Erc <- errors.New("Satoshi readHdrCont(): Fill() " +
                        err.Error())
                }
            } else {
                // cache module for a while ..
                s.SetModule(s.GetModule())
                if s.Cb.SatoshiDebug == "yes" {
                    s.Erc <- errors.New(fmt.Sprintf(
                        "Satoshi readHdrCont(): Good Header: %+v",dsh))
                }
                if strings.TrimSpace(dsh.DepaRPT) == "DIRECT" {
                    dsh.Nat(s.Cb.ModulesOwner,s.Cb.ModulesOwner,s.Module(),"G")
                }
                s.SetLastHeader(&dsh)
                s.SetLastHeaderTime(time.Now())
                s.Seq = 0
                if exit {
                    break
                }
            }
        }
        time.Sleep(100*time.Millisecond)
    }
}

func (s *Satoshi) readDataCont(ret chan<- *dstar.DSXFrame, exit bool) {
    for {
        now := time.Now()
        d, last, err := s.readData()
        if err != nil {
            s.Erc <- errors.New(fmt.Sprintf(
                "Satoshi readDataCont(): " + err.Error()))
        } else if len(d) > 0 {
            if last {
                // the stupid satoshi board gives me also the last 3 sync bytes
                d = append(d,make([]byte,dstar.DSTAR_FRAME_SIZE-len(d))...)
            }
            dsf := dstar.DSFrame{}
            err = dsf.Fill(d)
            if err != nil {
                if s.Cb.SatoshiDebug == "yes" {
                    s.Erc <- errors.New("Satoshi readDataCont(): Fill() " +
                        err.Error())
                }
            } else {
                if s.Cb.SatoshiDebug == "yes" {
                    s.Erc <- errors.New(fmt.Sprintf(
                        "Satoshi readDataCont(): Good Frame: %x",dsf))
                }
                if s.LastHeader() != nil {
                    dsxf := dstar.DSXFrame{
                        Hdr: *s.LastHeader(),
                        Frm: dsf,
                        FromMod: s.Module(),
                        LastFrame: last,
                        Seq: s.Seq,
                    }
                    ret <- &dsxf
                    // no lock here .. too time expensive
                    s.SetUsedBy(fmt.Sprintf("%-7s%s",
                        s.Cb.ModulesOwner,s.Module()))
                    s.SetUsedByTime(now)
                    s.SetLastHeaderTime(now)
                    s.Seq = s.Seq + 1
                    if s.Seq == 21 {
                        s.Seq = 0
                    }
                }
                if exit && last {
                    break
                }
            }
        }
        time.Sleep(5*time.Millisecond)
    }
}

func (s *Satoshi) readCont(ret chan<- *dstar.DSXFrame) {
    for {
        s.readHdrCont(true)
        s.readDataCont(ret, true)
    }
}

func (s *Satoshi) StartReadLoops(data chan<- *dstar.DSXFrame) error {
    if s.Cb.SATOSHI_2THREADS == "yes" {
        go s.readHdrCont(false)
        go s.readDataCont(data, false)
    } else {
        go s.readCont(data)
    }
    return nil
}

func (s *Satoshi) txTimeout(reset <-chan bool) {
    tot := 0
    for {
        select {
            case <-reset:
                tot = 0
            case <-time.After(100*time.Millisecond):
                tot = tot + 1
        }
        if tot == 10 { // after 1 sec, release PTT (just for safety..)
            s.FIFOClean() // this is just in case I havent got the lastframe..
            s.SetPTT(false)
        }
        now := time.Now()
        if s.UsedBy() != "" &&
            s.UsedByTime().Add(15*time.Second).Before(now) {
            s.UsedByMutexLock()
            s.SetUsedBy("")
            s.UsedByMutexUnlock()
        }
        if (s.LastHeader() != nil) &&
            (s.LastHeaderTime().Add(5*time.Second).Before(now)) {
            s.SetLastHeader(nil)
        }
    }
}

func (s *Satoshi) txLoop(txt chan<- bool) {
    for {
        f := <-s.Dch
        ptt, err := s.GetPTT()
        if (err == nil) && (ptt == false) {
            time.Sleep(50*time.Millisecond)
            if s.Cb.SatoshiDebug == "yes" {
                s.Erc <- errors.New(fmt.Sprintf(
                    "Satoshi txLoop: TX HDR: %+v",f.Hdr))
            }
            s.writeHeader(f.Hdr.Get())
            time.Sleep(1*time.Millisecond)
            s.SetPTT(true)
            time.Sleep(20*time.Millisecond)
        }
        err = s.writeData(f.Frm.Get())
        if err != nil {
            // if you see this in the logs, increase the 20ms above
            s.Erc <- errors.New("Satoshi txLoop: " + err.Error())
        }
        txt <- true
        for {
            free, _ := s.getSpace()
            if free < 12 {
                time.Sleep(10*time.Millisecond)
            } else {
                break
            }
        }
    }
}

func (s *Satoshi) writeHeader(hdr []byte) {
    s.dev.Control(0x40,SET_FLAGS, 0, 0, hdr[:3])
    s.dev.Control(0x40,SET_DestRPT, 0, 0, hdr[3:11])
    s.dev.Control(0x40,SET_DepaRPT, 0, 0, hdr[11:19])
    s.dev.Control(0x40,SET_YoCALL, 0, 0, hdr[19:27])
    s.dev.Control(0x40,SET_MyCALL1, 0, 0, hdr[27:35])
    s.dev.Control(0x40,SET_MyCALL2, 0, 0, hdr[35:39])
}

func (s *Satoshi) getSpace() (byte, error) {
    space := make([]byte, 1)
    _, err := s.dev.Control(0xc0, GET_REMAINSPACE, 0, 0, space)
    if err != nil {
        return 0, err
    }
    return space[0], nil
}

func (s *Satoshi) writeData(data []byte) error {
    le := len(data)
    if le <= 8 {
        _, err := s.dev.Control(0x40, PUT_DATA, 0, 0, data)
        return err
    }
    rl := le - 8
    if rl > 4 {
        rl = 4
    }
    _, err := s.dev.Control(0x40, PUT_DATA, 0, 0, data[:8])
    if err != nil {
        return err
    }
    time.Sleep(3*time.Millisecond)
    _, err = s.dev.Control(0x40, PUT_DATA, 0, 0, data[8:8+rl])
    return err
}

func (s *Satoshi) IsDuplex() bool {
    hexprod := fmt.Sprintf("0x%04x",int(s.dev.Descriptor.Product))
    addmods := strings.Split(s.Cb.SATOSHI_MODULES,",")
    for _, am := range addmods {
        a := strings.Split(am,":")
        if a[0] == hexprod {
            return a[2] == "D"
        }
    }
    // TODO fix false also if module not found .. 
    return false
}

func (s *Satoshi) GetModule() string {
    // conf is "0x300:A:S,0x301:B:D"
    hexprod := fmt.Sprintf("0x%04x",int(s.dev.Descriptor.Product))
    addmods := strings.Split(s.Cb.SATOSHI_MODULES,",")
    for _, am := range addmods {
        a := strings.Split(am,":")
        if a[0] == hexprod {
            return a[1]
        }
    }
    return ""
}

func (s *Satoshi) StartWriteLoops() error {
    b := make(chan bool)
    // FIXME do I need this buffered?
    s.Dch = make(chan *dstar.DSXFrame, 50) // 50 pkts = 1 sec
    go s.txTimeout(b)
    go s.txLoop(b)
    return nil
}

type SatoshiCtx struct {
    devices.GenericCtx
}

func (sctx *SatoshiCtx) Init(erc chan<- error) error {
    sctx.SetCtx(usb.NewContext())
    devs, err := sctx.GetDevices(VENDOR_ID)
    if err != nil {
        return err
    }
    ss := make([]devices.IDevice,0)
    for _, dev := range devs {
        dev.SetConfig(1)
        dev.Control(0x40,SET_AD_INIT,0,0,make([]byte,0))
        s := Satoshi{
            dev: dev,
            GenericDevice: devices.GenericDevice{ Thr: 5, Cb: sctx.Cb, Erc: erc},
        }
        s.FIFOInit()
        s.SetPTT(false)
        s.GetVersion()
        ss = append(ss, &s)
    }
    sctx.SetSs(ss)
    return nil
}

