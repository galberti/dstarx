
package dsnetwork

/*
19:29:51.310380 IP 127.0.0.1.20010 > 127.0.0.1.20011: UDP, length 34
        0x0000:  4500 003e 775d 4000 4011 c54f 7f00 0001  E..>w]@.@..O....
        0x0010:  7f00 0001 4e2a 4e2b 002a fe3d 4453 5250  ....N*N+.*.=DSRP
        0x0020:  004c 696e 6b69 6e67 2074 6f20 5852 4630  .Linking.to.XRF0
        0x0030:  3639 2041 2003 5852 4630 3639 2041       69.A..XRF069.A

19:35:37.409877 IP 127.0.0.1.20010 > 127.0.0.1.20011: UDP, length 34
        0x0000:  4500 003e 0058 4000 4011 3c55 7f00 0001  E..>.X@.@.<U....
        0x0010:  7f00 0001 4e2a 4e2b 002a fe3d 4453 5250  ....N*N+.*.=DSRP
        0x0020:  004e 6f74 206c 696e 6b65 6420 2020 2020  .Not.linked.....
        0x0030:  2020 2020 2000 2020 2020 2020 2020       ..............
*/

import (
    "fmt"
    "net"
    "strings"
    "errors"
    "time"
    "bytes"
//    "sync"
    "gitlab.com/galberti/dstarx/devices"
    "gitlab.com/galberti/dstarx/dstar"
)

const (
    DSN_HEADER_HDR_SIZE = 8
    DSN_HEADER_SIZE = 49 // 41 + 8
    DSN_VOICE_HDR_SIZE = 9
    DSN_VOICE_SIZE = 21 // 12 + 9

    TYPE_HEADER = 0x20
    TYPE_VOICE  = 0x21
)

type DSNHeaderHeader struct {
    Pre [4]byte     // always DSRP
    Type byte       // 0x20 header, 0x21 data (voice)
    Session [2]byte // session, still to find a good use..
    Zero byte       // always zero
}

func (dsnhh *DSNHeaderHeader) Fill(raw []byte) error {
    if len(raw) != DSN_HEADER_HDR_SIZE {
        return errors.New("Wrong raw size")
    }
    copy(dsnhh.Pre[:], raw[0:4])
    dsnhh.Type = raw[4]
    copy(dsnhh.Session[:], raw[5:7])
    dsnhh.Zero = raw[7]
    return nil
}

func (dsnhh *DSNHeaderHeader) Get() []byte {
    ret := make([]byte, DSN_HEADER_HDR_SIZE)
    copy(ret[0:4],dsnhh.Pre[:])
    ret[4] = dsnhh.Type
    copy(ret[5:7],dsnhh.Session[:])
    ret[7] = dsnhh.Zero
    return ret
}

type DSNVoiceHeader struct {
    Pre [4]byte     // always DSRP
    Type byte       // 0x20 header, 0x21 data (voice)
    Session [2]byte // session, still to find a good use..
    Count byte      // usual 0x00-0x14, |=0x40 if end
    Zero byte       // always zero
}

func (dsnvh *DSNVoiceHeader) Fill(raw []byte) error {
    if len(raw) != DSN_VOICE_HDR_SIZE {
        return errors.New("Wrong raw size")
    }
    copy(dsnvh.Pre[:], raw[0:4])
    dsnvh.Type = raw[4]
    copy(dsnvh.Session[:], raw[5:7])
    dsnvh.Count = raw[7]
    dsnvh.Zero = raw[8]
    return nil
}

func (dsnvh *DSNVoiceHeader) Get() []byte {
    ret := make([]byte, DSN_VOICE_HDR_SIZE)
    copy(ret[0:4],dsnvh.Pre[:])
    ret[4] = dsnvh.Type
    copy(ret[5:7],dsnvh.Session[:])
    ret[7] = dsnvh.Count
    ret[8] = dsnvh.Zero
    return ret
}

type DSNHeaderFrame struct {
    dsnhdr DSNHeaderHeader
    dshdr dstar.DSHeader
}

func (dsnhf *DSNHeaderFrame) Fill(raw []byte) error {
    err := dsnhf.dsnhdr.Fill(raw[:DSN_HEADER_HDR_SIZE])
    if err != nil {
        return err
    }
    err = dsnhf.dshdr.Fill(raw[DSN_HEADER_HDR_SIZE:])
    if err != nil {
        return err
    }
    return nil
}

func (dsnhf *DSNHeaderFrame) Get() []byte {
    ret := make([]byte, DSN_HEADER_SIZE)
    copy(ret[:DSN_HEADER_HDR_SIZE], dsnhf.dsnhdr.Get())
    copy(ret[DSN_HEADER_HDR_SIZE:], dsnhf.dshdr.Get())
    return ret
}

type DSNVoiceFrame struct {
    dsnfrm DSNVoiceHeader
    dsfrm dstar.DSFrame
}

func (dsnvf *DSNVoiceFrame) Fill(raw []byte) error {
    err := dsnvf.dsnfrm.Fill(raw[:DSN_VOICE_HDR_SIZE])
    if err != nil {
        return err
    }
    err = dsnvf.dsfrm.Fill(raw[DSN_VOICE_HDR_SIZE:])
    if err != nil {
        return err
    }
    return nil
}

func (dsnvf *DSNVoiceFrame) Get() []byte {
    ret := make([]byte, DSN_VOICE_SIZE)
    copy(ret[:DSN_VOICE_HDR_SIZE], dsnvf.dsnfrm.Get())
    copy(ret[DSN_VOICE_HDR_SIZE:], dsnvf.dsfrm.Get())
    return ret
}

type DSNDev struct {
    devices.GenericDevice
    devConn *net.UDPConn
    remAdd *net.UDPAddr
    ptt bool
}

func (d *DSNDev) readData(rd chan<- *dstar.DSXFrame) {
    buf := make([]byte,60)
    for {
        l, addr, err := d.devConn.ReadFromUDP(buf)
        if err != nil {
            d.Erc <- errors.New("" + err.Error())
            return
        }
        if !bytes.Equal(addr.IP, d.remAdd.IP) || (addr.Port != d.remAdd.Port) {
            fmt.Printf("ADDR DIVERSO\n%v %v\n",addr, d.remAdd)
        }
        switch l {
            case DSN_HEADER_SIZE:
                dsnhdr := DSNHeaderFrame{}
                err := dsnhdr.Fill(buf[:l])
                if err != nil {
                    fmt.Printf("wrong header")
                    continue
                }
                hdr := dsnhdr.dshdr
                d.SetModule(d.GetModule())
                if d.Cb.DsnDebug == "yes" {
                    d.Erc <- errors.New(fmt.Sprintf(
                        "dsnetwork readData(): Good Header: %+v",hdr))
                }
                if strings.TrimSpace(hdr.DepaRPT) == "DIRECT" {
                    hdr.Nat(d.Cb.ModulesOwner,d.Cb.ModulesOwner,d.Module(),"G")
                }
                d.SetLastHeader(&hdr)
                d.SetLastHeaderTime(time.Now())
            case DSN_VOICE_SIZE:
                frm := DSNVoiceFrame{}
                err := frm.Fill(buf[:l])
                if err != nil {
                    fmt.Printf("wrong frame")
                    continue
                }
                if d.LastHeader() != nil {
                    dsf := frm.dsfrm
                    last := false
                    if (frm.dsnfrm.Count & 0x40) != 0 {
                        last = true
                    }
                    dsxf := dstar.DSXFrame{
                        Hdr: *d.LastHeader(),
                        Frm: dsf,
                        FromMod: d.Module(),
                        LastFrame: last,
                        Seq: frm.dsnfrm.Count,
                    }
                    rd <- &dsxf
                    d.SetUsedBy(fmt.Sprintf("%-7s%s",
                        d.Cb.ModulesOwner,d.Module()))
                    now := time.Now()
                    d.SetUsedByTime(now)
                    d.SetLastHeaderTime(now)
                }
        }
    }
}

func (d *DSNDev) txTimeout(reset <-chan bool) {
    tot := 0
    for {
        select {
            case <-reset:
                tot = 0
            case <-time.After(100*time.Millisecond):
                tot = tot + 1
        }
        if tot == 10 { // after 1 sec, release PTT (just for safety..)
            d.FIFOClean() // this is just in case I havent got the lastframe..
            d.SetPTT(false)
        }
        now := time.Now()
        if d.UsedBy() != "" &&
            d.UsedByTime().Add(15*time.Second).Before(now) {
            d.UsedByMutexLock()
            d.SetUsedBy("")
            d.UsedByMutexUnlock()
        }
        if (d.LastHeader() != nil) &&
            (d.LastHeaderTime().Add(5*time.Second).Before(now)) {
            d.SetLastHeader(nil)
        }
    }
}

func (d *DSNDev) txLoop(txt chan<- bool) {
    for {
        f := <-d.Dch
        ptt, _ := d.GetPTT()
        if ! ptt {
            // send header
            /*
            bhdr := make([]byte, 0)
            bhdr = append(bhdr, dstar.InitSync[:]...)
            bhdr = append(bhdr, f.Hdr.GetNetworkBits()[:]...)
            u := dstar.SliceSwapBits(dstar.BitsToBytes(bhdr))
            d.WriteData(u)
            */
            d.SetPTT(true)
        }
        if f.Seq == 0 {
            // send header
            oh := DSNHeaderFrame{}
            copy(oh.dsnhdr.Pre[:], "DSRP")
            oh.dsnhdr.Type = TYPE_HEADER
            oh.dsnhdr.Session[0] = 0
            oh.dsnhdr.Session[1] = 0
            oh.dsnhdr.Zero = 0
            oh.dshdr = f.Hdr
            _, err := d.devConn.WriteToUDP(oh.Get(),d.remAdd)
            if err != nil {
                d.Erc <- errors.New("DSNDev txLoop (hdr): " + err.Error())
            }
        }
        of := DSNVoiceFrame{}
        copy(of.dsnfrm.Pre[:], "DSRP")
        of.dsnfrm.Type = TYPE_VOICE
        // FIXME is this ok?
        of.dsnfrm.Session[0] = 0
        of.dsnfrm.Session[1] = 0
        of.dsnfrm.Count = f.Seq
        of.dsnfrm.Zero = 0
        of.dsfrm = f.Frm

        _, err := d.devConn.WriteToUDP(of.Get(),d.remAdd)
        if err != nil {
            d.Erc <- errors.New("DSNDev txLoop: " + err.Error())
        }
        txt <- true
    }
}

func (d *DSNDev) StartWriteLoops() error {
    b := make(chan bool)
    // FIXME do I need this buffered?
    d.Dch = make(chan *dstar.DSXFrame, 50) // 50 pkts = 1 sec
    go d.txTimeout(b)
    go d.txLoop(b)
    return nil
}

func (d *DSNDev) StartReadLoops(exd chan<- *dstar.DSXFrame) error {
    go d.readData(exd)
    //go d.bitStream(rawdata, syncdata)
    return nil
}

func (d *DSNDev) GetVersion() (string, error) {
    return "Who knows..", nil
}

func (d *DSNDev) SetPTT(s bool) error {
    d.ptt = s
    return nil
}

func (d *DSNDev) GetPTT() (bool, error) {
    return d.ptt, nil
}

func (d *DSNDev) GetModule() string {
    // COMXX:A:Hz:Hz:lv,.. (serial:mod:rxf:txf:pwr)
    ip := d.remAdd.IP.String()
    addmods := strings.Split(d.Cb.DSN_MODULES,",")
    for _, am := range addmods {
        a := strings.Split(am,":")
        if a[1] == ip {
            return a[3]
        }
    }
    return ""
}

func (d *DSNDev) Close() error {
    return nil
}

func (d *DSNDev) IsDuplex() bool {
    return true
}

type DSNDevCtx struct {
    devices.GenericCtx
}

func (d *DSNDevCtx) Init(erc chan<- error) error {
    devs := strings.Split(d.Cb.DSN_MODULES, ",")
    if len(devs[0]) == 0 {
        return nil
    }
    ss := make([]devices.IDevice,0)
    for _, dev := range devs {
        devs := strings.Split(dev, ":")
        a, err := net.ResolveUDPAddr("udp",
            fmt.Sprintf(":%s", devs[0]))
        if err != nil {
            return err
        }
        nd := DSNDev{
            GenericDevice: devices.GenericDevice{ Thr: 5, Cb: d.Cb, Erc: erc},
        }
        nd.devConn, err = net.ListenUDP("udp", a)
        if err != nil {
            return err
        }
        nd.remAdd, err = net.ResolveUDPAddr("udp",
            fmt.Sprintf("%s:%s", devs[1], devs[2]))
        if err != nil {
            return err
        }
        nd.SetModule(devs[3])
        nd.FIFOInit()
        ss = append(ss, &nd)
    }
    d.SetSs(ss)
    return nil
}


