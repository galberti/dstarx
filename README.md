```
DIR=$HOME

if [ ! -f $DIR/goroot/go/bin/go ]
then
    cd
    wget https://dl.google.com/go/go1.11.1.linux-armv6l.tar.gz
    mkdir -p $DIR/goroot && cd $DIR/goroot
    tar xvzf $HOME/go1.11.1.linux-armv6l.tar.gz
fi

export GOPATH=$DIR/go
export PATH=$DIR/goroot/go/bin:$PATH
[ -d $DIR/go/src/gitlab.com/galberti/dstarx ] && rm -rf $DIR/go/src/gitlab.com/galberti/dstarx
git clone https://gitlab.com/galberti/dstarx $DIR/go/src/gitlab.com/galberti/dstarx
cd $DIR/go/src/gitlab.com/galberti/dstarx
make

```