
package dv4mini

import (
    "fmt"
    "strings"
    "errors"
    "time"
    "bytes"
    "sync"
    "gitlab.com/galberti/dstarx/devices"
    "gitlab.com/galberti/dstarx/dstar"
    "github.com/tarm/serial"
)

const (
    SETADFQRG = 1
    SETADFMODE = 2
    FLUSHTXBUF = 3
    ADFWRITE = 4
    ADFWATCHDOG = 5
    ADFGETDATA = 7
    ADFGREENLED = 8
    ADFSETPOWER = 9
    ADFDEBUG = 10
    ADFSETSEED = 17
    ADFVERSION = 18
    ADFSETTXBUF = 19
)

const (
    MATCH_INIT = iota
    MATCH_SYNC_TERM
)

var Pre = []byte{0x71, 0xfe, 0x39, 0x1d}

// aab468
var Sync = []byte{
    1,0,1,0, 1,0,1,0, 1,0,1,1, 0,1,0,0, 0,1,1,0, 1,0,0,0,
}

// 55555555c87a
var Term = []byte{
    0,1,0,1, 0,1,0,1, 0,1,0,1, 0,1,0,1, 0,1,0,1, 0,1,0,1, 0,1,0,1, 0,1,0,1,
    1,1,0,0, 1,0,0,0, 0,1,1,1, 1,0,1,0,
}

type DV4Command struct {
    pre []byte
    command byte
    len byte
    data []byte
}

func (c *DV4Command) Get() []byte {
    r := make([]byte, 6 + c.len)
    copy(r[0:4], c.pre[:])
    r[4] = c.command
    r[5] = c.len
    if c.len > 0 {
        copy(r[6:],c.data[:])
    }
    return r
}

type Dv4mini struct {
    devices.GenericDevice
    c *serial.Config
    s *serial.Port
    sm sync.Mutex
    btube []byte
    ptt bool
    match_status byte
    Seq byte
    receiving bool
    synced bool
}

func (d *Dv4mini) Ping() (bool, uint16) {
    c := DV4Command{
        pre: Pre,
        len: 0,
        command: ADFWATCHDOG,
    }
    rx := make([]byte, 256)
    d.sm.Lock()
    n, err := d.s.Write(c.Get())
    if err != nil {
        d.Erc <- err
        d.sm.Unlock()
        return false, 0
    }
    n, err = d.s.Read(rx)
    d.sm.Unlock()
    if err != nil {
        d.Erc <- err
        return false, 0
    }
    var r uint16
    if n >= 8 {
        //fmt.Printf("%d %d\n", rx[6], rx[7])
        // this should be to compute RSSI, but rx[6] is always 255 .. wtf?
        // r = (uint16(rx[7]) << 8) | uint16(rx[6])
        r = uint16(rx[7])
        return true, r
    }
    return false, 0
}

func (d *Dv4mini) SetDstar() error {
    c := DV4Command{
        pre: Pre,
        len: 1,
        command: SETADFMODE,
    }
    c.data = make([]byte, 1)
    c.data[0] = 'D'
    _, err := d.s.Write(c.Get())
    return err
}

func (d *Dv4mini) SetFreq(rx, tx uint32) error {
    if (rx < 430000000) || (rx > 440000000) {
        return errors.New("RX freq is out of range (must be 430-440 millions)")
    }
    if (tx < 430000000) || (tx > 440000000) {
        return errors.New("TX freq is out of range (must be 430-440 millions)")
    }
    c := DV4Command{
        pre: Pre,
        len: 8,
        command: SETADFQRG,
    }
    c.data = make([]byte, 8)
    c.data[0] = byte((rx & 0xff000000) >> 24)
    c.data[1] = byte((rx & 0x00ff0000) >> 16)
    c.data[2] = byte((rx & 0x0000ff00) >> 8)
    c.data[3] = byte(rx & 0x000000ff)
    c.data[4] = byte((tx & 0xff000000) >> 24)
    c.data[5] = byte((tx & 0x00ff0000) >> 16)
    c.data[6] = byte((tx & 0x0000ff00) >> 8)
    c.data[7] = byte(tx & 0x000000ff)
    _, err := d.s.Write(c.Get())
    return err
}

func (d *Dv4mini) WriteData(data []byte) error {
    if (len(data) < 1) || (len(data) > 245) {
        return errors.New("data size is wrong (must be 1-245)")
    }
    c := DV4Command{
        pre: Pre,
        len: byte(len(data)),
        command: ADFWRITE,
    }
    c.data = make([]byte, len(data))
    copy(c.data, data[:])
    _, err := d.s.Write(c.Get())
    return err
}

func (d *Dv4mini) txTimeout(reset <-chan bool) {
    tot := 0
    for {
        select {
            case <-reset:
                tot = 0
            case <-time.After(100*time.Millisecond):
                tot = tot + 1
        }
        if tot == 10 { // after 1 sec, release PTT (just for safety..)
            d.FIFOClean() // this is just in case I havent got the lastframe..
            d.SetPTT(false)
        }
        now := time.Now()
        if d.UsedBy() != "" &&
            d.UsedByTime().Add(15*time.Second).Before(now) {
            d.UsedByMutexLock()
            d.SetUsedBy("")
            d.UsedByMutexUnlock()
        }
        if (d.LastHeader() != nil) &&
            (d.LastHeaderTime().Add(5*time.Second).Before(now)) {
            d.SetLastHeader(nil)
        }
    }
}

func (d *Dv4mini) txLoop(txt chan<- bool) {
    for {
        f := <-d.Dch
        ptt, _ := d.GetPTT()
        if ! ptt {
            // send header
            bhdr := make([]byte, 0)
            bhdr = append(bhdr, dstar.InitSync[:]...)
            bhdr = append(bhdr, f.Hdr.GetNetworkBits()[:]...)
            u := dstar.SliceSwapBits(dstar.BitsToBytes(bhdr))
            d.WriteData(u)
            d.SetPTT(true)
        }
        err := d.WriteData(f.Frm.Get())
        if err != nil {
            d.Erc <- errors.New("Dv4mini txLoop: " + err.Error())
        }
        txt <- true
    }
}

func (d *Dv4mini) match() bool {
    switch d.match_status {
    case MATCH_INIT:
        for i := 0; i < 39; i++ {
            if (d.btube[i] ^ dstar.InitSync[i+237]) == 1 {
            //if (d.btube[i] ^ dstar.InitSync[i+45]) == 1 {
                return false
            }
        }
        d.match_status = MATCH_SYNC_TERM
        return true
    case MATCH_SYNC_TERM:
        for i := 0; i < 24; i++ {
            if (d.btube[i] ^ Sync[i]) == 1 {
                return false
            }
        }
        d.match_status = MATCH_INIT
        return true
    }
    return false
}

func (d *Dv4mini) swapbits(b byte) byte {
    ret := ((b >> 7) & 0x01)
    ret |= ((b >> 5) & 0x02)
    ret |= ((b >> 3) & 0x04)
    ret |= ((b >> 1) & 0x08)
    ret |= ((b << 1) & 0x10)
    ret |= ((b << 3) & 0x20)
    ret |= ((b << 5) & 0x40)
    ret |= ((b << 7) & 0x80)
    return ret
}

func (d *Dv4mini) packer(sd <-chan byte, exd chan<- *dstar.DSXFrame) {
    // receives bytes, change endianess, sends frames
    // look for sync frames, when not found set async mode and trigger
    // again match()
    rx := 0
    np := -1
    var buf [12]byte
    term := 0
    outsync := 0
    for {
        data := d.swapbits(<-sd)
        if d.synced {
            buf[rx] = data
            rx += 1
        }
        if rx == 12 {
            np++
            last := false
            if (np % 21) == 0 {
                if ! bytes.Equal(buf[9:12], dstar.Sync_seq[:]) {
                    outsync++
                    if outsync == 3 {
                        outsync = 0
                        d.Erc <- errors.New("Dv4mini packer(): OUT OF SYNC")
                        d.SetLED(false)
                        d.synced = false
                    }
                } else {
                    outsync = 0
                }
            }
            if (term == 0) && bytes.Equal(buf[9:12], dstar.Term_seq[0:3]) {
                term = 1
                if d.Cb.Dv4miniDebug == "yes" {
                    d.Erc <- errors.New("Dv4mini packer(): TERM1")
                }
            }
            if (term == 1) && bytes.Equal(buf[0:3], dstar.Term_seq[3:6]) {
                if d.Cb.Dv4miniDebug == "yes" {
                    d.Erc <- errors.New("Dv4mini packer(): TERM2")
                }
                d.SetLED(false)
                d.match_status = MATCH_INIT
                last = true
                d.synced = false
                term = 0
                rx = 0
                np = -1
            }
            if d.LastHeader() != nil {
                dsf := dstar.DSFrame{}
                dsf.Fill(buf[:])
                dsxf := dstar.DSXFrame{
                    Hdr: *d.LastHeader(),
                    Frm: dsf,
                    FromMod: d.Module(),
                    LastFrame: last,
                    Seq: d.Seq,
                }
                exd <- &dsxf
                d.SetUsedBy(fmt.Sprintf("%-7s%s",
                    d.Cb.ModulesOwner,d.Module()))
                now := time.Now()
                d.SetUsedByTime(now)
                d.SetLastHeaderTime(now)
                d.Seq = d.Seq + 1
                if d.Seq == 21 {
                    d.Seq = 0
                }
            }
            rx = 0
        }
    }
}

func (d *Dv4mini) decode(hdr []byte) {
    rxhdr := dstar.DSHeader{}
    err := rxhdr.Fill(dstar.SliceSwapBits(dstar.BitsToBytes(
        dstar.FECDecode(dstar.Deinterleave(dstar.Scramble(hdr)))[:328])))
    if err != nil {
        d.Erc <- errors.New(fmt.Sprintf(
            "Dv4mini decode(): " + err.Error()))
    } else {
        d.SetModule(d.GetModule())
        if d.Cb.Dv4miniDebug == "yes" {
            d.Erc <- errors.New(fmt.Sprintf(
                "Dv4mini decode(): Good Header: %+v",rxhdr))
        }
        if strings.TrimSpace(rxhdr.DepaRPT) == "DIRECT" {
            rxhdr.Nat(d.Cb.ModulesOwner,d.Cb.ModulesOwner,d.Module(),"G")
        }
        d.SetLastHeader(&rxhdr)
        d.SetLastHeaderTime(time.Now())
        d.Seq = 0
    }
}

func (d *Dv4mini) bitStream(rd <-chan byte, sd chan<- byte) {
    var offset uint8
    var reminder byte
    var header [660]byte
    for {
        b := <-rd
        if ! d.synced {
            for i := 0; i < 8; i++ {
                copy(d.btube, d.btube[1:])
                if d.match_status == MATCH_INIT {
                    d.btube[len(d.btube) - 1] = (b >> uint8(7 - i)) & 0x01
                    if d.match() {
                        if d.Cb.Dv4miniDebug == "yes" {
                            d.Erc <- errors.New("Dv4mini packer(): INIT SYNC")
                        }
                        copy(header[:], d.btube[39:])
                        d.decode(header[:])
                        d.synced = true
                        offset = uint8(i + 1)
                        reminder = (b << offset)
                        d.SetLED(true)
                        break
                    }
                } else if d.match_status == MATCH_SYNC_TERM {
                    d.btube[23] = (b >> uint8(7 - i)) & 0x01
                    if d.match() {
                        if d.Cb.Dv4miniDebug == "yes" {
                            d.Erc <- errors.New("Dv4mini packer(): RESYNC")
                        }
                        d.synced = true
                        offset = uint8(i + 1)
                        reminder = (b << offset)
                        d.SetLED(true)
                        break
                    } else if ! d.receiving {
                        d.match_status = MATCH_INIT
                        if d.Cb.Dv4miniDebug == "yes" {
                            d.Erc <- errors.New("Dv4mini packer(): RX TMOUT")
                        }
                        // LED is already off
                    }
                }
            }
        } else {
            bb := reminder | (b >> (8 - offset))
            sd <- bb
            reminder = (b << offset)
        }
    }
}

func (d *Dv4mini) readData(rd chan<- byte) {
    c := DV4Command{
        pre: Pre,
        len: 0,
        command: ADFGETDATA,
    }
    rx := make([]byte, 256)
    for {
        d.sm.Lock()
        _, err := d.s.Write(c.Get())
        if err != nil {
            d.Erc <- err
        }
        _, err = d.s.Read(rx)
        d.sm.Unlock()
        if err != nil {
            d.Erc <- err
        }
        // check for claimed length. If acceptable, use it
        cl := int(rx[5])
        // N = nibble
        // 55557650X        [164N  ][voice][sync]
        // aaaaeca0Y Y=0..1 [163N3b][][]
        // 55555d94Y Y=0..3 [163N2b]
        // aaaabb28Y Y=0..7 [163N1b]
        // Bits arrive in reverse order here! Ex.: 
        // aab468 10101010 10110100 01101000 sync OK
        // 552d16 01010101 00101101 00010110 real sync OK
        for _, c := range rx[6:6+cl] {
            rd <- c
        }
        /*
            if d.LastHeader() == nil {
                // send header, set header
            }
            dsf := dstar.DSFrame{}
            err = dsf.Fill(dsf)
            if err != nil {
                if s.Cb.SatoshiDebug == "yes" {
                    erc <- errors.New("Satoshi readDataCont(): Fill() " +
                        err.Error())
                }
            } else {
                dsxf := dstar.DSXFrame{
                    Hdr: *d.LastHeader,
                    Frm: dsf,
                    FromMod: d.Module(),
                    LastFrame: last,
                    Seq: d.Seq,
                }
                exd <- &dsxf
                d.SetUsedBy(fmt.Sprintf("%-7s%s",
                    d.Cb.ModulesOwner,s.Module))
                d.SetUsedByTime(now)
                d.SetLastHeaderTime(now)
                d.Seq = d.Seq + 1
                if d.Seq == 21 {
                    d.Seq = 0
                }
        }*/
        time.Sleep(100*time.Millisecond)
    }
}

func (d *Dv4mini) SetBuf(ms byte) error {
    if (ms < 1) || (ms > 15) {
        return errors.New("buf size is wrong (must be 1-15)")
    }
    c := DV4Command{
        pre: Pre,
        len: 1,
        command: ADFSETTXBUF,
    }
    c.data = make([]byte, 1)
    c.data[0] = ms
    _, err := d.s.Write(c.Get())
    return err
}

func (d *Dv4mini) SetPower(lvl byte) error {
    if (lvl < 1) || (lvl > 9) {
        return errors.New("tx level is wrong (must be 1-9)")
    }
    c := DV4Command{
        pre: Pre,
        len: 1,
        command: ADFSETPOWER,
    }
    c.data = make([]byte, 1)
    c.data[0] = lvl
    _, err := d.s.Write(c.Get())
    return err
}

func (d *Dv4mini) SetLED(status bool) error {
    c := DV4Command{
        pre: Pre,
        len: 1,
        command: ADFGREENLED,
    }
    c.data = make([]byte, 1)
    if status {
        c.data[0] = 1
    } else {
        c.data[0] = 0
    }
    rx := make([]byte, 256)
    d.sm.Lock()
    _, err := d.s.Write(c.Get())
    d.s.Read(rx)
    d.sm.Unlock()
    return err
}

func (d *Dv4mini) RSSILoop() {
    for {
        if ! d.ptt {
            success, rssi := d.Ping()
            // rssi is <150 with no signals .. 160 should be ok
            if success && rssi > 160 {
                d.receiving = true
            } else {
                d.receiving = false
            }
        }
        time.Sleep(500*time.Millisecond)
    }
}

func (d *Dv4mini) SetPTT(s bool) error {
    d.ptt = s
    return nil
}

func (d *Dv4mini) GetPTT() (bool, error) {
    return d.ptt, nil
}

func (d *Dv4mini) GetVersion() (string, error) {
    return "Who knows..", nil
}

func (d *Dv4mini) Close() error {
    return nil
}

func (d *Dv4mini) StartReadLoops(exd chan<- *dstar.DSXFrame) error {
    rawdata := make(chan byte)
    syncdata := make(chan byte)
    go d.readData(rawdata)
    go d.bitStream(rawdata, syncdata)
    go d.packer(syncdata, exd)
    return nil
}

func (d *Dv4mini) GetModule() string {
    // COMXX:A:Hz:Hz:lv,.. (serial:mod:rxf:txf:pwr)
    serial := d.c.Name
    addmods := strings.Split(d.Cb.DV4MINI_MODULES,",")
    for _, am := range addmods {
        a := strings.Split(am,":")
        if a[0] == serial {
            return a[1]
        }
    }
    return ""
}

func (d *Dv4mini) IsDuplex() bool {
    return false
}

func (d *Dv4mini) StartWriteLoops() error {
    b := make(chan bool)
    // FIXME do I need this buffered?
    d.Dch = make(chan *dstar.DSXFrame, 50) // 50 pkts = 1 sec
    go d.txTimeout(b)
    go d.txLoop(b)
    return nil
}

type Dv4miniCtx struct {
    devices.GenericCtx
}

func (dctx *Dv4miniCtx) Init(erc chan<- error) error {
    devs := strings.Split(dctx.Cb.DV4MINI_MODULES, ",")
    if len(devs[0]) == 0 {
        return nil
    }
    ss := make([]devices.IDevice,0)
    var err error
    for _, dev := range devs {
        devss := strings.Split(dev, ":")
        d := Dv4mini{
            GenericDevice: devices.GenericDevice{ Thr: 5, Cb: dctx.Cb, Erc: erc},
            match_status: MATCH_INIT,
        }
        d.c = &serial.Config{ Name: devss[0], Baud: 115200 }
        d.s, err = serial.OpenPort(d.c)
        if err != nil {
            erc <- err
            continue
        }
        success, _ := d.Ping()
        if ! success {
            continue
        }
        d.btube = make([]byte, 39 + 660)
        d.SetModule(devss[1])

        go d.RSSILoop()
        //d.SetLED(false)
        d.SetDstar()
        var rxf, txf uint32
        var pwr byte
        _, err = fmt.Sscanf(devss[2], "%d", &rxf)
        if err != nil {
            erc <- err
            continue
        }
        _, err = fmt.Sscanf(devss[3], "%d", &txf)
        if err != nil {
            erc <- err
            continue
        }
        _, err = fmt.Sscanf(devss[4], "%d", &pwr)
        if err != nil {
            erc <- err
            continue
        }
        err = d.SetFreq(rxf,txf)
        if err != nil {
            erc <- err
            continue
        }
        d.SetBuf(1)
        err = d.SetPower(pwr)
        if err != nil {
            erc <- err
            continue
        }
        d.FIFOInit()
        ss = append(ss, &d)
    }
    dctx.SetSs(ss)
    return nil
}

