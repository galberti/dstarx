
package main

import (
    "reflect"
    "net"
    "strings"
    "errors"
    "time"
)

func confHandler(pars []string, erc chan<- error) string {
    var reply string
    switch pars[1] {
            case "help", "h", "":
                reply = "conf: [h]elp, [p]rint, [s]et, [w]rite"
            case "print", "p":
                p, err := Cb.PrettyConf()
                if err != nil {
                    reply = err.Error()
                } else {
                    reply = string(p)
                }
            case "set", "s":
                if len(pars) < 4 {
                    reply = "Usage: conf set <var> <value>"
                    break
                }
                f := reflect.ValueOf(&Cb).Elem().FieldByName(pars[2])
                if !f.IsValid() {
                    reply = "var '" + pars[2] + "' not found."
                    break
                }
                f.SetString(pars[3])
                reply = "ok."
            case "write", "w":
                Cb.Save(Cb.ConfPath)
                reply = "Written."
            default:
                reply = "Unknown command."
    }
    return reply
}

func dextraHandler(pars []string, erc chan<- error) string {
    var reply string
    switch pars[1] {
        case "help", "h", "":
            reply = "dextra: [h]elp, [p]rint, [l]ink, [u]nlink"
        case "print", "p":
            reply = ex.StatusString()
        case "link", "l":
            if len(pars) < 3 {
                reply = "Usage: dextra link <call:r:l> (Ex. XRF069:B:A)"
                break
            }
            xref := strings.Split(pars[2],":")
            if len(xref) != 3 {
                reply = "Wrong format. <call:remotemod:localmod>"
                break
            }
            reply = ex.EstablishLink(xref[0],xref[1],xref[2])
        case "unlink", "u":
            if len(pars) < 3 {
                reply = "Usage: dextra unlink <call:r:l> (Ex. XRF069:B:A)"
                break
            }
            xref := strings.Split(pars[2],":")
            if len(xref) != 3 {
                reply = "Wrong format. <call:remotemod:localmod>"
                break
            }
            reply = ex.Unlink(xref[0],xref[1],xref[2])
        default:
            reply = "Unknown command."
    }
    return reply
}

func dcsHandler(pars []string, erc chan<- error) string {
    var reply string
    switch pars[1] {
        case "help", "h", "":
            reply = "dcs: [h]elp, [p]rint, [l]ink, [u]nlink"
        case "print", "p":
            reply = dcs.StatusString()
        case "link", "l":
            if len(pars) < 3 {
                reply = "Usage: dcs link <call:r:l> (Ex. DCS027:B:A)"
                break
            }
            dcsr := strings.Split(pars[2],":")
            if len(dcsr) != 3 {
                reply = "Wrong format. <call:remotemod:localmod>"
                break
            }
            reply = dcs.EstablishLink(dcsr[0],dcsr[1],dcsr[2])
        case "unlink", "u":
            if len(pars) < 3 {
                reply = "Usage: dcs unlink <call:r:l> (Ex. DCS027:B:A)"
                break
            }
            dcsr := strings.Split(pars[2],":")
            if len(dcsr) != 3 {
                reply = "Wrong format. <call:remotemod:localmod>"
                break
            }
            reply = dcs.Unlink(dcsr[0],dcsr[1],dcsr[2])
        default:
            reply = "Unknown command."
    }
    return reply
}

func routeHandler(pars []string, erc chan<- error) string {
    var reply string
    switch pars[1] {
        case "help", "h", "":
        reply = "route: [h]elp, [p]rint, [s]etup, [d]elete, [r]eroute, [m]ain"
        case "print", "p":
            if len(pars) < 3 {
                reply = "Usage: route print <module> (Ex. B)"
                break
            }
            reply = infoRoute(pars[2])
        case "setup", "s":
            if len(pars) < 3 {
                reply = "Usage: route link <mod1:mod2> (Ex. B:A)"
                break
            }
            dcsr := strings.Split(pars[2],":")
            if len(dcsr) != 2 {
                reply = "Wrong format. <mod1:mod2>"
                break
            }
            reply = setupRoute(dcsr[0],dcsr[1])
        case "delete", "d":
            if len(pars) < 3 {
                reply = "Usage: dextra unlink <mod1:mod2> (Ex. B:A)"
                break
            }
            dcsr := strings.Split(pars[2],":")
            if len(dcsr) != 2 {
                reply = "Wrong format. <mod1:mod2>"
                break
            }
            reply = deleteRoute(dcsr[0],dcsr[1])
        case "reroute", "r":
            netw = setupNetwork(erc)
            reply = "Ok."
        case "main", "m":
            if len(pars) < 3 {
                reply = "Usage: route main <mod1:mod2> (Ex. B:A)"
                break
            }
            dcsr := strings.Split(pars[2],":")
            if len(dcsr) != 2 {
                reply = "Wrong format. <mod1:mod2>"
                break
            }
            localSetMain(dcsr[0],dcsr[1],getModuleType(dcsr[0]))
            reply = "Ok."
        default:
            reply = "Unknown command."
    }
    return reply
}

func handleConnection(conn net.Conn, erc chan<- error) {
    buf := make([]byte,80)
    reply := ""
    for reply != "Bye." {
        conn.Write([]byte("dstarx> "))
        l, err := conn.Read(buf)
        if err != nil {
            erc <- errors.New("handleConnection(): " + err.Error())
            conn.Write([]byte("\n"))
            break
        }
        pars := strings.Split(strings.Trim(string(buf[:l]),"\n\r")," ")
        if len(pars) == 1 {
            pars = append(pars, "help")
        }
        switch strings.ToLower(pars[0]) {
            case "help", "h", "":
                reply = "[c]onf [h]elp, [d]extra, dc[s], [r]oute, [q]uit"
            case "dextra", "d":
                reply = dextraHandler(pars,erc)
            case "dcs", "s":
                reply = dcsHandler(pars,erc)
            case "route", "r":
                reply = routeHandler(pars,erc)
            case "conf", "c":
                reply = confHandler(pars,erc)
            case "quit", "q":
                reply = "Bye."
            default:
                reply = "Unknown command."
        }
        conn.Write([]byte(reply+"\n"))
    }
    conn.Close()
}

func Serve(erc chan<- error) {
    var ln net.Listener
    var err error
    for {
        ln, err = net.Listen("tcp",Cb.CONF_SERVER_STRING)
        if err == nil {
            break
        }
        erc <- errors.New("Serve(): " + err.Error())
        time.Sleep(5*time.Second)
    }
    for {
        conn, err := ln.Accept()
        if err != nil {
            erc <- errors.New("Serve(): " + err.Error())
            return
        }
        go handleConnection(conn,erc)
    }
}

