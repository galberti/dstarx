
package dstar

var sb []byte

func generateScrambleBits(sb []byte) {
    r0 := byte(1)
    r1 := byte(1)
    r2 := byte(1)
    r3 := byte(1)
    r4 := byte(1)
    r5 := byte(1)
    r6 := byte(1)
    for i := 0; i < 660; i++ {
        sb[i] = r3 ^ r6
        r6 = r5
        r5 = r4
        r4 = r3
        r3 = r2
        r2 = r1
        r1 = r0
        r0 = sb[i]
    }
}

func Scramble(in []byte) []byte {
    if sb == nil {
        sb = make([]byte, 660)
        generateScrambleBits(sb)
    }
    out := make([]byte, 660)
    for i, _ := range in {
        out[i] = in[i] ^ sb[i]
    }
    return out
}

