
package dstar

/*
 * this is for debugging purposes only ..
 */

/*
import "fmt"

func PrintMatrix() {
    for i := 0; i < 660; i++ {
        fmt.Printf("%03d ",deintToBit(i))
        if i < 336 {
            if (i + 1) % 28 == 0 {
                fmt.Println()
            }
        } else {
            if (i - 336 + 1) % 27 == 0 {
                fmt.Println()
            }
        }
    }
}
*/

func interleaveIndex(k int) int {
    // indices in normal matrix
    i := k % 24
    j := k / 24
    if i < 12 {
        return (i * 28) + j
    }
    return ((i - 12) * 27) + j + (28 * 12)
}

func Deinterleave(in []byte) []byte {
    // NB this only works for len(in) == 660 or less..
    out := make([]byte, len(in))
    for i, _ := range in {
        out[i] = in[interleaveIndex(i)]
    }
    return out
}

func Interleave(in []byte) []byte {
    out := make([]byte, len(in))
    for i, _ := range in {
        out[interleaveIndex(i)] = in[i]
    }
    return out
}

