
package dstar

import "fmt"

func FECEncode(bits []byte) []byte {
    out := make([]byte, 660)
    r1 := byte(0)
    r2 := byte(0)
    for i := 0; i < len(bits); i++ {
        out[2*i] = (bits[i] ^ r1 ^ r2) & 0x01
        out[2*i+1] = (bits[i] ^ r2) & 0x01
        r2 = r1
        r1 = bits[i]
    }
    return out
}

type state struct {
    arrived byte
    fw1 *state
    fw1m int
    fw2 *state
    fw2m int
    bw1 *state
    bw2 *state
}

type instant struct {
    statuses [4]state
}

type trellis struct {
    instants [331]instant
}

func (t *trellis) Init() {
    // 00 has 1 -> 00 and 2 -> 10, 01 is viceversa
    // 00 -+---- 00
    // 01 -+     01
    //     +---- 10
    //           11
    // 
    //           00
    //     +---- 01
    // 10 -+     10
    // 11 -+---- 11

    for i := 0; i < len(t.instants) - 1; i++ {
        t.instants[i].statuses[0].arrived = 0
        t.instants[i].statuses[0].fw1 = &t.instants[i+1].statuses[0]
        t.instants[i].statuses[0].fw2 = &t.instants[i+1].statuses[2]
        t.instants[i].statuses[1].arrived = 0
        t.instants[i].statuses[1].fw2 = &t.instants[i+1].statuses[0]
        t.instants[i].statuses[1].fw1 = &t.instants[i+1].statuses[2]
        t.instants[i].statuses[2].arrived = 1
        t.instants[i].statuses[2].fw1 = &t.instants[i+1].statuses[1]
        t.instants[i].statuses[2].fw2 = &t.instants[i+1].statuses[3]
        t.instants[i].statuses[3].arrived = 1
        t.instants[i].statuses[3].fw2 = &t.instants[i+1].statuses[1]
        t.instants[i].statuses[3].fw1 = &t.instants[i+1].statuses[3]

        t.instants[i+1].statuses[0].bw1 = &t.instants[i].statuses[0]
        t.instants[i+1].statuses[0].bw2 = &t.instants[i].statuses[1]
        t.instants[i+1].statuses[1].bw1 = &t.instants[i].statuses[2]
        t.instants[i+1].statuses[1].bw2 = &t.instants[i].statuses[3]
        t.instants[i+1].statuses[2].bw2 = &t.instants[i].statuses[0]
        t.instants[i+1].statuses[2].bw1 = &t.instants[i].statuses[1]
        t.instants[i+1].statuses[3].bw2 = &t.instants[i].statuses[2]
        t.instants[i+1].statuses[3].bw1 = &t.instants[i].statuses[3]

        for j := 0; j < 4; j++ {
            t.instants[i].statuses[j].fw1m = -1
            t.instants[i].statuses[j].fw2m = -1
        }
    }
    i := len(t.instants) - 1
    t.instants[i].statuses[0].arrived = 0
    t.instants[i].statuses[1].arrived = 0
    t.instants[i].statuses[2].arrived = 1
    t.instants[i].statuses[3].arrived = 1
}

var tr trellis

func expected(b byte) byte {
    in1 := b & 0x01
    in2 := (b & 0x02) >> 1
    in3 := (b & 0x04) >> 2
    o1 := in1 ^ in2 ^ in3
    o2 := in1 ^ in3
    return (o1 << 1) | o2
}

func metric(b1, b2 byte) int {
    r := b1 ^ b2
    switch r {
        case 0:
            return 0
        case 1,2:
            return 1
    }
    // case 3
    return 2
}

func FECDecode(bits []byte) []byte {
    out := make([]byte,len(bits)/2)
    tr.Init()
    exp := make(map[byte]byte)
    for i := 0; i < 8; i++ {
        // compute expectation table like this is wrong. However, as for this
        // encoder enc(100) == enc(001) it is ok
        exp[byte(i)] = expected(byte(i))
        //fmt.Printf("exp[%02x] = %02x\n", i, exp[byte(i)])
    }
    for i := 0; i < len(bits)/2; i++ {
        if i > 0 {
            // remove colliding branches
            for j := 0; j < 4; j++ {
                var b *state
                b1 := tr.instants[i].statuses[j].bw1
                b2 := tr.instants[i].statuses[j].bw2
                if b1.fw1m > b2.fw2m {
                    tr.instants[i].statuses[j].bw1 = nil
                    b = b1
                } else {
                    tr.instants[i].statuses[j].bw2 = nil
                    b = b2
                }
                for (b != nil) && ((b.fw1 == nil) || (b.fw2 == nil)) {
                    // kill my back branch until it joins another branch
                    if b.fw1 != nil {
                        b.fw1 = nil
                    } else {
                        b.fw2 = nil
                    }
                    if b.bw1 != nil {
                        b = b.bw1
                    } else if b.bw2 != nil {
                        b = b.bw2
                    } else { // i == 0
                        b = nil
                    }
                }
            }
        }
        rx := (bits[2*i] << 1) | bits[2*i+1]
        //fmt.Printf("rx[%d] = %x\n", i, rx)
        for j := 0; j < 4; j++ {
            // compute all metrics
            mj := ((byte(j) & 0x01) << 1) | ((byte(j) & 0x02) >> 1)
            if (i > 0) &&
                ((tr.instants[i].statuses[mj].bw1 != nil) &&
                (tr.instants[i].statuses[mj].bw2 != nil)) {
                continue
            }
            if mj % 2 == 0 {
                tr.instants[i].statuses[mj].fw1m = metric(exp[mj], rx)
                tr.instants[i].statuses[mj].fw2m = metric(exp[mj|0x04], rx)
            } else {
                tr.instants[i].statuses[mj].fw2m = metric(exp[mj], rx)
                tr.instants[i].statuses[mj].fw1m = metric(exp[mj|0x04], rx)
            }
            //fmt.Printf("raw %d %d  ",tr.instants[i].statuses[mj].fw1m,
            //    tr.instants[i].statuses[mj].fw2m)
            if i == 0 {
                //fmt.Println()
                continue
            }
            t1 := tr.instants[i].statuses[mj].bw1
            t2 := tr.instants[i].statuses[mj].bw2
            if t1 != nil {
                tr.instants[i].statuses[mj].fw1m += t1.fw1m
                tr.instants[i].statuses[mj].fw2m += t1.fw1m
            } else { // t2 must be != nil
                tr.instants[i].statuses[mj].fw1m += t2.fw2m
                tr.instants[i].statuses[mj].fw2m += t2.fw2m
            }
            //fmt.Printf("accu %d %d  ",tr.instants[i].statuses[mj].fw1m,
            //    tr.instants[i].statuses[mj].fw2m)
            //fmt.Println()
        }
    }
    // pickup the best branch, follow it backwards.
    m := tr.instants[len(bits)/2].statuses[0].fw1m
    fs := &tr.instants[len(bits)/2].statuses[0]
    for k := 0; k < 4; k++ {
        if m > tr.instants[len(bits)/2].statuses[k].fw1m {
            m = tr.instants[len(bits)/2].statuses[k].fw1m
            fs = tr.instants[len(bits)/2].statuses[k].fw1
        }
        if m > tr.instants[len(bits)/2].statuses[k].fw2m {
            m = tr.instants[len(bits)/2].statuses[k].fw2m
            fs = tr.instants[len(bits)/2].statuses[k].fw2
        }
    }
    for i := len(bits)/2-1; i >= 0; i-- {
        out[i] = fs.arrived
        if fs.bw1 != nil {
            fs = fs.bw1
        } else if fs.bw2 != nil {
            fs = fs.bw2
        } else {
            fmt.Println("This shouldn't happen!!!")
        }
    }
    return out
}

