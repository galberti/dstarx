
package dstar

import (
    "fmt"
    "errors"
    "strings"
    "net"
    "time"
    "math/rand"
)

const (
    DCS_STAT_INIT = iota
    DCS_STAT_CONN

    DCS_HF_IMG = "hf.jpg"
    DCS_HOTSPOT_IMG = "hotspot.jpg"
    DCS_RPT_STRING = "REPEATER"
    DCS_HSP_STRING = "HOTSPOT"
    // 3 %s in here to be filled: img, string, sw
    HTML_CRAP = "<table border=\"0\" width=\"95%%\"><tr><td width=\"4%%\">" +
        "<img border=\"0\" src=%s></td><td width=\"96%%\">" +
        "<font size=\"2\"><b>%s</b> %s</font></td></tr></table>"
    DCS_UNLINKREQ_SIZE = 19
    DCS_LINKREQ_SIZE = 519
    DCS_LINKACK_SIZE = 14
    DCS_HBTREQ_SIZE = 22
    DCS_HBTACK_SIZE = 17
    DCS_FRAME_SIZE = 100
    DCS_STATUS_SIZE = 35
    DCS_NEWLINK_SIZE = 7
    DCS_HBTWTF_SIZE = 9 // what the fuck are these packets?

    DCS_PORT = 30051
    DCS_MAX_BW = 5000 * 1.5 // theoretical B/s scaled
)

type DCSUnlinkReq struct {
    Local string
    LocMod string
    RemMod string
    Remote string
}

func (ulr *DCSUnlinkReq) Fill(raw []byte) error {
    if len(raw) != DCS_UNLINKREQ_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DCS_UNLINKREQ_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    ulr.Local = string(raw[:8])
    ulr.LocMod = string(raw[8])
    ulr.RemMod = string(raw[9])
    ulr.Remote = string(raw[11:19])
    return nil
}

func (ulr *DCSUnlinkReq) Get() []byte {
    ret := make([]byte, DCS_UNLINKREQ_SIZE)
    copy(ret[:8],[]byte(fmt.Sprintf("%-8s",ulr.Local)))
    copy(ret[8:9],[]byte(fmt.Sprintf("%s",ulr.LocMod)))
    copy(ret[9:10],[]byte(fmt.Sprintf("%s",ulr.RemMod)))
    ret[10] = 0x00
    copy(ret[11:19],[]byte(fmt.Sprintf("%-8s",ulr.Remote)))
    return ret
}

type DCSLinkReq struct {
    Ulp DCSUnlinkReq // UnLinkPart, same as UnlinkReq
    Html string
}

func (lr *DCSLinkReq) Fill(raw []byte) error {
    if len(raw) != DCS_LINKREQ_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DCS_LINKREQ_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    lr.Ulp = DCSUnlinkReq{}
    lr.Ulp.Fill(raw[:DCS_UNLINKREQ_SIZE])
    lr.Html = strings.TrimRight(string(raw[19:519]),"0x00")
    return nil
}

func (lr *DCSLinkReq) Get() []byte {
    ret := make([]byte, DCS_LINKREQ_SIZE)
    copy(ret[:DCS_UNLINKREQ_SIZE],lr.Ulp.Get())
    copy(ret[19:518],[]byte(lr.Html))
    return ret
}

type DCSLinkReply struct {
    Local string
    LocMod string
    RemMod string
    Ack bool
}

func (lr *DCSLinkReply) Fill(raw []byte) error {
    if len(raw) != DCS_LINKACK_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DCS_LINKACK_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    lr.Local = string(raw[:8])
    lr.LocMod = string(raw[8])
    lr.RemMod = string(raw[9])
    if string(raw[10:13]) == "ACK" {
        lr.Ack = true
    } else { // no enforcement on NAK here ..
        lr.Ack = false
    }
    return nil
}

func (lr *DCSLinkReply) Get() []byte {
    ret := make([]byte, DCS_LINKACK_SIZE)
    copy(ret[:8],[]byte(fmt.Sprintf("%-8s",lr.Local)))
    copy(ret[8:9],[]byte(fmt.Sprintf("%1s",lr.LocMod)))
    copy(ret[9:10],[]byte(fmt.Sprintf("%1s",lr.RemMod)))
    if lr.Ack {
        copy(ret[10:13],[]byte("ACK"))
    } else {
        copy(ret[10:13],[]byte("NAK"))
    }
    ret[13] = 0x00
    return ret
}

/* there is also an heartbeat packet of 9 bytes. No idea what that is.
 * format is 0..7 remote, 7 ' ', 8 0x00.
 */
type DCSHeartBeatReq struct {
    Remote string
    RemMod string
    Local string
    LocMod string
}

func (hb *DCSHeartBeatReq) Fill(raw[]byte) error {
    if len(raw) != DCS_HBTREQ_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DCS_HBTREQ_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    hb.Remote = fmt.Sprintf("%-8s",string(raw[:7]))
    hb.RemMod = string(raw[7])
    // raw[8] is ' '
    hb.Local = fmt.Sprintf("%-8s",string(raw[9:16]))
    hb.LocMod = string(raw[16])
    // raw[17] no idea ('B')
    // raw[18:22] = \n, 0x00, ' ', ' '
    return nil
}

type DCSHeartBeatReply struct {
    Local string
    LocMod string
    Remote string
    RemMod string
}

func (hb *DCSHeartBeatReply) Get() []byte {
    ret := make([]byte, DCS_HBTACK_SIZE)
    copy(ret[:7],fmt.Sprintf("%-7s",hb.Local[:7]))
    copy(ret[7:8],fmt.Sprintf("%s",hb.LocMod[:1]))
    ret[8] = 0x00
    copy(ret[9:17],fmt.Sprintf("%-7s",hb.Remote[:7]))
    return ret
}

type DCSFrame struct {
    Pre [4]byte     // always 0001 ?
    Dsh DSHeader    // NB we have NOT the CRC bytes here
    Session [2]byte // different every tx
    OutSeq byte     // 0x00 - 0x14
    Dsf DSFrame
    RptSeq [3]byte  // lsb..msb
    Flags [3]byte   // 0x01 0x00 0x21. I don't even know if these are flags.
    Text [35]byte
}

func (f *DCSFrame) Fill(raw []byte) error {
    if len(raw) != DCS_FRAME_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DCS_FRAME_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    copy(f.Pre[:],raw[0:4])
    f.Dsh = DSHeader{}
    dsh := make([]byte,DSTAR_HEADER_SIZE)
    copy(dsh,raw[4:DSTAR_HEADER_SIZE+2]) // 4 -> 41 + 4 - 2
    copy(dsh[DSTAR_HEADER_SIZE-2:],[]byte{0x00,0x00}) // fake crc
    f.Dsh.Fill(dsh)
    copy(f.Session[:],raw[43:45])
    f.OutSeq = raw[45]
    f.Dsf = DSFrame{}
    f.Dsf.Fill(raw[46:46+DSTAR_FRAME_SIZE]) // 46 -> 58
    copy(f.RptSeq[:],raw[58:61])
    copy(f.Flags[:],raw[61:64])
    copy(f.Text[:],raw[64:99])
    return nil
}

func (f *DCSFrame) Get() []byte {
    ret := make([]byte, DCS_FRAME_SIZE)
    copy(ret[:4],f.Pre[:])
    copy(ret[4:DSTAR_HEADER_SIZE+2],f.Dsh.Get()) // copy is limited by ret size
    copy(ret[43:45],f.Session[:])
    ret[45] = f.OutSeq
    copy(ret[46:46+DSTAR_FRAME_SIZE],f.Dsf.Get())
    copy(ret[58:61],f.RptSeq[:])
    copy(ret[61:64],f.Flags[:])
    copy(ret[64:99],f.Text[:])
    return ret
}

/* Logic structures */

type DCSPeer struct {
    Peer
}

type DCS struct {
    NetStatus
}

func (ref *DCS) peersManager() {
    for {
        if ref.Cb.DCSDebug == "yes" {
            ref.ErChan <- errors.New("DCS peersManager() start")
        }
        now := time.Now()
        /* kill expired */
        newpeers := make([]IPeer,0)
        ref.peersmtx.Lock()
        for i := range ref.peers {
            if ref.Cb.DCSDebug == "yes" {
                ref.ErChan <- errors.New(fmt.Sprintf(
                    "DCS peersManager(): Peers %+v",ref.peers[i]))
            }
            // check if bw is too high
            bw := float32(ref.peers[i].RxBytes()) /
                float32(now.Sub(ref.peers[i].AccountTime()).Seconds())
            ref.peers[i].ResetRxBytes()
            ref.peers[i].AccountTimeNow()
            if bw > DCS_MAX_BW {
                ref.peers[i].IncExcessBW()
                if ref.peers[i].ExcessBW() == 3 {
                    ref.ErChan <- errors.New(fmt.Sprintf(
                    "DCS peersManager(): %s:%s Bandwidth is %f B/s, removing",
                        ref.peers[i].Remote(),ref.peers[i].RemMod(),bw))
                    continue
                }
            } else {
                ref.peers[i].ResetExcessBW()
            }
            // clear stuff
            if (ref.peers[i].UsedBy() != "") &&
                (ref.peers[i].UsedByTime().Add(2*time.Second).Before(now)) {
                ref.peers[i].SetUsedBy("")
                ref.peers[i].ResetTxCounter()
            }
            // kick off those expired
            if ref.peers[i].LastHeard().Add(1*time.Minute).After(now) {
                // not expired, add
                newpeers = append(newpeers,ref.peers[i])
            } else if len(ref.peers[i].LocMods()) != 0 {
                // expired but still interesting, add and reconnect
                newpeers = append(newpeers,ref.peers[i])
                ref.ErChan <- errors.New(fmt.Sprintf(
                    "DCS peersManager(): %s:%s timed out, reconnect",
                    ref.peers[i].Remote(),ref.peers[i].RemMod()))
                for tomod := range ref.peers[i].LocMods() {
                    ref.peers[i].SetLocMod(tomod,DCS_STAT_INIT)
                    go ref.ReEstablishLink(strings.TrimSpace(
                        ref.peers[i].Remote()),ref.peers[i].RemMod(),tomod)
                }
            } else {
                ref.ErChan <- errors.New(fmt.Sprintf(
                    "DCS peersManager(): %s:%s timed out or removed",
                    ref.peers[i].Remote(),ref.peers[i].RemMod()))
            }
        }
        ref.peers = newpeers
        ref.peersmtx.Unlock()
        if ref.Cb.DCSDebug == "yes" {
            ref.ErChan <- errors.New("DCS peersManager() sleep")
        }
        time.Sleep(5*time.Second)
    }
}

func (ref *DCS) Init() error {
    ipport := fmt.Sprintf("%s:%d",ref.Cb.DCS_SERV_STRING,DCS_PORT)
    a, err := net.ResolveUDPAddr("udp",ipport)
    if err != nil {
        return err
    }
    ref.servConn, err = net.ListenUDP("udp",a)
    if err != nil {
        return err
    }
    go ref.peersManager()
    return nil
}

/* Network handling funcs */

func (ref *DCS) framePkt(data []byte, addr *net.UDPAddr) {
    dcsf := DCSFrame{}
    err := dcsf.Fill(data)
    if err != nil {
        if ref.Cb.DCSDebug == "yes" {
            ref.ErChan <- errors.New("DCS framePkt(): Fill() " + err.Error())
        }
        return
    }
    last := false
    if ((dcsf.OutSeq & 0x40) == 0x40) {
        last = true
    }
    remote := fmt.Sprintf("%-8s",dcsf.Dsh.DestRPT[:7])
    remmod := dcsf.Dsh.DestRPT[7:8]
    ref.peersmtx.Lock()
    p := ref.findPeer(remote,remmod)
    if (p != nil) {
        dsxf := DSXFrame{
            Frm: dcsf.Dsf,
            FromMod: DCS_FROM_LETTER,
            LastFrame: last,
            Seq: dcsf.OutSeq,
        }
        (*p).AddRxBytes(DCS_FRAME_SIZE)
        for tomod, stat := range (*p).LocMods() {
            if stat != DEXTRA_STAT_CONN {
                continue
            }
            if (*p).isMainOf(tomod) {
                dsxf.FromMod = DCS_MAIN_LETTER
            }
            ref.InNat(&dcsf.Dsh,(*p).Remote(),(*p).RemMod(),tomod)
            dsxf.Hdr = dcsf.Dsh
            sdsxf := dsxf
            ref.exd <- &sdsxf
        }
    }
    ref.peersmtx.Unlock()
}


func (ref *DCS) InNat(dsh *DSHeader, remote, remmod, tomod string) {
    dsh.Nat(remote, ref.Cb.ModulesOwner, remmod, tomod)
}

func (ref *DCS) hbtPkt(data []byte, addr *net.UDPAddr) {
    hbr := DCSHeartBeatReq{}
    err := hbr.Fill(data)
    if err != nil {
        if ref.Cb.DCSDebug == "yes" {
            ref.ErChan <- errors.New("DCS hbtPkt(): Fill() " + err.Error())
        }
        return
    }
    if ref.Cb.DCSDebug == "yes" {
        ref.ErChan <- errors.New(fmt.Sprintf(
            "DCS hbtPkt(): DCS HeartBeat: %+v", hbr))
    }
    ref.peersmtx.Lock()
    p := ref.findPeer(hbr.Remote,hbr.RemMod)
    if p != nil {
        s, ok := (*p).LocMod(hbr.LocMod)
        if ok && s == DCS_STAT_CONN {

            (*p).LastHeardNow()
            hbrp := DCSHeartBeatReply{
                Local: hbr.Local,
                LocMod: hbr.LocMod,
                Remote: hbr.Remote,
                RemMod: hbr.RemMod,
            }
            _, err = ref.servConn.WriteToUDP(hbrp.Get(),(*p).Addr())
            if err != nil {
                ref.ErChan <- errors.New(
                    "DCS EstablishLink()[2]: " + err.Error())
            }
        }
    }
    ref.peersmtx.Unlock()
}

func (ref *DCS) ackPkt(data []byte, addr *net.UDPAddr) {
    lr := DCSLinkReply{}
    err := lr.Fill(data)
    if err != nil {
        if ref.Cb.DCSDebug == "yes" {
            ref.ErChan <- errors.New("DCS ackPkt(): Fill() " + err.Error())
        }
        return
    }
    if ref.Cb.DCSDebug == "yes" {
        ref.ErChan <- errors.New(
            fmt.Sprintf("DCS ackPkt(): DCSLinkReply: %+v",lr))
    }
    ref.peersmtx.Lock()
    /* ack doesn't have remote callsign .. */
    var p *IPeer = nil
    for i := range ref.peers {
        if ref.peers[i].HasSameIP(addr) {
            s, ok := ref.peers[i].LocMod(lr.LocMod)
            if ok && (s == DCS_STAT_INIT) {
                p = &ref.peers[i]
                break
            }
        }
    }
    if p != nil {
        if lr.Ack {
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DCS ackPkt(): %s:%s -> %s:%s linked",lr.Local,lr.LocMod,
                (*p).Remote(),(*p).RemMod()))
                (*p).SetLocMod(lr.LocMod,DCS_STAT_CONN)
        } else {
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DCS ackPkt(): %s:%s -> %s:%s link denied",lr.Local,lr.LocMod,
                (*p).Remote(),(*p).RemMod()))
            (*p).DeleteLocMod(lr.LocMod)
        }
    } else {
        ref.ErChan <- errors.New(fmt.Sprintf(
            "DCS ackPkt(): reply from unknown %+v",lr))
    }
    ref.peersmtx.Unlock()
}

func (ref *DCS) StartServer(exd chan<- *DSXFrame) {
    buf := make([]byte,600) // biggest packet size is 600 so far ..
    ref.exd = exd
    for {
        l, addr, err := ref.servConn.ReadFromUDP(buf)
        if err != nil {
            ref.ErChan <- errors.New("" + err.Error())
            return
        }
        switch l {
            //case DCS_UNLINKREQ_SIZE:
            case DCS_LINKACK_SIZE:
                ref.ackPkt(buf[:l],addr)
            case DCS_HBTREQ_SIZE:
                ref.hbtPkt(buf[:l],addr)
            case DCS_FRAME_SIZE:
                ref.framePkt(buf[:l],addr)
            case DCS_STATUS_SIZE:
            //case DCS_NEWLINK_SIZE:
            case DCS_HBTWTF_SIZE:
            default:
                ref.ErChan <- errors.New(fmt.Sprintf(
                    "DCS GetData(): Unknown packet, %d bytes from %v: %v",
                    l,addr,buf[:l]))
        }
    }
}

func (ref *DCS) getAddr(xref string) (*net.UDPAddr, error) {
    ipport := fmt.Sprintf("%s.xreflector.net:%d",strings.TrimSpace(xref),
        DCS_PORT)
    a, err := net.ResolveUDPAddr("udp",ipport)
    if err != nil {
        return nil,err
    }
    return a,nil
}

func (ref *DCS) txLocal(f *DSXFrame, p *IPeer) {
    for m, s := range (*p).LocMods() {
        if (m == f.FromMod) || (s != DEXTRA_STAT_CONN) {
            continue
        }
        f.Hdr.Nat(f.Hdr.DepaRPT,f.Hdr.DestRPT,f.FromMod,m)
        // I can avoid SetCRC() here, the pkt isn't going out yet
        ref.exd <- f
    }
}

func (ref *DCS) txNet(f *DSXFrame, p *IPeer) {
    dcsf := DCSFrame{}
    copy(dcsf.Pre[:],[]byte("0001"))
    f.Hdr.Nat(ref.Cb.DCSOwner,(*p).Remote(),f.FromMod,(*p).RemMod())
    f.Hdr.SetCRC()
    dcsf.Dsh = f.Hdr
    sess := (*p).TxSess()
    if sess == 0 {
        (*p).SetTxSess(uint16(rand.Uint32() & 0x0000ffff))
        sess = (*p).TxSess()
    }
    dcsf.Session[0] = byte(sess & 0xff)
    dcsf.Session[1] = byte((sess >> 8) & 0xff)
    txc := (*p).TxCounter()
    (*p).IncTxCounter()
    dcsf.OutSeq = byte(txc % 0x15)
    if f.LastFrame {
        dcsf.OutSeq = dcsf.OutSeq | 0x40
        (*p).ResetTxCounter()
    }
    dcsf.Dsf = f.Frm
    dcsf.RptSeq[0] = byte(txc & 0x000000ff)
    dcsf.RptSeq[1] = byte((txc & 0x0000ff00) >> 8)
    dcsf.RptSeq[2] = byte((txc & 0x00ff0000) >> 16)
    dcsf.Flags = [3]byte{0x01,0x00,0x21}
    // FIXME Text
    _, err := ref.servConn.WriteToUDP(dcsf.Get(), (*p).Addr())
    if err != nil {
        ref.ErChan <- errors.New("DCS Tx(): " + err.Error())
    }
}

/* External interface */

func (ref *DCS) Tx(f *DSXFrame) {
    // TODO check realfrom to be local?
    if strings.TrimSpace(f.Hdr.YoCall) != "CQCQCQ" {
        ref.Command(f.Hdr.DepaRPT[7:8],f.Hdr.YoCall,f.FromMod)
        return
    }
    ref.peersmtx.Lock()
    p := ref.modGetMain(f.FromMod)
    if (p != nil) && (((*p).UsedBy() == "") || ((*p).UsedBy() == f.FromMod)) {
        (*p).SetUsedBy(f.FromMod)
        (*p).UsedByTimeNow()
        lf := *f
        ref.txLocal(&lf, p)
        nf := *f
        ref.txNet(&nf, p)
    }
    ref.peersmtx.Unlock()
}

func (ref *DCS) ReEstablishLink(dcsref, remmod, locmod string) string {
    return ref.establishLink(dcsref, remmod, locmod, false)
}

func (ref *DCS) EstablishLink(dcsref, remmod, locmod string) string {
    return ref.establishLink(dcsref, remmod, locmod, true)
}

func (ref *DCS) establishLink(dcsref, remmod, locmod string,
    setmain bool) string {
    dcsref = strings.ToUpper(dcsref)
    remmod = strings.ToUpper(remmod)
    locmod = strings.ToUpper(locmod)
    ref.ErChan <- errors.New(fmt.Sprintf(
        "DCS EstablishLink(): %s:%s -> %s:%s",ref.Cb.DCSOwner,locmod,dcsref,
        remmod))
    addr, err := ref.getAddr(dcsref)
    if err != nil {
        ref.ErChan <- errors.New("DCS EstablishLink(): " + err.Error())
        return "Not found"
    }
    dcsref8 := fmt.Sprintf("%-8s",dcsref)
    lr := DCSLinkReq{
        Ulp: DCSUnlinkReq{
            Local: fmt.Sprintf("%-8s",ref.Cb.DCSOwner),
            LocMod: locmod,
            RemMod: remmod,
            Remote: dcsref8,
        },
        Html: fmt.Sprintf(HTML_CRAP,DCS_HOTSPOT_IMG,DCS_HSP_STRING,
            "DstarX Beta"),
    }
    ref.peersmtx.Lock()
    modmain := ref.modGetMain(locmod)
    p := ref.findPeer(dcsref8,remmod)
    var ret string
    if (p == nil) {
        np := DCSPeer{
            Peer{
                addr: addr,
                remote: dcsref8,
                remMod: remmod,
            },
        }
        np.LastHeardNow()
        np.SetLocMod(locmod,DCS_STAT_INIT)
        if setmain {
            np.AppendMainMod(locmod)
            if modmain != nil {
                (*modmain).RemoveMainMod(locmod)
            }
        }
        ref.peers = append(ref.peers,&np)
        _, err = ref.servConn.WriteToUDP(lr.Get(),addr)
        if err != nil {
            ref.ErChan <- errors.New("DCS EstablishLink()[1]: " +
                err.Error())
        }
        ret = "Linking (new peer)"
    } else {
        // peer already connected
        stat, ok := (*p).LocMod(locmod)
        if ok {
            if (stat != DCS_STAT_CONN) {
                // I was already trying to connect ..
                ret = "Relinking"
                _, err = ref.servConn.WriteToUDP(lr.Get(),addr)
                if err != nil {
                    ref.ErChan <- errors.New(
                        "DCS EstablishLink()[2]: " + err.Error())
                }
            } else {
                ret = "Already linked"
            }
        } else {
            // local mod wasn't connected
            (*p).SetLocMod(locmod,DCS_STAT_CONN)
            ret = "Linked"
        }
        if setmain {
            if (modmain != nil) && (modmain != p) {
                (*modmain).RemoveMainMod(locmod)
            }
            if (modmain == nil) || (modmain != p) {
                (*p).AppendMainMod(locmod)
            }
        }
    }
    ref.peersmtx.Unlock()
    return ret
}

func (ref *DCS) Unlink(dcsref, remmod, locmod string) string {
    // I won't wait for an ack, hence I will get the "reply from unknown"
    ret := "Not found"
    dcsref = strings.ToUpper(dcsref)
    remmod = strings.ToUpper(remmod)
    locmod = strings.ToUpper(locmod)
    ref.peersmtx.Lock()
    p := ref.findPeer(fmt.Sprintf("%-8s",dcsref), remmod)
    if p != nil {
        _ , ok := (*p).LocMod(locmod)
        ret = "Module not linked"
        if ok {
            (*p).DeleteLocMod(locmod)
            if (*p).isMainOf(locmod) {
                (*p).RemoveMainMod(locmod)
            }
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DCS Unlink(): %s -> %s:%s", locmod,(*p).Remote(),
                (*p).RemMod()))
            ret = fmt.Sprintf("Unlinked %s>%s%s",locmod,(*p).Remote(),
                (*p).RemMod())
        }
        if len((*p).LocMods()) == 0 {
            var own string
            own = fmt.Sprintf("%-8s",ref.Cb.DCSOwner)
            ur := DCSUnlinkReq{
                Remote: (*p).Remote(),
                Local: own,
                RemMod: remmod,
                LocMod: locmod,

            }
            _, err := ref.servConn.WriteToUDP(ur.Get(),(*p).Addr())
            if err != nil {
                ref.ErChan <- errors.New("DCS Unlink(): " + err.Error())
            }
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DCS Unlink(): %s -> %s:%s [D]",
                locmod,(*p).Remote(),(*p).RemMod()))
            ret = fmt.Sprintf("UNLINKED %s>%s%s",locmod,(*p).Remote(),
                (*p).RemMod())
        }
    } else {
        ref.ErChan <- errors.New(fmt.Sprintf(
            "DCS Unlink(): %s:%s not linked",dcsref,remmod))
        ret = "DCSPeer not linked"
    }
    ref.peersmtx.Unlock()
    return ret
}

func (ref *DCS) infoDCS(mod string) string {
    ret := "No Main"
    mod = strings.ToUpper(mod)
    ref.peersmtx.Lock()
    p := ref.modGetMain(mod)
    if p != nil {
        ret = fmt.Sprintf("%s>%s%s",mod,strings.TrimSpace((*p).Remote()),
            (*p).RemMod())
    }
    ref.peersmtx.Unlock()
    return ret
}

func (ref *DCS) statusDCS(mod string) string {
    ret := ""
    mod = strings.ToUpper(mod)
    ref.peersmtx.Lock()
    for _, v := range ref.peers {
        _, ok := v.LocMod(mod)
        if ok {
            ret = ret + fmt.Sprintf("%s%s",v.Remote()[3:6],v.RemMod())
        }
    }
    ref.peersmtx.Unlock()
    if ret == "" {
        ret = "No DCS peers"
    }
    return ret
}

func (ref *DCS) Command(frommod, yocall, realfrom string) bool {
    now := time.Now()
    if (ref.lastCommandTime != nil) &&
        (ref.lastCommandTime.Add(2*time.Second).After(now)) {
        return true
    }
    ref.lastCommandTime = &now
    call := yocall[:6]
    mod := yocall[6:7]
    com := yocall[7:8]
    switch com {
        case "L":
            // link
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DCS_MAIN_LETTER,
                ref.EstablishLink(call,mod,frommod))
            return true
        case "U":
            // unlink
            if mod == " " {
                mod = realfrom
            }
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DCS_MAIN_LETTER,
                ref.Unlink(call,mod,frommod))
            return true
        case "I":
            // info
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DCS_MAIN_LETTER,
                ref.infoDCS(frommod))
            return true
        case "S":
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DCS_MAIN_LETTER,
                ref.statusDCS(frommod))
            return true
        case "E":
            // echo
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DCS_MAIN_LETTER,
                "Not implemented")
            return true
    }
    return false
}


/*
17:18:43.410063 IP 87.106.84.53.30051 > 192.168.0.4.53362: UDP, length 35
    0x0000:  4500 003f 0000 4000 3811 d662 576a 5435  E..?..@.8..bWjT5
    0x0010:  c0a8 0004 7563 d072 002b 241e 4955 3043  ....uc.r.+$.IU0C
    0x0020:  5853 2020 5461 6c6b 206f 6e20 4443 5330  XS..Talk.on.DCS0
    0x0030:  3038 2044 0000 0000 0000 0000 0000 00    08.D...........

17:17:11.049893 IP 87.106.84.53.30051 > 192.168.0.4.53362: UDP, length 7
    0x0000:  4500 0023 0000 4000 3811 d67e 576a 5435  E..#..@.8..~WjT5
    0x0010:  c0a8 0004 7563 d072 000f 13ce 4e45 574c  ....uc.r....NEWL
    0x0020:  494e 4b00 0000 0000 0000 0000 0000 65f2  INK...........e.
    0x0030:  72e0             
*/

