
package dstar

import (
    "net"
    "fmt"
    "errors"
    "time"
    "sync"
    "strings"
    "math/rand"
    "gitlab.com/galberti/dstarx/confbox"
)

const (
    DEXTRA_STAT_INIT = iota // Connecting
    DEXTRA_STAT_CONN        // Connected

    DEXTRA_LINKREQ_SIZE = 11
    DEXTRA_LINKACK_SIZE = 14
    DEXTRA_UNLINKR_SIZE = 11
    DEXTRA_HEARTBT_SIZE = 9
    DEXTRA_HEADER_SIZE = 56
    DEXTRA_FRAMEP_SIZE = 27
    DEXTRA_FRAMEHH_SIZE = 15
    DEXTRA_PORT = 30001
    DEXTRA_MAX_BW = 1350 * 1.5 // theoretical B/s scaled
)

type DExtraLinkReq struct {
    Owner string
    FromMod string
    ToMod string
}

func (lr *DExtraLinkReq) Fill(raw []byte) error {
    if len(raw) != DEXTRA_LINKREQ_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DEXTRA_LINKREQ_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    lr.Owner = string(raw[:8])
    lr.FromMod = string(raw[8])
    lr.ToMod = string(raw[9])
    return nil
}

func (lr *DExtraLinkReq) Get() []byte {
    ret := make([]byte, DEXTRA_LINKREQ_SIZE)
    copy(ret[:8],[]byte(fmt.Sprintf("%-8s",lr.Owner[:8])))
    copy(ret[8:9],[]byte(fmt.Sprintf("%1s",lr.FromMod[:1])))
    copy(ret[9:10],[]byte(fmt.Sprintf("%1s",lr.ToMod[:1])))
    ret[10] = 0x00
    return ret
}

type DExtraLinkReply struct {
    Owner string
    FromMod string
    ToMod string
    Ack bool
}

func (lr *DExtraLinkReply) Fill(raw []byte) error {
    if len(raw) != DEXTRA_LINKACK_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DEXTRA_LINKACK_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    lr.Owner = string(raw[:8])
    lr.FromMod = string(raw[8])
    lr.ToMod = string(raw[9])
    if string(raw[10:13]) == "ACK" {
        lr.Ack = true
    } else { // no enforcement on NAK here ..
        lr.Ack = false
    }
    return nil
}

func (lr *DExtraLinkReply) Get() []byte {
    ret := make([]byte, DEXTRA_LINKACK_SIZE)
    copy(ret[:8],[]byte(fmt.Sprintf("%-8s",lr.Owner[:8])))
    copy(ret[8:9],[]byte(fmt.Sprintf("%1s",lr.FromMod[:1])))
    copy(ret[9:10],[]byte(fmt.Sprintf("%1s",lr.ToMod[:1])))
    if lr.Ack {
        copy(ret[10:13],[]byte("ACK"))
    } else {
        copy(ret[10:13],[]byte("NAK"))
    }
    ret[13] = 0x00
    return ret
}

type DExtraHeartBeat struct {
    Owner string
}

func (hb *DExtraHeartBeat) Fill(raw []byte) error {
    if len(raw) != DEXTRA_HEARTBT_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DEXTRA_HEARTBT_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    hb.Owner = string(raw[:8])
    return nil
}

func (hb *DExtraHeartBeat) Get() []byte {
    ret := make([]byte, DEXTRA_HEARTBT_SIZE)
    copy(ret[:8],[]byte(fmt.Sprintf("%-8s",hb.Owner[:8])))
    ret[8] = 0x00
    return ret
}

type DExtraFrameHeaderHeader struct {
    Pre [4]byte     // Always DSVT
    Type byte       // 0x10 header, 0x20 voice/data
    Flags [4]byte   // 0x00 0x00 0x00 0x20 (no idea what they are)
    Bands [3]byte   // 0x00 0x02 0x01 - [0] [1] tomod [2]: frommod 'A'..'D'
    Session [2]byte // id%256, id/256 - unused?
    Count byte      // 0x80 for header, 0x00-0x14 for voice/data, if|0x40 = END
}

func (defhh *DExtraFrameHeaderHeader) Fill(raw []byte) error {
    if len(raw) != DEXTRA_FRAMEHH_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DEXTRA_FRAMEHH_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    copy(defhh.Pre[:],raw[0:4])
    defhh.Type = raw[4]
    copy(defhh.Flags[:],raw[5:9])
    copy(defhh.Bands[:],raw[9:12])
    copy(defhh.Session[:],raw[12:14])
    defhh.Count = raw[14]
    return nil
}

func (defhh *DExtraFrameHeaderHeader) Get() []byte {
    ret := make([]byte, DEXTRA_FRAMEHH_SIZE)
    copy(ret[0:4],defhh.Pre[:])
    ret[4] = defhh.Type
    copy(ret[5:9],defhh.Flags[:])
    copy(ret[9:12],defhh.Bands[:])
    copy(ret[12:14],defhh.Session[:])
    ret[14] = defhh.Count
    return ret
}

type DExtraFrameHeader struct {
    dxhdr DExtraFrameHeaderHeader
    dshdr DSHeader
}

func (dxfh *DExtraFrameHeader) Fill(raw []byte) error {
    err := dxfh.dxhdr.Fill(raw[:DEXTRA_FRAMEHH_SIZE])
    if err != nil {
        return err
    }
    err = dxfh.dshdr.Fill(raw[DEXTRA_FRAMEHH_SIZE:])
    if err != nil {
        return err
    }
    return nil
}

func (dxfh *DExtraFrameHeader) Get() []byte {
    ret := make([]byte, DEXTRA_FRAMEHH_SIZE + DSTAR_HEADER_SIZE)
    copy(ret[:DEXTRA_FRAMEHH_SIZE],dxfh.dxhdr.Get())
    copy(ret[DEXTRA_FRAMEHH_SIZE:],dxfh.dshdr.Get())
    return ret
}

type DExtraFrameVoice struct {
    dxhdr DExtraFrameHeaderHeader
    dsfrm DSFrame
}

func (dxfv *DExtraFrameVoice) Fill(raw []byte) error {
    err := dxfv.dxhdr.Fill(raw[:DEXTRA_FRAMEHH_SIZE])
    if err != nil {
        return err
    }
    copy(dxfv.dsfrm.data[:],raw[DEXTRA_FRAMEHH_SIZE:])
    return nil
}

func (dxfv *DExtraFrameVoice) Get() []byte {
    ret := make([]byte, DEXTRA_FRAMEHH_SIZE + DSTAR_FRAME_SIZE)
    copy(ret[:DEXTRA_FRAMEHH_SIZE],dxfv.dxhdr.Get())
    copy(ret[DEXTRA_FRAMEHH_SIZE:],dxfv.dsfrm.data[:])
    return ret
}

/* Logic structs */

type Ban struct {
    Call string
    expires time.Time
}

func (b *Ban) SetBan(c string) {
    b.Call = c
    b.expires = time.Now().Add(5*time.Minute)
}

func (b *Ban) IsExpired() bool {
    return b.expires.Before(time.Now())
}

type Bans struct {
    calls []Ban
}

func (b *Bans) Add(c string) {
    ba := Ban{}
    ba.SetBan(c)
    b.calls = append(b.calls, ba)
}

func (b *Bans) Contains(c string) bool {
    for _, v := range b.calls {
        if (v.Call == c) && !v.IsExpired() {
            return true
        }
    }
    return false
}

func (b *Bans) Purge() {
    n := make([]Ban, 0)
    for _, v := range b.calls {
        if ! v.IsExpired() {
            n = append(n, v)
        }
    }
    b.calls = n
}

// FIXME use Peer here
type DEPeer struct {
    Addr *net.UDPAddr
    Owner string
    FromMod string
    ToMod map[string]byte
    LastHeard time.Time       // last time I heard this peer
    Reconnect bool            // do I need to attempt reconnecting?
    LastHeader *DSHeader      // last header heard from this peer..
    LastHeaderTime time.Time  // ..and its time
    MainMods []string         // these mods have this peer as main
    UsedBy string             // this peer is used for Tx by this Module..
    UsedByTime time.Time      // ..and its time
    TxSess uint16             // shit way to get the right peer from DExFrames
    RxSess uint16             // shit way to get the right peer from DExFrames
    Reflect string            // "" no reflector, otherwise reflector module
    ConnectTo string
    RxBytes uint32
    ExcessBW byte
    Banned *Bans
    AccountTime time.Time
}

func (p *DEPeer) isMainOf(mod string) bool {
    for _, v := range p.MainMods {
        if mod == v {
            return true
        }
    }
    return false
}

func (p *DEPeer) removeMain(mod string) {
    nm := make([]string,0)
    for i := range p.MainMods {
        if p.MainMods[i] != mod {
            nm = append(nm, p.MainMods[i])
        }
    }
    p.MainMods = nm
}

// FIXME use NetStatus
type DExtra struct {
    Cb *confbox.ConfBox
    FbChan chan<- string
    ErChan chan<- error
    peers []DEPeer
    peersmtx sync.Mutex
    servConn *net.UDPConn
    lastCommandTime *time.Time
    exd chan<- *DSXFrame
}

func (ref *DExtra) peersManager() {
    for {
        if ref.Cb.DExtraDebug == "yes" {
            ref.ErChan <- errors.New("DExtra peersManager() start")
        }
        now := time.Now()
        /* kill expired */
        newpeers := make([]DEPeer,0)
        ref.peersmtx.Lock()
        for i := range ref.peers {
            if ref.Cb.DExtraDebug == "yes" {
                ref.ErChan <- errors.New(fmt.Sprintf(
                    "DExtra peersManager(): Peers %+v",ref.peers[i]))
            }
            // check bw
            bw := float32(ref.peers[i].RxBytes) /
                float32(now.Sub(ref.peers[i].AccountTime).Seconds())
            ref.peers[i].RxBytes = 0
            ref.peers[i].AccountTime = now
            if bw > DEXTRA_MAX_BW {
                ref.peers[i].ExcessBW = ref.peers[i].ExcessBW + 1
                if ref.peers[i].ExcessBW == 2 {
                    ref.ErChan <- errors.New(fmt.Sprintf(
                    "DExtra peersManager(): %s:%s %s BWidth is %f B/s. Ban.",
                        ref.peers[i].Owner,ref.peers[i].FromMod,
                        ref.peers[i].LastHeader.MyCall1,bw))
                    ref.peers[i].Banned.Add(ref.peers[i].LastHeader.MyCall1)
                    ref.peers[i].LastHeader = nil
                    ref.peers[i].UsedBy = ""
                }
            } else {
                ref.peers[i].ExcessBW = 0
            }
            // clear stuff
            if (ref.peers[i].LastHeader != nil) &&
                (ref.peers[i].LastHeaderTime.Add(2*time.Second).Before(now)) {
                ref.peers[i].LastHeader = nil
            }
            if (ref.peers[i].UsedBy != "") &&
                (ref.peers[i].UsedByTime.Add(2*time.Second).Before(now)) {
                ref.peers[i].UsedBy = ""
            }
            // kick off those expired
            if (ref.peers[i].LastHeard.Add(1*time.Minute).After(now)) {
                // not expired, add
                newpeers = append(newpeers,ref.peers[i])
            } else if len(ref.peers[i].ToMod) != 0 && ref.peers[i].Reconnect {
                // expired but still interesting, add and reconnect
                newpeers = append(newpeers,ref.peers[i])
                ref.ErChan <- errors.New(fmt.Sprintf(
                    "DExtra peersManager(): %s:%s timed out, reconnect",
                    ref.peers[i].Owner,ref.peers[i].FromMod))
                for tomod := range ref.peers[i].ToMod {
                    ref.peers[i].ToMod[tomod] = DEXTRA_STAT_INIT
                    if ref.peers[i].ConnectTo != "" {
                        go ref.ReEstablishLink(strings.TrimSpace(
                            ref.peers[i].ConnectTo),ref.peers[i].FromMod,tomod)
                    } else {
                        go ref.ReEstablishLink(strings.TrimSpace(
                            ref.peers[i].Owner),ref.peers[i].FromMod,tomod)
                    }
                }
            } else {
                ref.ErChan <- errors.New(fmt.Sprintf(
                    "DExtra peersManager(): %s:%s timed out or removed",
                    ref.peers[i].Owner,ref.peers[i].FromMod))
            }
            // purge bans
            ref.peers[i].Banned.Purge()
        }
        ref.peers = newpeers
        /* I am alive folks! */
        hb := DExtraHeartBeat{}
        if (ref.Cb.DExtraOwner != ref.Cb.ModulesOwner) {
            hb.Owner = fmt.Sprintf("%-8s",ref.Cb.DExtraOwner)
        } else {
            hb.Owner = fmt.Sprintf("%-8s",ref.Cb.ModulesOwner)
        }
        hbb := hb.Get()
        for _, v := range ref.peers {
            if (len(v.ToMod) != 0) && (v.Addr != nil) {
                _, err := ref.servConn.WriteToUDP(hbb,v.Addr)
                if err != nil {
                    ref.ErChan <- errors.New("DExtra peersManager(): " +
                        err.Error())
                }
            }
        }
        ref.peersmtx.Unlock()
        if ref.Cb.DExtraDebug == "yes" {
            ref.ErChan <- errors.New("DExtra peersManager() sleep")
        }
        time.Sleep(5*time.Second)
    }
}

func (ref *DExtra) Init() error {
    ipport := fmt.Sprintf("%s:%d",ref.Cb.DEXTRA_SERV_STRING,DEXTRA_PORT)
    a, err := net.ResolveUDPAddr("udp",ipport)
    if err != nil {
        return err
    }
    ref.servConn, err = net.ListenUDP("udp",a)
    if err != nil {
        return err
    }
    go ref.peersManager()
    return nil
}

func (ref *DExtra) findPeer(owner, frommod string) *DEPeer {
    // CAUTION: to be called with mutex already set
    var ret *DEPeer = nil
    if strings.TrimSpace(owner) == "" {
        for i := range ref.peers {
            if (ref.peers[i].isMainOf(frommod)) {
                ret = &(ref.peers[i])
                break
            }
        }
    } else {
        for i := range ref.peers {
            if (owner == ref.peers[i].Owner) &&
                (frommod == ref.peers[i].FromMod) {
                ret = &(ref.peers[i])
                break
            }
        }
    }
    return ret
}

func (ref *DExtra) findPeerByAddr(addr *net.UDPAddr) *DEPeer {
    // CAUTION: to be called with mutex already set
    var p *DEPeer = nil
    for i := range ref.peers {
        if (ref.peers[i].Addr != nil) && ref.peers[i].Addr.IP.Equal(addr.IP) {
            p = &ref.peers[i]
            break
        }
    }
    return p
}

func (ref *DExtra) findPeerByAddrSess(addr *net.UDPAddr, sess uint16) *DEPeer {
    // CAUTION: to be called with mutex already set
    var p *DEPeer = nil
    for i := range ref.peers {
        if (ref.peers[i].Addr != nil) && ref.peers[i].Addr.IP.Equal(addr.IP) &&
            (ref.peers[i].RxSess == sess) {
            p = &ref.peers[i]
            break
        }
    }
    return p
}

func (ref *DExtra) weHaveMod(mod string) bool {
    modules := strings.Split(ref.Cb.DExtraModules,",")
    for _, m := range modules {
        if m == mod {
            return true
        }
    }
    return false
}

func (ref *DExtra) modGetMain(mod string) *DEPeer {
    var modmain *DEPeer = nil
    for i, _ := range ref.peers {
        if ref.peers[i].isMainOf(mod) {
            modmain = &ref.peers[i]
            break
        }
    }
    return modmain
}

func (ref *DExtra) StartServer(exd chan<- *DSXFrame) {
    buf := make([]byte,60) // biggest packet size is 56 so far ..
    ref.exd = exd
    for {
        l, addr, err := ref.servConn.ReadFromUDP(buf)
        if err != nil {
            ref.ErChan <- errors.New("" + err.Error())
            return
        }
        //if ref.Cb.DExtraDebug == "yes" {
        //    erc <- errors.New(
        //        fmt.Sprintf("DExtra GetData(): %v bytes from %v: %v",l,addr,
        //        buf[:l]))
        //}
        switch l {
            case DEXTRA_LINKREQ_SIZE:
                ref.linkPkt(buf[:l],addr)
            case DEXTRA_LINKACK_SIZE:
                ref.ackPkt(buf[:l],addr)
            case DEXTRA_HEARTBT_SIZE:
                ref.hbtPkt(buf[:l],addr)
            case DEXTRA_HEADER_SIZE:
                ref.headerPkt(buf[:l],addr)
            case DEXTRA_FRAMEP_SIZE:
                ref.framePkt(buf[:l],addr)
            default:
                ref.ErChan <- errors.New(fmt.Sprintf(
                    "DExtra GetData(): Unknown packet, %d bytes from %v: %v",
                    l,addr,buf[:l]))
        }
    }
}

func (ref *DExtra) linkPkt(data []byte, addr *net.UDPAddr) {
    // I get this either as link request or as ack when connecting a reflector
    // and acting as a reflector
    lr := DExtraLinkReq{}
    err := lr.Fill(data)
    if err != nil {
        if ref.Cb.DExtraDebug == "yes" {
            ref.ErChan <- errors.New("DExtra linkPkt(): Fill() " + err.Error())
        }
        return
    }
    ref.peersmtx.Lock()
    ack := true
    if strings.TrimSpace(lr.ToMod) == "" {
        // unlink
        // this should arrive only if just one local mod is connected..
        p := ref.findPeer(lr.Owner, lr.FromMod)
        if p == nil {
            ref.peersmtx.Unlock()
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DExtra linkPkt(): UnlinkReq from unknown %+v",lr))
            return
        }
        ref.ErChan <- errors.New(fmt.Sprintf("DEXtra linkPkt(): %+v unlinked",
            lr))
        ur := DExtraLinkReq{
            Owner: p.Owner,
            FromMod: p.FromMod,
            ToMod: " ",
        }
        _, err := ref.servConn.WriteToUDP(ur.Get(),p.Addr)
        if err != nil {
            ref.ErChan <- errors.New("DExtra linkPkt() [1]: " + err.Error())
        }
        for tomod, _ := range p.ToMod {
            delete(p.ToMod, tomod)
        }
        p.MainMods = []string{}
        p.Reconnect = false
        p.Reflect = ""
    } else {
        // link - also ack if remote is a reflector (...)
        raddr := *addr
        // this to ALWAYS reply to DEXTRA_PORT (very bad idea..)
        //raddr.Port = DEXTRA_PORT
        usexref := (ref.Cb.ModulesOwner != ref.Cb.DExtraOwner)

        if (!usexref &&
            (!ref.weHaveMod(lr.ToMod) || (ref.modGetMain(lr.ToMod) != nil))) {
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DExtra linkPkt(): Unknown/busy module req from %+v",lr))
            ack = false
        } else if usexref && (lr.ToMod[0] < 65) || (lr.ToMod[0] > 90) {
            // allow reflector modules A..Z
            ack = false
        } else {
            p := ref.findPeer(lr.Owner, lr.FromMod)
            modmain := ref.modGetMain(lr.FromMod)
            if (p == nil) {
                ref.ErChan <- errors.New(fmt.Sprintf(
                    "DEXtra linkPkt(): DExtraLinkReq %+v",lr))
                p = &DEPeer{
                    Addr: &raddr,
                    Owner: lr.Owner,
                    FromMod: lr.FromMod,
                    ToMod: make(map[string]byte),
                    LastHeard: time.Now(),
                    Banned: &Bans{},
                }
                p.TxSess = uint16(rand.Uint32() & 0x0000ffff)
                p.ToMod[lr.ToMod] = DEXTRA_STAT_CONN
                if usexref {
                    // connect it to the reflector
                    p.Reflect = lr.ToMod
                } else if modmain == nil {
                    // not reflector, setup main if needed
                    p.MainMods = []string{lr.ToMod}
                }
                ref.peers = append(ref.peers,*p)
            } else if !usexref {
                // not connected to a local reflector and normal mode
                // connect it to the req module
                // lr.ToMod is available and free, see above
                p.ToMod[lr.ToMod] = DEXTRA_STAT_CONN
                if modmain == nil {
                    p.MainMods = append(p.MainMods,lr.ToMod)
                }
            } else if (p.Reflect != "" &&
                p.ToMod[lr.ToMod] == DEXTRA_STAT_INIT) {
                // if in reflector mode it has to be an ACK
                p.ToMod[lr.ToMod] = DEXTRA_STAT_CONN
                // the code below was meant to differentiate but it's the same.
                /*
                // if I initiated it, set it connected
                s, ok := p.ToMod[lr.ToMod]
                if ok && (s == DEXTRA_STAT_INIT) {
                    // ack to an initiated connection
                    p.ToMod[lr.ToMod] = DEXTRA_STAT_CONN
                } else {
                    // same peer connecting to another ref module.
                    p.ToMod[lr.ToMod] = DEXTRA_STAT_CONN
                }
                */
            } else {
                // We are already connected
                ack = false
            }
        }
        rp := DExtraLinkReply{
            Owner: lr.Owner,
            FromMod: lr.FromMod,
            ToMod: lr.ToMod,
            Ack: ack,
        }
        _, err := ref.servConn.WriteToUDP(rp.Get(),&raddr)
        if err != nil {
            ref.ErChan <- errors.New("DExtra linkPkt() [2]: " + err.Error())
        }
    }
    ref.peersmtx.Unlock()
}

func (ref *DExtra) ackPkt(data []byte, addr *net.UDPAddr) {
    lr := DExtraLinkReply{}
    err := lr.Fill(data)
    if err != nil {
        if ref.Cb.DExtraDebug == "yes" {
            ref.ErChan <- errors.New("DExtra ackPkt(): Fill() " + err.Error())
        }
        return
    }
    if ref.Cb.DExtraDebug == "yes" {
        ref.ErChan <- errors.New(
            fmt.Sprintf("DExtra ackPkt(): DExtraLinkReply: %+v",lr))
    }
    ref.peersmtx.Lock()
    /* ack doesn't have remote callsign .. */
    var p *DEPeer = nil
    for i := range ref.peers {
        if (ref.peers[i].Addr != nil) && ref.peers[i].Addr.IP.Equal(addr.IP) {
            s, ok := ref.peers[i].ToMod[lr.FromMod]
            if ok && (s == DEXTRA_STAT_INIT) {
                p = &ref.peers[i]
                break
            }
        }
    }
    if p != nil {
        if lr.Ack {
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DExtra ackPkt(): %s:%s -> %s:%s linked",lr.Owner,lr.FromMod,
                    p.Owner,p.FromMod))
                p.ToMod[lr.FromMod] = DEXTRA_STAT_CONN
                //p.LastHeard = time.Now()
        } else {
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DExtra ackPkt(): %s:%s -> %s:%s link denied",lr.Owner,
                    lr.FromMod,p.Owner,p.FromMod))
            delete(p.ToMod, lr.FromMod)
        }
    } else {
        ref.ErChan <- errors.New(fmt.Sprintf(
            "DExtra ackPkt(): reply from unknown %+v",lr))
    }
    ref.peersmtx.Unlock()
}

func (ref *DExtra) hbtPkt(data []byte, addr *net.UDPAddr) {
    hb := DExtraHeartBeat{}
    err := hb.Fill(data)
    if err != nil {
        if ref.Cb.DExtraDebug == "yes" {
            ref.ErChan <- errors.New("DExtra hbtPkt(): Fill() " + err.Error())
        }
        return
    }
    if ref.Cb.DExtraDebug == "yes" {
        ref.ErChan <- errors.New(fmt.Sprintf(
            "DExtra hbtPkt(): DExtraHeartBeat: %+v", hb))
    }
    ref.peersmtx.Lock()
    for i := range ref.peers {
        if ref.peers[i].Owner == hb.Owner {
            // refresh only if it's a reflector peer..
            if ref.peers[i].Reflect != "" {
                ref.peers[i].LastHeard = time.Now()
                for m, s := range ref.peers[i].ToMod {
                    if s == DEXTRA_STAT_INIT {
                        ref.peers[i].ToMod[m] = DEXTRA_STAT_CONN
                    }
                }
                break
            }
            for _,s := range ref.peers[i].ToMod {
                // .. or connected to a local module
                if s == DEXTRA_STAT_CONN {
                    ref.peers[i].LastHeard = time.Now()
                    break
                }
            }
        }
    }
    ref.peersmtx.Unlock()
}

func (ref *DExtra) headerPkt(data []byte, addr *net.UDPAddr) {
    now := time.Now()
    dxfh := DExtraFrameHeader{}
    err := dxfh.Fill(data)
    if err != nil {
        if ref.Cb.DExtraDebug == "yes" {
            ref.ErChan <- errors.New("DExtra headerPkt(): Fill() " +
                err.Error())
        }
        return
    }
    crcok := dxfh.dshdr.CheckCRC()
    if ref.Cb.DExtraDebug == "yes" {
        ref.ErChan <- errors.New(fmt.Sprintf(
            "DExtra headerPkt(): received %+v (crcok: %t)", dxfh.dshdr, crcok))
    }
    if ref.Cb.DExtraIgnoreCRC != "yes" && !crcok {
        return
    }
    //owner := fmt.Sprintf("%-8s",dxfh.dshdr.DestRPT[:6])
    //frommod := dxfh.dshdr.DestRPT[7:8]
    ref.peersmtx.Lock()
    p := ref.findPeerByAddr(addr)
    if (p != nil) {
        if ref.Cb.DExtraDebug == "yes" {
            ref.ErChan <- errors.New("DExtra headerPkt(): peer found")
        }
        if p.Banned.Contains(dxfh.dshdr.MyCall1) {
            if ref.Cb.DExtraDebug == "yes" {
                ref.ErChan <- errors.New(fmt.Sprintf(
                "DExtra headerPkt(): received header from %s, but he's banned.",
                    dxfh.dshdr.MyCall1))
            }
            ref.peersmtx.Unlock()
            return
        }
        p.LastHeader = &dxfh.dshdr
        p.LastHeaderTime = now
        p.RxSess = uint16(dxfh.dxhdr.Session[0]) |
            (uint16(dxfh.dxhdr.Session[1]) << 8)
    }
    ref.peersmtx.Unlock()
}

func (ref *DExtra) framePkt(data []byte, addr *net.UDPAddr) {
    // Simulate packet loss ..
    //if rand.Float32() > 0.7 {
    //    return
    //}
    now := time.Now()
    dxfv := DExtraFrameVoice{}
    err := dxfv.Fill(data)
    if err != nil {
        if ref.Cb.DExtraDebug == "yes" {
            ref.ErChan <- errors.New("DExtra framePkt(): Fill() " + err.Error())
        }
        return
    }
    last := false
    if ((dxfv.dxhdr.Count & 0x40) == 0x40) {
        last = true
    }
    ref.peersmtx.Lock()
    sess := uint16(dxfv.dxhdr.Session[0]) | (uint16(dxfv.dxhdr.Session[1]) << 8)
    p := ref.findPeerByAddrSess(addr,sess)
    if (p != nil) && (p.LastHeader != nil) {
        if ref.Cb.DExtraDebug == "yes" {
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DExtra framePkt(): peer found %+v",p))
        }

        nhdr := &DSHeader{}
        nhdr.Fill((*p).LastHeader.Get())
        dsxf := DSXFrame{
            Frm: dxfv.dsfrm,
            FromMod: DEXTRA_FROM_LETTER,
            Seq: dxfv.dxhdr.Count,
            LastFrame: last,
        }
        p.RxBytes = p.RxBytes + DEXTRA_FRAMEP_SIZE

        if p.Reflect == "" {
            // very similar to txLocal(), except for the NAT .. 
            for tomod, stat := range p.ToMod {
                if stat != DEXTRA_STAT_CONN {
                    continue
                }
                // I dont care where he wants to go ..
                ref.InNat(nhdr,p.Owner,p.FromMod,tomod)
                if p.isMainOf(tomod) {
                    dsxf.FromMod = DEXTRA_MAIN_LETTER
                }
                dsxf.Hdr = *nhdr
                sdsxf := dsxf
                ref.exd <- &sdsxf
            }
        } else {
            dsxf.Hdr = *nhdr
            ref.txReflector(&dsxf, p)
        }

        p.LastHeaderTime = now
    }
    ref.peersmtx.Unlock()
}

func (ref *DExtra) getAddr(xref string) (*net.UDPAddr, error) {
    ipport := fmt.Sprintf("%s:%d",strings.TrimSpace(xref),DEXTRA_PORT)
    a, err := net.ResolveUDPAddr("udp",ipport)
    if err != nil {
        return nil,err
    }
    return a,nil
}

/*
func (ref *DExtra) isLinked(xref, frommod, tomod string) bool {
    ret := false
    ref.peersmtx.Lock()
    p := ref.findPeer(fmt.Sprintf("%-8s",xref), tomod)
    if p != nil && p.Status == DEXTRA_STAT_CONN {
        ret = true
    }
    ref.peersmtx.Unlock()
    return ret
}
*/

func (ref *DExtra) Unlink(xref, remmod, locmod string) string {
    // inverted logic for from and to ..
    ret := "Not found"
    xref = strings.ToUpper(xref)
    remmod = strings.ToUpper(remmod)
    locmod = strings.ToUpper(locmod)
    // XRFNNNYU -> unlink local "from" -> XRFNNN:Y
    // LLXNNNYU -> unlink local reflector mod X -> XRFNNN:Y
    if (ref.Cb.ModulesOwner != ref.Cb.DExtraOwner) && (xref[0:2] == "LL") {
        // unlink local reflector from remote reflecor
        xref = "XRF" + xref[3:6]
        locmod = xref[2:3]
    }
    ref.peersmtx.Lock()
    p := ref.findPeer(fmt.Sprintf("%-8s",xref), remmod)
    if p != nil {
        _ , ok := p.ToMod[locmod]
        ret = "Module not linked"
        if ok {
            delete(p.ToMod,locmod)
            if p.isMainOf(locmod) {
                p.removeMain(locmod)
            }
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DExtra Unlink(): %s -> %s:%s", locmod,p.Owner,p.FromMod))
            ret = fmt.Sprintf("Unlinked %s>%s%s",locmod,p.Owner,p.FromMod)
        }
        if len(p.ToMod) == 0 {
            var own string
            if p.Reflect != "" {
                own = fmt.Sprintf("%-8s",ref.Cb.DExtraOwner)
            } else {
                own = fmt.Sprintf("%-8s",ref.Cb.ModulesOwner)
            }
            p.Reconnect = false
            p.Reflect = ""
            p.LastHeard = time.Now().Add(-1*time.Minute)
            ur := DExtraLinkReq{
                Owner: own,
                FromMod: remmod,
                ToMod: " ",
            }
            if p.Addr != nil {
                _, err := ref.servConn.WriteToUDP(ur.Get(),p.Addr)
                if err != nil {
                    ref.ErChan <- errors.New("DExtra Unlink(): " +
                        err.Error())
                }
            }
            ref.ErChan <- errors.New(fmt.Sprintf(
                "DExtra Unlink(): %s -> %s:%s [D]",
                locmod,p.Owner,p.FromMod))
            ret = fmt.Sprintf("UNLINKED %s>%s%s",locmod,p.Owner,
                p.FromMod)
        }
    } else {
        ref.ErChan <- errors.New(fmt.Sprintf(
            "DExtra Unlink(): %s:%s not linked",xref,remmod))
        ret = "DEPeer not linked"
    }
    ref.peersmtx.Unlock()
    return ret
}

func (ref *DExtra) ReEstablishLink(xref, remmod, locmod string) string {
    return ref.establishLink(xref, remmod, locmod, false)
}

func (ref *DExtra) EstablishLink(xref, remmod, locmod string) string {
    return ref.establishLink(xref, remmod, locmod, true)
}

func (ref *DExtra) establishLink(xref, remmod, locmod string,
    setmain bool) string {
    // inverted logic for from and to ..
    var own string
    xref = strings.ToUpper(xref)
    remmod = strings.ToUpper(remmod)
    locmod = strings.ToUpper(locmod)

    // XRFNNNYL -> normal link local "from" -> XRFNNN:Y
    // LLXNNNYL -> local reflector mod X -> XRFNNN:Y
    localref := false
    connectto := ""
    if (ref.Cb.ModulesOwner != ref.Cb.DExtraOwner) && (xref[0:2] == "LL") {
        // link local reflector to remote reflecor
        connectto = xref
        locmod = xref[2:3]
        xref = "XRF" + xref[3:6]
        own = fmt.Sprintf("%-8s",ref.Cb.DExtraOwner)
        localref = true
    } else {
        // link local module to remote/local reflector
        own = fmt.Sprintf("%-8s",ref.Cb.ModulesOwner)
    }
    ref.ErChan <- errors.New(fmt.Sprintf(
        "DExtra EstablishLink(): %s:%s -> %s:%s",own,locmod,xref,remmod))
    var addr *net.UDPAddr
    var err error
    if xref == ref.Cb.DExtraOwner {
        // connect local reflector
        addr = nil
    } else {
        // connect external reflector
        addr, err = ref.getAddr(xref)
        if err != nil {
            ref.ErChan <- errors.New("DExtra EstablishLink(): " + err.Error())
            return "Not found"
        }
    }
    lr := DExtraLinkReq{
        Owner: own,
        FromMod: locmod,
        ToMod: remmod,
    }
    ref.peersmtx.Lock()
    modmain := ref.modGetMain(locmod)
    xref8 := fmt.Sprintf("%-8s",xref)
    p := ref.findPeer(xref8,lr.ToMod)
    var ret string
    if (p == nil) {
        // first time..
        p = &DEPeer{
            Addr: addr,
            Owner: xref8,
            FromMod: lr.ToMod,
            ToMod: map[string]byte{
                lr.FromMod: DEXTRA_STAT_INIT,
            },
            LastHeard: time.Now(),
            Reconnect: true,
            ConnectTo: connectto,
            Banned: &Bans{},
        }
        // if connect to local reflector or local reflector to ext reflector
        if addr == nil || localref {
            p.Reflect = lr.FromMod
        }
        if addr == nil {
            p.Reflect = lr.ToMod
        }
        if addr == nil {
            p.ToMod[lr.FromMod] = DEXTRA_STAT_CONN
            // set this to ~1 year ..
            p.LastHeard = time.Now().Add(8760*time.Hour)
        }
        p.TxSess = uint16(rand.Uint32() & 0x0000ffff)
        if setmain && !localref {
            p.MainMods = []string{locmod}
            if modmain != nil {
                modmain.removeMain(locmod)
            }
        }
        ref.peers = append(ref.peers,*p)
        if addr != nil {
            _, err = ref.servConn.WriteToUDP(lr.Get(),addr)
            if err != nil {
                ref.ErChan <- errors.New("DExtra EstablishLink()[1]: " +
                    err.Error())
            }
        }
        ret = "Linking (new peer)"
    } else {
        // peer already connected
        stat, ok := p.ToMod[locmod]
        if ok {
            if (stat != DEXTRA_STAT_CONN) {
                // I was already trying to connect ..
                ret = "Relinking"
                _, err = ref.servConn.WriteToUDP(lr.Get(),addr)
                if err != nil {
                    ref.ErChan <- errors.New(
                        "DExtra EstablishLink()[2]: " + err.Error())
                }
            } else {
                ret = "Already linked"
            }
        } else if p.Reflect == "" {
            // local mod wasn't connected
            p.ToMod[locmod] = DEXTRA_STAT_CONN
            ret = "Linked"
        } else {
            // probably trying to connect a different local ref module to remote
            // reflector??
            ret = "No. Pls link local " + p.Reflect
        }
        /*
        if usexrf {
            if (p.Reflect == "") {
                // join the reflector
                p.Reflect = lr.FromMod
                ret = "Joined"
            } else {
                // a local reflector is already linked
                ret = "Already joined"
            }
        }
        */
        if setmain {
            if (modmain != nil) && (modmain != p) {
                modmain.removeMain(locmod)
            }
            if (modmain == nil) || (modmain != p) {
                p.MainMods = append(p.MainMods,locmod)
            }
        }
    }
    ref.peersmtx.Unlock()
    return ret
}

func (ref *DExtra) infoDExtra(mod string) string {
    ret := "No Main"
    mod = strings.ToUpper(mod)
    ref.peersmtx.Lock()
    p := ref.modGetMain(mod)
    if p != nil {
        ret = fmt.Sprintf("%s>%s%s",mod,strings.TrimSpace(p.Owner),p.FromMod)
    }
    ref.peersmtx.Unlock()
    return ret
}

func (ref *DExtra) statusDExtra(mod string) string {
    ret := ""
    mod = strings.ToUpper(mod)
    ref.peersmtx.Lock()
    for _, v := range ref.peers {
        _, ok := v.ToMod[mod]
        if ok {
            ret = ret + fmt.Sprintf("%s%s",v.Owner[3:6],v.FromMod)
        }
    }
    ref.peersmtx.Unlock()
    if ret == "" {
        ret = "No DExtra peers"
    }
    return ret
}

func (ref *DExtra) Command(frommod, yocall, realfrom string) bool {
    // FIXME check realfrom to be local?
    now := time.Now()
    if (ref.lastCommandTime != nil) &&
        (ref.lastCommandTime.Add(2*time.Second).After(now)) {
        return true
    }
    ref.lastCommandTime = &now
    call := yocall[:6]
    mod := yocall[6:7]
    com := yocall[7:8]
    switch com {
        case "L":
            // link
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DEXTRA_MAIN_LETTER,
                ref.EstablishLink(call,mod,frommod))
            return true
        case "U":
            // unlink
            if mod == " " {
                mod = realfrom
            }
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DEXTRA_MAIN_LETTER,
                ref.Unlink(call,mod,frommod))
            return true
        case "I":
            // info
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DEXTRA_MAIN_LETTER,
                ref.infoDExtra(frommod))
            return true
        case "S":
            // status, return all (up to 5) connected peers
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DEXTRA_MAIN_LETTER,
                ref.statusDExtra(frommod))
            return true
        case "E":
            // echo
            ref.FbChan <- fmt.Sprintf("%s:%s:%s",frommod,DEXTRA_MAIN_LETTER,
                "Not implemented")
            return true
    }
    return false
}

func (ref *DExtra) txNet(f *DSXFrame, p *DEPeer, reflect bool) {
    dxhdr := DExtraFrameHeaderHeader{}
    copy(dxhdr.Pre[:],[]byte("DSVT"))
    dxhdr.Flags[0] = 0x00
    dxhdr.Flags[1] = 0x00
    dxhdr.Flags[2] = 0x00
    dxhdr.Flags[3] = 0x20
    dxhdr.Bands[0] = 0x00
    dxhdr.Bands[1] = 0x00
    dxhdr.Bands[2] = 0x00
    dxhdr.Session[0] = byte(p.TxSess & 0xff)
    dxhdr.Session[1] = byte((p.TxSess >> 8) & 0xff)
    if f.Seq == 0 {
        if reflect {
            ref.OutNat(&f.Hdr,ref.Cb.DExtraOwner,p.Reflect)
        } else {
            ref.OutNat(&f.Hdr,p.Owner,p.FromMod)
        }
        f.Hdr.SetCRC()
        dxfh := DExtraFrameHeader{}
        dxhdr.Type = 0x10
        dxhdr.Count = 0x80
        dxfh.dxhdr = dxhdr
        dxfh.dshdr = f.Hdr
        _, err := ref.servConn.WriteToUDP(dxfh.Get(), p.Addr)
        if err != nil {
            ref.ErChan <- errors.New("DExtra Tx(): " + err.Error())
        }
    }
    dxfv := DExtraFrameVoice{}
    dxhdr.Type = 0x20
    dxhdr.Count = f.Seq
    if f.LastFrame {
        dxhdr.Count = dxhdr.Count | 0x40
    }
    dxfv.dxhdr = dxhdr
    dxfv.dsfrm = f.Frm
    _, err := ref.servConn.WriteToUDP(dxfv.Get(),p.Addr)
    if err != nil {
        ref.ErChan <- errors.New("DExtra Tx(): " + err.Error())
    }
}

func (ref *DExtra) txLocal(f *DSXFrame, p *DEPeer) {
    for m, s := range p.ToMod {
        if (m == f.FromMod) || (s != DEXTRA_STAT_CONN) {
            continue
        }
        f.Hdr.Nat(f.Hdr.DepaRPT,f.Hdr.DestRPT,f.FromMod,m)
        // I can avoid SetCRC() here, the pkt isn't going out yet
        ref.exd <- f
    }
}

func (ref *DExtra) txReflector(f *DSXFrame, p *DEPeer) {
    // CAUTION: to be called with mutex already set

    // collect information
    // TODO Could I do all this every once in a while (when receiving header
    // for net packets for ex) instead of doing it in every frame? (frmPkt())
    // Is it worth?
    lmods := make(map[string]bool,0) // so I get rid of duplicates
    rpeers := make([]*DEPeer,0)
    for i := range ref.peers {
        if ref.peers[i].Reflect == p.Reflect {
            if ref.peers[i].Addr == nil {
                // collect all locally connected mods
                for k, v := range ref.peers[i].ToMod {
                    if v == DEXTRA_STAT_CONN {
                        _, ok := lmods[k]
                        if !ok {
                            if ref.peers[i].isMainOf(k) {
                                lmods[k] = true
                            } else {
                                lmods[k] = false
                            }
                        }
                    }
                }
            } else {
                // collect all reflector peers
                // tx to peer if not source peer
                if (f.FromMod != DEXTRA_FROM_LETTER &&
                    f.FromMod != DEXTRA_MAIN_LETTER) || (p != &(ref.peers[i])) {
                    rpeers = append(rpeers,&(ref.peers[i]))
                }
            }
        }
    }
    if ref.Cb.DExtraDebug == "yes" {
        ref.ErChan <- errors.New("DExtra txReflector(): gathered lmods")
        for m, ismain := range lmods {
            ref.ErChan <- errors.New(fmt.Sprintf("Mod: %s isMain: %t",m,ismain))
        }
        ref.ErChan <- errors.New("DExtra txReflector(): gathered peers")
        for _,peer := range rpeers {
            ref.ErChan <- errors.New(fmt.Sprintf("Owner: %s:%s",peer.Owner,
                peer.FromMod))
        }
    }
    // tx to all collected local modules
    for m, ismain := range lmods {
        if m != f.FromMod {
            if ismain {
                f.FromMod = DEXTRA_MAIN_LETTER
            } else {
                f.FromMod = DEXTRA_FROM_LETTER
            }
            f.Hdr.Nat(f.Hdr.DepaRPT,ref.Cb.ModulesOwner,f.FromMod,m)
            // no SetCRC() here
            lf := *f
            ref.exd <- &lf
        }
    }
    // tx to all peers
    for i := range rpeers {
        pf := *f
        ref.txNet(&pf,rpeers[i],true)
    }
}

func (ref *DExtra) InNat(dsh *DSHeader, owner, remmod, tomod string) {
    dsh.Nat(owner, ref.Cb.ModulesOwner, remmod, tomod)
}

func (ref *DExtra) OutNat(dsh *DSHeader, dest, frommod string) {
    dsh.Nat(dest, dest, "G", frommod)
}

func (ref *DExtra) Tx(f *DSXFrame) {
    if strings.TrimSpace(f.Hdr.YoCall) != "CQCQCQ" &&
        ref.Command(f.Hdr.DepaRPT[7:8],f.Hdr.YoCall,f.FromMod) {
        return
    }

    ref.peersmtx.Lock()
    p := ref.modGetMain(f.FromMod)

    if (p != nil) && ((p.UsedBy == "") || (p.UsedBy == f.FromMod)) {
        p.UsedBy = f.FromMod
        p.UsedByTime = time.Now()
        if p.Reflect == "" {
            // careful ..  *f is always the same!!!
            // forward packet to locals ToMods
            lf := *f
            ref.txLocal(&lf, p)
            // forward packet to net
            nf := *f
            ref.txNet(&nf, p, false)
        } else if p.Addr == nil {
            // if reflector, to clients and connected local mods
            ref.txReflector(f,p)
        }
    }
    ref.peersmtx.Unlock()
}

/* DPlus (20001), DExtra (30001), DCS: reflectors
 * CCS ircddb: routing
 */

/* - linking request
 * XRF: owner 8, from_mod 1, to_mod 1, \0
 * XRF: KJ4NHF__BAACK0 or KJ4NHF__BANAK0 B is my mod; ACK ok NAK rejected
 * REF: 5 0 24 0 1
 * - unlink
 * XRF: owner 8, from_mod 1, ' ', \0
 * REF: 5 0 24 0 0
 * - send heartbeat
 * XRF: owner 7, from_mod 1, to_mod 1, \0 (scott) XRF038_AB0
 * XRF: owner 8, from_mod 1, to_mod 1, \0 (johnatan??? from tcpdump)
 * REF: 3 96 0
 * - voice
 * XRF: IF connected
 * (len==56|len==27) & (starts with DSVT) & ([4]==0x10|[4]==0x20) & ([8]==0x20)
 * 56: incorrect flags settings (?)
 *  [0:4] DSVT
 *  [4:8] = 0x10 0x00 0x00 0x00 (?)
 *  [8:12] = 0x20 0x00 0x01 0x00
 *  [12:14] streamID
 *  [14] counter (0x00-0x14), if % 0x40 != 0 END stream
 *  [15:18] flags
 *  [18:26] rpt1
 *  [26:34] rpt2
 *  [34:42] YoCall
 *  [42:50] MyCall
 *  [50:54] MyCall2
 *  [54:56] crc
 * voice packet come prima ma len==27 e:
 *  [4:8] = 0x20 0x00 0x00 0x00
 * G2: same as XRF but len+2 and
 *  [0]=58 [1]=(58>>8 & 0x1f), [1]=[1]|0xffffff80 (WTF [1] is a byte!!)
 */

/* check timeout on remotes
 * check timeout on dongles
 */
