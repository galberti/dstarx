
package dstar

import (
    "fmt"
)

func (ref *DExtra) StatusString() string {
    ret := fmt.Sprintf("%21s %-8s %-4s %-8s %-5s %-5s %-2s %-8s %-2s\n",
        "IP","Owner","Remo","Local","Recon","Mains","UB","LastHDR","RM")
    for _,v := range ref.peers {
        ret += fmt.Sprintf("%21s %8s %-4s",v.Addr,v.Owner,v.FromMod)
        r1 := ""
        for k, vv := range v.ToMod {
            r1 += fmt.Sprintf("%s%d,",k,vv)
        }
        if len(r1) > 0 {
            ret += fmt.Sprintf(" %-8s",r1[:len(r1)-1])
        } else {
            ret += fmt.Sprintf(" %-8s",r1)
        }
        ret += fmt.Sprintf(" %-5t",v.Reconnect)
        r1 = ""
        for _, vv := range v.MainMods {
            r1 += fmt.Sprintf("%s",vv)
        }
        ret += fmt.Sprintf(" %-5s %-2s",r1,v.UsedBy)
        if v.LastHeader != nil {
            ret += fmt.Sprintf(" %-8s",v.LastHeader.MyCall1)
        } else {
            ret += fmt.Sprintf(" %-8s","-")
        }
        ret += fmt.Sprintf(" %-2s",v.Reflect)
        ret += "\n"
    }
    return ret
}

func (ref *DCS) StatusString() string {
    ret := fmt.Sprintf("%21s %-8s %-4s %-8s %-5s %-2s\n",
        "IP","Owner","Remo","Local","Mains","UB")
    for _,v := range ref.peers {
        ret += fmt.Sprintf("%21s %8s %-4s",v.Addr(),v.Remote(),v.RemMod())
        r1 := ""
        for k, vv := range v.LocMods() {
            r1 += fmt.Sprintf("%s%d,",k,vv)
        }
        if len(r1) > 0 {
            ret += fmt.Sprintf(" %-8s",r1[:len(r1)-1])
        } else {
            ret += fmt.Sprintf(" %-8s",r1)
        }
        r1 = ""
        for _, vv := range v.MainMods() {
            r1 += fmt.Sprintf("%s",vv)
        }
        ret += fmt.Sprintf(" %-5s %-2s",r1,v.UsedBy())
        ret += "\n"
    }
    return ret
}

