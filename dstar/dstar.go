
package dstar

import (
    "errors"
    "fmt"
    "strings"
    "github.com/howeyc/crc16"
    "net"
    "time"
    "sync"
    "bytes"
    "gitlab.com/galberti/dstarx/confbox"
)

const (
    DSTAR_HEADER_SIZE = 41 // 1 + 1 + 1 + 8 + 8 + 8 + 8 + 4 + 2 bytes
    DSTAR_FRAME_SIZE  = 12 // 72 + 24 bits
    DSTAR_VOICE_SIZE  = 9
    DSTAR_SLOWD_SIZE  = 3

    DEXTRA_MAIN_LETTER = "M"
    DEXTRA_FROM_LETTER = "N"
    DCS_MAIN_LETTER = "O"
    DCS_FROM_LETTER = "P"
)

// network bit order
var InitSync = []byte{
    0,
    1,0,1,0,
    1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0,
    1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0,


    1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0,
    1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0,
    1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0,
    1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0,
    1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0,
    1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0, 1,0,1,0,1,0,1,0,


    1,1,1,0,1,1,0,0,1,0,1,0,0,0,0,
}

// normal order
var Silence = []byte{0x4e, 0x8d, 0x32, 0x88, 0x26, 0x1a, 0x3f, 0x61, 0xe8}
var Sync_seq = []byte{0x55,0x2d,0x16}
var Term_seq = []byte{0x55,0x55,0x55,0x55,0xc8,0x7a}
var Scrambler_slow = []byte{0x70, 0x4f, 0x93}
var Fixed_slow = []byte{'@','A','B','C'}

func Swapbits(b byte) byte {
    ret := ((b >> 7) & 0x01)
    ret |= ((b >> 5) & 0x02)
    ret |= ((b >> 3) & 0x04)
    ret |= ((b >> 1) & 0x08)
    ret |= ((b << 1) & 0x10)
    ret |= ((b << 3) & 0x20)
    ret |= ((b << 5) & 0x40)
    ret |= ((b << 7) & 0x80)
    return ret
}

func SliceSwapBits(s []byte) []byte {
    out := make([]byte, len(s))
    for i, _ := range s {
        out[i] = Swapbits(s[i])
    }
    return out
}

func BytesToBits(in []byte) []byte {
    out := make([]byte, 0)
    for i, _ := range in {
        for j := 0; j < 8; j++ {
            out = append(out, (in[i] >> uint8(7 - j)) & 0x01)
        }
    }
    return out
}

func BitsToBytes(in []byte) []byte {
    out := make([]byte, 0)
    for i := 0; i < len(in); i += 8 {
        o := byte(0)
        for j := 0; j < 8; j++ {
            o |= (in[i+j] << uint8(7 - j))
        }
        out = append(out, o)
    }
    return out
}

type DSHeader struct {
    Flag1 byte
    Flag2 byte
    Flag3 byte
    DestRPT string
    DepaRPT string
    YoCall string
    MyCall1 string
    MyCall2 string
    Crc [2]byte
}

func (dsh *DSHeader) Fill(raw []byte) error {
    if len(raw) != DSTAR_HEADER_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DSTAR_HEADER_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    dsh.Flag1 = raw[0]
    dsh.Flag2 = raw[1]
    dsh.Flag3 = raw[2]
    dsh.DestRPT = string(raw[3:11])
    dsh.DepaRPT = string(raw[11:19])
    dsh.YoCall = string(raw[19:27])
    dsh.MyCall1 = string(raw[27:35])
    dsh.MyCall2 = string(raw[35:39])
    copy(dsh.Crc[:],raw[39:41])
    return nil
}

func (dsh *DSHeader) Get() []byte {
    ret := make([]byte, DSTAR_HEADER_SIZE)
    ret[0] = dsh.Flag1
    ret[1] = dsh.Flag2
    ret[2] = dsh.Flag3
    copy(ret[3:11],[]byte(fmt.Sprintf("%-8s",dsh.DestRPT)))
    copy(ret[11:19],[]byte(fmt.Sprintf("%-8s",dsh.DepaRPT)))
    copy(ret[19:27],[]byte(fmt.Sprintf("%-8s",dsh.YoCall)))
    copy(ret[27:35],[]byte(fmt.Sprintf("%-8s",dsh.MyCall1)))
    copy(ret[35:39],[]byte(fmt.Sprintf("%-4s",dsh.MyCall2)))
    copy(ret[39:],dsh.Crc[:])
    return ret
}

func (dsh *DSHeader) CheckCRC() bool {
    var xexpec uint16 = (uint16(dsh.Crc[1]) << 8) | uint16(dsh.Crc[0])
    if xexpec ^ crc16.ChecksumCCITT(dsh.Get()[:DSTAR_HEADER_SIZE-2]) == 0 {
        return true
    }
    return false
}

func (dsh *DSHeader) SetCRC() {
    crc := crc16.ChecksumCCITT(dsh.Get()[:DSTAR_HEADER_SIZE-2])
    dsh.Crc[0] = uint8(crc)
    dsh.Crc[1] = uint8(crc >> 8)
}

func (dsh *DSHeader) Nat(deparpt, destrpt, frommod, tomod string) {
    depat := fmt.Sprintf("%-7s",strings.TrimSpace(deparpt))
    destt := fmt.Sprintf("%-7s",strings.TrimSpace(destrpt))
    dsh.DepaRPT = fmt.Sprintf("%-7s%s",depat[:7],frommod[:1])
    dsh.DestRPT = fmt.Sprintf("%-7s%s",destt[:7],tomod[:1])
}

func (dsh *DSHeader) GetNetworkBits() []byte {
    dsb := dsh.Get()
    bs := SliceSwapBits(dsb)
    bits := BytesToBits(bs)
    bits = append(bits, []byte{0x00, 0x00}...)
    //for _, e := range bits {
    //    fmt.Printf("%d", e)
    //}
    //fmt.Println()
    return Scramble(Interleave(FECEncode(bits)))
}

type DSFrame struct {
    //voice [9]byte
    //xtra [3]byte
    // Until I find a better use for those .. 
    data [12]byte
}

func (dsf *DSFrame) Fill(raw []byte) error {
    if len(raw) != DSTAR_FRAME_SIZE {
        return errors.New("Wrong raw size, should be " +
            fmt.Sprintf("%d",DSTAR_FRAME_SIZE) + ", not " +
            fmt.Sprintf("%d",len(raw)))
    }
    copy(dsf.data[:],raw)
    return nil
}

func (dsf *DSFrame) Get() []byte {
    return dsf.data[:]
}

func (dsf *DSFrame) IsSync() bool {
    return bytes.Equal(dsf.data[9:12], Sync_seq)
}

type DSXFrame struct {
    Hdr DSHeader
    Frm DSFrame
    FromMod string
    Seq byte
    LastFrame bool
}

func GetTermSeq() [][]byte {
    // term frame1
    tf1 := make([]byte,DSTAR_FRAME_SIZE)
    copy(tf1[:DSTAR_VOICE_SIZE],Silence)
    copy(tf1[DSTAR_VOICE_SIZE:],Term_seq[:DSTAR_SLOWD_SIZE])
    // term frame2
    tf2 := make([]byte,DSTAR_FRAME_SIZE)
    copy(tf2[:DSTAR_SLOWD_SIZE],Term_seq[DSTAR_SLOWD_SIZE:])
    return [][]byte{tf1,tf2}
}

func GetSilentMessage(message string) [][]byte {
    // sync 552d16
    // 8 pacchi (@_A_B_C_)
    // term flag ..xxxx555555
    // term flag (2) 55c87axxxx..
    var bmess [20]byte
    copy(bmess[:20],[]byte(fmt.Sprintf("%-20s",message)))
    var ret [][]byte

    // sync frame
    sf := make([]byte,DSTAR_FRAME_SIZE)
    copy(sf[:DSTAR_VOICE_SIZE],Silence)
    copy(sf[DSTAR_VOICE_SIZE:],Sync_seq)
    ret = append(ret,sf)

    for i := 0; i < 8; i++ {
        df := make([]byte,DSTAR_FRAME_SIZE)
        copy(df[:DSTAR_VOICE_SIZE],Silence)
        if (i % 2) == 1 { // odd frames
            df[DSTAR_VOICE_SIZE+0] = bmess[((i-1)/2*5)+2] ^ Scrambler_slow[0]
            df[DSTAR_VOICE_SIZE+1] = bmess[((i-1)/2*5)+3] ^ Scrambler_slow[1]
            df[DSTAR_VOICE_SIZE+2] = bmess[((i-1)/2*5)+4] ^ Scrambler_slow[2]
        } else { // even frames
            df[DSTAR_VOICE_SIZE+0] = Fixed_slow[i/2]  ^ Scrambler_slow[0]
            df[DSTAR_VOICE_SIZE+1] = bmess[(i/2*5)+0] ^ Scrambler_slow[1]
            df[DSTAR_VOICE_SIZE+2] = bmess[(i/2*5)+1] ^ Scrambler_slow[2]
        }
        ret = append(ret,df)
    }

    // This is sent by satoshi mod
    tfs := GetTermSeq()
    ret = append(ret,tfs[0])
    ret = append(ret,tfs[1])

    return ret
}

/* Peers stuff */

type IPeer interface {
    Addr() *net.UDPAddr
    isMainOf(string) bool
    Remote() string
    RemMod() string
    LocMod(string) (byte, bool)
    LocMods() map[string]byte
    SetLocMod(string,byte)
    DeleteLocMod(string)
    LastHeard() time.Time
    LastHeardNow()
    //SetLastHeard(time.Time)
    MainMods() []string
    AppendMainMod(string)
    RemoveMainMod(string)
    HasSameIP(*net.UDPAddr) bool
    UsedBy() string
    SetUsedBy(string)
    TxSess() uint16
    SetTxSess(uint16)
    TxCounter() uint32
    ResetTxCounter()
    IncTxCounter()
    UsedByTime() time.Time
    UsedByTimeNow()
    // byte accounting
    RxBytes() uint32
    AddRxBytes(uint32)
    ResetRxBytes()
    AccountTime() time.Time
    AccountTimeNow()
    ExcessBW() byte
    IncExcessBW()
    ResetExcessBW()
}

type Peer struct {
    addr *net.UDPAddr
    remote string
    remMod string
    locMod map[string]byte
    lastHeard time.Time       // last time I heard this peer
    mainMods []string         // these mods have this peer as main
    usedBy string             // this peer is used for Tx by this Module..
    usedByTime time.Time      // ..and its time
    txCounter uint32          // the counter, reset every new Tx
    txSess uint16             // new one every new Tx
    rxbytes uint32
    accountTime time.Time
    excessBW byte
}

func (p *Peer) ExcessBW() byte {
    return p.excessBW
}

func (p *Peer) IncExcessBW() {
    p.excessBW = p.excessBW + 1
}

func (p *Peer) ResetExcessBW() {
    p.excessBW = 0
}

func (p *Peer) UsedByTime() time.Time {
    return p.usedByTime
}

func (p *Peer) UsedByTimeNow() {
    p.usedByTime = time.Now()
}

func (p *Peer) LastHeard() time.Time {
    return p.lastHeard
}

func (p *Peer) IncTxCounter() {
    p.txCounter++
}

func (p *Peer) TxCounter() uint32 {
    return p.txCounter
}

func (p *Peer) ResetTxCounter() {
    p.txCounter = 0
}

func (p *Peer) TxSess() uint16 {
    return p.txSess
}

func (p *Peer) SetTxSess(s uint16) {
    p.txSess = s
}

func (p *Peer) SetUsedBy(b string) {
    p.usedBy = b
}

func (p *Peer) UsedBy() string {
    return p.usedBy
}

func (p *Peer) Addr() *net.UDPAddr {
    return p.addr
}

//func (p *Peer) SetLastHeard(t time.Time) {
//    p.LastHeard = t
//}

func (p *Peer) LastHeardNow() {
    p.lastHeard = time.Now()
}

func (p *Peer) HasSameIP(addr *net.UDPAddr) bool {
    return bytes.Equal(addr.IP[len(addr.IP)-4:len(addr.IP)-1],
        p.addr.IP[len(p.addr.IP)-4:len(p.addr.IP)-1])
}

func (p *Peer) LocMods() map[string]byte {
    return p.locMod
}

func (p *Peer) LocMod(mod string) (byte, bool) {
    v, ok := p.locMod[mod]
    return v, ok
}

func (p *Peer) SetLocMod(mod string, b byte) {
    if p.locMod == nil {
        p.locMod = make(map[string]byte)
    }
    p.locMod[mod] = b
}

func (p *Peer) DeleteLocMod(mod string) {
    delete(p.locMod,mod)
}

func (p *Peer) MainMods() []string {
    return p.mainMods
}

func (p *Peer) AppendMainMod(mod string) {
    if p.mainMods == nil {
        p.mainMods = make([]string,0)
    }
    p.mainMods = append(p.mainMods,mod)
}

func (p *Peer) RemoveMainMod(mod string) {
    nm := make([]string,0)
    for i := range p.mainMods {
        if p.mainMods[i] != mod {
            nm = append(nm, p.mainMods[i])
        }
    }
    p.mainMods = nm
}

func (p *Peer) isMainOf(mod string) bool {
    for _, v := range p.mainMods {
        if mod == v {
            return true
        }
    }
    return false
}

func (p *Peer) Remote() string {
    return p.remote
}

func (p *Peer) RemMod() string {
    return p.remMod
}

func (p *Peer) RxBytes() uint32 {
    return p.rxbytes
}

func (p *Peer) AddRxBytes(b uint32) {
    p.rxbytes = p.rxbytes + b
}

func (p *Peer) ResetRxBytes() {
    p.rxbytes = 0
}

func (p *Peer) AccountTime() time.Time {
    return p.accountTime
}

func (p *Peer) AccountTimeNow() {
    p.accountTime = time.Now()
}

/* Net status stuff */

type Status interface {
    modGetMain(string) *Peer
    EstablishLink(string,string,string) string
    Unlink(string,string,string) string
}

type NetStatus struct {
    Cb *confbox.ConfBox
    FbChan chan<- string
    ErChan chan<- error
    exd chan<- *DSXFrame
    peers []IPeer
    peersmtx sync.Mutex
    servConn *net.UDPConn
    lastCommandTime *time.Time
}

func (ref *NetStatus) modGetMain(mod string) *IPeer {
    var modmain *IPeer = nil
    for i, _ := range ref.peers {
        if ref.peers[i].isMainOf(mod) {
            modmain = &ref.peers[i]
            break
        }
    }
    return modmain
}

func (ref *NetStatus) findPeer(remote, remmod string) *IPeer {
    // CAUTION: to be called with mutex already set
    var ret *IPeer = nil
    if strings.TrimSpace(remote) == "" {
        for i := range ref.peers {
            if (ref.peers[i].isMainOf(remmod)) {
                ret = &(ref.peers[i])
                break
            }
        }
    } else {
        for i := range ref.peers {
            if (remote == ref.peers[i].Remote()) &&
                (remmod == ref.peers[i].RemMod()) {
                ret = &(ref.peers[i])
                break
            }
        }
    }
    return ret
}

