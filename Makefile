
GO = go
VERSION = $(shell git log -n 1 --format='%ai %h')

all: dstarx

version:
	echo 'package main' > version.go
	echo 'var VERSION = "$(VERSION)"' >> version.go

## dependencies

# crc16
CRC_SRC = github.com/howeyc/crc16
$(GOPATH)/src/$(CRC_SRC)/crc16.go:
	go get $(CRC_SRC)
crc16: $(GOPATH)/src/$(CRC_SRC)/crc16.go

# gousb
USB_SRC = github.com/kylelemons/gousb/usb
$(GOPATH)/src/$(USB_SRC)/usb.go:
	go get $(USB_SRC)
gousb: $(GOPATH)/src/$(USB_SRC)/usb.go

# serial
SER_SRC = github.com/tarm/serial
$(GOPATH)/src/$(SER_SRC)/serial.go:
	go get $(SER_SRC)
goserial: $(GOPATH)/src/$(SER_SRC)/serial.go

deps: crc16 gousb goserial

dstarx: deps version
	$(GO) build

clean:
	rm dstarx version.go
