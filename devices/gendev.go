
package devices

import (
    "fmt"
    "errors"
    "sync"
    "time"
    "gitlab.com/galberti/dstarx/confbox"
    "gitlab.com/galberti/dstarx/dstar"
    "github.com/kylelemons/gousb/usb"
)

const (
    TYPE_SATOSHI = iota
    TYPE_DVRPTRV2
)

/* Interfaces */

type IDevice interface {
    /* implemented in generic class */
    FIFOInit()
    FIFOClean()
    FIFOFlush()
    FIFOPush(*dstar.DSXFrame)
    StartReadLoops(chan<- *dstar.DSXFrame) error
    StartWriteLoops() error
    /* helpers implemented in generic class */
    LastHeaderTime() time.Time
    SetLastHeaderTime(time.Time)
    UsedByMutexLock()
    UsedByMutexUnlock()
    UsedBy() string
    SetUsedBy(string)
    MainNet() string
    SetMainNet(string)
    Module() string
    SetModule(string)
    LastUsedBy() string
    SetLastUsedBy(string)
    UsedByTime() time.Time
    SetUsedByTime(time.Time)
    LastHeader() *dstar.DSHeader
    SetLastHeader(*dstar.DSHeader)
    /* To implement in child classes */
    SetPTT(bool) error
    GetPTT() (bool, error)
    GetVersion() (string, error)
    Close() error
    GetModule() string
    IsDuplex() bool
}

type ICtx interface {
    Init(chan<- error) error
    GetModules() []string
    Tx(*dstar.DSXFrame, chan<- string, chan<- error)
    SetMain(string, string)
    GetLastUsedBy(string) string
    StartReceiving(chan<- *dstar.DSXFrame, chan<- error)
    PrepareTransmission(chan<- error)
    Close()
    Ctx() *usb.Context
    SetCtx(*usb.Context)
    GetDevices(usb.ID) ([]*usb.Device, error)
    SetSs([]IDevice)
}

/* GenericDevice type */

type GenericDevice struct {
    ubmtx sync.Mutex // for accessing UsedBy
    Cb *confbox.ConfBox
    Dch chan *dstar.DSXFrame
    Erc chan<- error
    usedBy string
    usedByTime time.Time
    lastUsedBy string
    lastHeader *dstar.DSHeader
    lastHeaderTime time.Time
    mainNet string
    module string
    // FIFO stuff
    fifomtx sync.Mutex // for FIFO operations
    ofrm *dstar.DSXFrame
    pkts [21]*dstar.DSFrame
    Thr int8 // how many we want in before spitting out
    num int8 // how many we have in queue
    out int8 // out seqno
    prev int8 // last seq received
    silence dstar.DSFrame
}

func (gd *GenericDevice) FIFOInit() {
    gd.fifomtx.Lock()
    ss := make([]byte,12)
    copy(ss,dstar.Silence)
    copy(ss[dstar.DSTAR_VOICE_SIZE:],dstar.Scrambler_slow)
    gd.silence.Fill(ss)
    gd.num = -1
    gd.fifomtx.Unlock()
}

func (gd *GenericDevice) FIFOClean() {
    gd.fifomtx.Lock()
    for i := 0; i < len(gd.pkts); i++ {
        gd.pkts[i] = nil
    }
    gd.num = -1
    gd.fifomtx.Unlock()
}

func (gd *GenericDevice) FIFOFlush() {
    gd.fifomtx.Lock()
    //fmt.Printf("FLUSH num: %d out: %d\n", gd.num, gd.out)
    //for i:=0; i<21; i++ {
    //    if gd.pkts[i] == nil {
    //        fmt.Printf("%02d <nil>\n",i)
    //    } else {
    //        fmt.Printf("%02d %v\n", i, *gd.pkts[i])
    //    }
    //}
    for gd.num > 0 {
        if gd.pkts[gd.out] != nil {
            gd.ofrm.Frm = *gd.pkts[gd.out]
        } else {
            gd.ofrm.Frm = gd.silence
        }
        gd.ofrm.Seq = byte(gd.out)
        if gd.num == 1 {
            gd.ofrm.Seq |= 0x40
        }
        fc := *gd.ofrm
        gd.Dch <- &fc
        gd.out = gd.out + 1
        if gd.out == 21 {
            gd.out = 0
        }
        gd.num = gd.num - 1
    }
    gd.fifomtx.Unlock()
    gd.FIFOClean()
}

func (gd *GenericDevice) FIFOPush(tf *dstar.DSXFrame) {
    f := *tf
    seq := (f.Seq & ^byte(0x40))
    if seq > 20 {
        // FIXME return a bool and log elsewhere
        gd.Erc <- errors.New("FIFOPush() Invalid sequence!!")
        return
    }
    gd.fifomtx.Lock()
    if gd.num == -1 {
        gd.ofrm = &f
        gd.out = int8(seq)
        gd.prev = int8(seq)
        gd.num = 1
    }
    //fmt.Printf("1 seq:%02d prev:%d %v\n",seq,gd.prev,f.Frm)
    //fmt.Printf("2 num %d\n",gd.num)
    if gd.pkts[seq] == nil {
        // nil (above) protect from silly sequences 0101234..
        // compute also the missing packets
        r := ((int8(seq) - gd.prev) % 21)
        // I should count in a more precise way, but this "only forward"
        // counting seems to give the best results so far..
        if r < 0 {
            r = r + 21
        }
        if r < 5 {
            gd.num = gd.num + r
            gd.prev = int8(seq)
        }
        //fmt.Printf("3 r:%d num:%d\n",r, gd.num)
    }
    gd.pkts[seq] = &f.Frm
    if f.LastFrame {
        gd.fifomtx.Unlock()
        if gd.Cb.RouteDebug == "yes" {
            gd.Erc <- errors.New("FIFOPush() LastFrame Flush")
        }
        gd.FIFOFlush()
        return
    }
    pktout := 0
    for (gd.num > gd.Thr) && (pktout < 20) {
        if gd.pkts[gd.out] != nil {
            gd.ofrm.Frm = *gd.pkts[gd.out]
            gd.pkts[gd.out] = nil
        } else {
            gd.ofrm.Frm = gd.silence
            pktout = pktout + 1
        }
        gd.num = gd.num - 1
        gd.ofrm.Seq = byte(gd.out)
        fc := *gd.ofrm
        gd.Dch <- &fc
        gd.out = gd.out + 1
        if gd.out == 21 {
            gd.out = 0
        }
    }
    gd.fifomtx.Unlock()
    if pktout == 20 {
        // probably last frame arrived without LastFrame set .. 
        gd.Erc <- errors.New("FIFOPush() backup Flush")
        gd.FIFOFlush()
    }
}

func (gd *GenericDevice) SetPTT(on bool) error {
    return errors.New("This must be implemented")
}

//func (gd *GenericDevice) GetPTT() (bool, error) {
//    return false, errors.New("This must be implemented")
//}

//func (gd *GenericDevice) GetVersion() (string, error) {
//    return "", errors.New("This must be implemented")
//}

//func (gd *GenericDevice) Close() error {
//    return errors.New("This must be implemented")
//}

//func (gd *GenericDevice) StartReadLoops(data chan<- *dstar.DSXFrame,
//    erc chan<- error) error {
//    return errors.New("This must be implemented")
//}

//func (gd *GenericDevice) GetModule() (string, error) {
//    return "", errors.New("This must be implemented")
//}

//func (gd *GenericDevice) IsDuplex() bool {
    // This must be implemented
//    return false
//}

func (gd *GenericDevice) LastHeader() *dstar.DSHeader {
    return gd.lastHeader
}

func (gd *GenericDevice) SetLastHeader(toset *dstar.DSHeader) {
    gd.lastHeader = toset
}

func (gd *GenericDevice) LastHeaderTime() time.Time {
    return gd.lastHeaderTime
}

func (gd *GenericDevice) SetLastHeaderTime(toset time.Time) {
    gd.lastHeaderTime = toset
}

func (gd *GenericDevice) UsedByMutexLock() {
    gd.ubmtx.Lock()
}

func (gd *GenericDevice) UsedByMutexUnlock() {
    gd.ubmtx.Unlock()
}

func (gd *GenericDevice) UsedBy() string {
    return gd.usedBy
}

func (gd *GenericDevice) SetUsedBy(toset string) {
    gd.usedBy = toset
}

func (gd *GenericDevice) MainNet() string {
    return gd.mainNet
}

func (gd *GenericDevice) SetMainNet(toset string) {
    gd.mainNet = toset
}

func (gd *GenericDevice) Module() string {
    return gd.module
}

func (gd *GenericDevice) SetModule(toset string) {
    gd.module = toset
}

func (gd *GenericDevice) LastUsedBy() string {
    return gd.lastUsedBy
}

func (gd *GenericDevice) SetLastUsedBy(toset string) {
    gd.lastUsedBy = toset
}

func (gd *GenericDevice) UsedByTime() time.Time {
    return gd.usedByTime
}

func (gd *GenericDevice) SetUsedByTime(toset time.Time) {
    gd.usedByTime = toset
}

/* GenericCtx type */

type GenericCtx struct {
    ctx *usb.Context
    ss []IDevice
    Cb *confbox.ConfBox
}

func (gctx *GenericCtx) Ctx() *usb.Context {
    return gctx.ctx
}

func (gctx *GenericCtx) SetCtx(toset *usb.Context) {
    gctx.ctx = toset
}

func (gctx *GenericCtx) GetDevices(devid usb.ID) ([]*usb.Device, error) {
    //uctx.Debug(3)
    devs, err := gctx.ctx.ListDevices(func(desc *usb.Descriptor) bool {
        if desc.Vendor == devid {
            return true
        }
        return false
    })
    if err != nil {
        return nil, err
    }
    return devs, nil
}

//func (gctx *GenericCtx) Init(erc chan<- error) error {
//    return errors.New("This must be implemented")
//}

func (gctx *GenericCtx) GetModules() []string {
    ret := make([]string,0)
    for i, _ := range gctx.ss {
        ret = append(ret, gctx.ss[i].GetModule())
    }
    return ret
}

func (gctx *GenericCtx) Tx(f *dstar.DSXFrame, fbc chan<- string,
    erc chan<- error) {
    omod := f.Hdr.DestRPT[7:8]
    f.Hdr.SetCRC()
    for i, _ := range gctx.ss {
        if gctx.Cb.RouteDebug == "yes" {
            erc <- errors.New(fmt.Sprintf("Generic Tx(): %+v",gctx.ss[i]))
        }
        //fmt.Printf("omod: *%s*\n",omod)
        //fmt.Printf("module: *%s*\n",gctx.ss[i].Module())
        if (omod == gctx.ss[i].GetModule()) &&
            ( gctx.ss[i].IsDuplex() ||
            (gctx.ss[i].LastHeaderTime().Add(200*time.Millisecond).Before(
                time.Now())) ) {

            //fmt.Printf("used: %s\n",gctx.ss[i].UsedBy())
            //fmt.Printf("frommod: %s\n",f.FromMod)
            //fmt.Printf("mainnet: %s\n",gctx.ss[i].MainNet())

            gctx.ss[i].UsedByMutexLock()
            if ( (gctx.ss[i].UsedBy() == "") ||
                (gctx.ss[i].Module() == f.FromMod) ||
                (gctx.ss[i].MainNet() == f.FromMod) ) &&
                (gctx.ss[i].UsedBy() != f.Hdr.DepaRPT) {

                gctx.ss[i].SetUsedBy(f.Hdr.DepaRPT)
                gctx.ss[i].SetLastUsedBy(f.Hdr.DepaRPT)
                gctx.ss[i].FIFOClean()
            }
            gctx.ss[i].UsedByMutexUnlock()
            if gctx.ss[i].UsedBy() == f.Hdr.DepaRPT {
                //gctx.ss[i].Dch <- f
                //fmt.Printf("frame in\n")
                gctx.ss[i].FIFOPush(f)
                gctx.ss[i].SetUsedByTime(time.Now())
            }
            break
        }
    }
}

func (gctx *GenericCtx) SetMain(mod,main string) {
    for i, _ := range gctx.ss {
        gctx.ss[i].SetModule(gctx.ss[i].GetModule())
        if gctx.ss[i].Module() == mod {
            gctx.ss[i].SetMainNet(main)
            break
        }
    }
}

func (gctx *GenericCtx) GetLastUsedBy(mod string) string {
    for i, _ := range gctx.ss {
        gctx.ss[i].SetModule(gctx.ss[i].GetModule())
        if gctx.ss[i].Module() == mod {
            return gctx.ss[i].LastUsedBy()
        }
    }
    return ""
}

func (gctx *GenericCtx) StartReceiving(exd chan<- *dstar.DSXFrame) {
    for i, _ := range gctx.ss {
        gctx.ss[i].StartReadLoops(exd)
    }
}

func (gctx *GenericCtx) PrepareTransmission() {
    for i, _ := range gctx.ss {
        gctx.ss[i].StartWriteLoops()
    }
}

func (gctx *GenericCtx) Close() {
    for _, dev := range gctx.ss {
        dev.Close()
    }
    gctx.ctx.Close()
}

func (gctx *GenericCtx) SetSs(toset []IDevice) {
    gctx.ss = toset
}

