
package confbox

import (
    "fmt"
    "os"
    "encoding/json"
    "bytes"
)

type ConfBox struct {
    ConfPath string           // path to conf file (used for write conf)
    CONF_SERVER_STRING string // addr:port or :port for conf server
    LOG string                // where to log: stdout or /path/to/file
    ModulesOwner string       // Owner of local modules
    /* Satoshi */
    SATOSHI_MODULES string    // 0xaddr1:A:D,0xaddr2:B:S,.. (addr:mod:Dup/Simp)
    SATOSHI_2THREADS string   // Use 2 threads to read hdr/data (no is better)
    SatoshiDebug string
    /* DVRPTRV2 */
    DVRPTRV2_MODULES string   // 1.1.1.1:23:A:D:CALLSIGN:INVERT:MODLVL:TXD,..
    DVrptrv2Debug string      //     (host:tcpport:module:Dup/Simp:etc)
    /* DV4MINI */
    DV4MINI_MODULES string    // COMXX:A:Hz:Hz:lv,.. (serial:mod:rxf:txf:pwr)
    Dv4miniDebug string
    /* DStarNetwork devices */
    DSN_MODULES string        // 1.1.1.1:201:202:K,..
    DsnDebug string           //        localport:IP:remoteport:module
    /* MAINS conf */
    LocalMains string         // initial mains for satoshis 
    /* DExtra */
    DEXTRA_SERV_STRING string // addr for DExtra server ("" binds to *)
    DExtraDebug string
    DExtraOwner string        // Owner of DExtra module (useful for reflector)
    DExtraModules string      // Modules available on net (A,B,..)
    DExtraIgnoreCRC string    // should I ignore CRC errors?
    /* DCS */
    DCS_SERV_STRING string
    DCSOwner string
    DCSDebug string
    /* General */
    Links string              // initial links (A:B:C,D:N,..)
    AUTOCONNECT string       // connect on boot (REF:RemMod:LocMod,..)
    RouteDebug string
}

// FIXME confpath?
var defConf = []byte(`
{
    "CONF_SERVER_STRING":"127.0.0.1:3333",
    "LOG":"stdout",
    "ModulesOwner":"XX0XXX",
    "SATOSHI_MODULES":"0x0300:A:S,0x0301:B:D",
    "SATOSHI_2THREADS": "no",
    "SatoshiDebug":"no",
    "DVRPTRV2_MODULES": "1.1.1.1:23:C:S:XX0XXX:N:18:80",
    "DVrptrv2Debug": "no",
    "DV4MINI_MODULES": "/dev/cu.usbmodem1411:B:430000000:430000000:1",
    "Dv4miniDebug": "no",
    "DSN_MODULES": "20010:2.2.2.2:20011:B",
    "DsnDebug": "no",
    "LocalMains": "B:N",
    "DEXTRA_SERV_STRING":"",
    "DExtraDebug":"no",
    "DExtraOwner":"XX0XXX",
    "DExtraModules":"A",
    "DExtraIgnoreCRC": "yes",
    "DCS_SERV_STRING":"",
    "DCSOwner":"XX0XXX",
    "DCSDebug":"no",
    "Links":"B:N:P",
    "AUTOCONNECT":"XRF069:B:B",
    "RouteDebug":"no"
}
`)

func (cb *ConfBox) LoadFS(cs []byte) error {
    err := json.Unmarshal(cs,cb)
    return err
}

func (cb *ConfBox) Load(fname string) error {
    f, err := os.Open(fname)
    if err != nil {
        return err
    }
    defer f.Close()
    bs := 1024
    fi, _ := f.Stat()
    for bs < int(fi.Size()) {
        bs = bs * 2
    }
    buf := make([]byte,bs)
    l, err := f.Read(buf)
    if err != nil {
        return err
    }
    cb.ConfPath = fname
    return cb.LoadFS(buf[:l])
}

func (cb *ConfBox) PrettyConf() ([]byte, error) {
    b, err := json.Marshal(*cb)
    if err != nil {
        return nil, err
    }
    var out bytes.Buffer
    json.Indent(&out,b,"","\t")
    out.Write([]byte("\n"))
    return out.Bytes(), nil
}

func (cb *ConfBox) Save(fname string) error {
    f, err := os.Open(fname)
    if err == nil {
        f.Close()
        os.Rename(fname, fname + "~")
    }
    f, err = os.OpenFile(fname, os.O_CREATE|os.O_WRONLY,0644)
    if err != nil {
        return err
    }
    defer f.Close()

    b, err := cb.PrettyConf()
    if err != nil {
        return err
    }
    _, err = f.Write(b)
    if err != nil {
        return err
    }
    return nil
}

func (cb *ConfBox) WriteSample(fname string) {
    err := cb.LoadFS(defConf)
    if err != nil {
        fmt.Println(err)
    }
    cb.Save(fname)
}

