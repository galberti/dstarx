#!/usr/bin/env python

import socket, threading, time, random, sys, string

USAGE = """
%s -h <host> -w <owner> [-r <remote>] [-f <from_module>] [-t <to_module>] [-d]
"""

HELP = """
co    : connect
de    : toggle debug
di    : disconnect
h or ?: help
tx    : tx
txq   : stop tx
q     : quit
"""

ANSI_COLOR_RED = "\x1b[31m"
ANSI_COLOR_GREEN = "\x1b[32m"
ANSI_COLOR_YELLOW = "\x1b[33m"
ANSI_COLOR_RESET = "\x1b[0m"

DEXTRA_LINKACK_SIZE = 14
DEXTRA_HEARTBT_SIZE = 9
DEXTRA_HEADER_SIZE = 56
DEXTRA_FRAMEP_SIZE = 27

DEXTRA_PORT = 30001

class XRFClient(threading.Thread):
    def __init__(self,dbg=False):
        threading.Thread.__init__(self, target=self.main_loop)
        self.running = True
        self.send = False
        self.s = None
        self.dbg = dbg
        self.dst = None
        self.owner = None
        self.to = None
        self.frm = None
        self.nextco = 0

    def __dump(self, data):
        l = 0
        for i in data:
            if (l % 0x10) == 0:
                print "0x%02x:" % l,
            print "%02x" % ord(i),
            if (((l + 1) % 0x10) == 0) or ((l + 1) == len(data)):
                if (l != 0):
                    print "   " + "   "*(0x10 - (l % 0x10)),
                    if ((l + 1) == len(data)):
                        st = l - ((l + 1) % 0x10) + 1
                    else:
                        st = l - 16 + 1
                    for j in range(st, l + 1):
                        if (data[j] in string.letters) or \
                            (data[j] in string.punctuation) or \
                            (data[j] in string.digits):
                            sys.stdout.write(data[j])
                        else:
                            sys.stdout.write(".")
                    print
            l += 1

    def __log(self, out, descr, what, data):
        if out: d = "->"
        else: d = "<-"
        print "%s [%s]%s" % (d, descr, what)
        if self.dbg:
            self.__dump(data)
        
    #def connect(self,host,remote,to,owner,frm='B'):
    def connect(self,host,to,owner,frm='B',remote=""):
        self.to = to
        self.owner = owner
        self.frm = frm
        self.host = host
        if remote == "":
            self.remote = host
        else:
            self.remote = remote
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.s.settimeout(0.001)
        data = "%-08s%s%s\x00" % (owner[:8], frm[0:1], to[0:1])
        try:
            self.dst = socket.gethostbyname(host)
        except socket.gaierror:
            print "Host not found"
            return False
        self.s.sendto(data, (self.dst, DEXTRA_PORT))
        self.__log(True, "CNNCT", "%s:%s -> %s:%s" % \
            (owner, frm, remote, to), data)
        return True

    def disconnect(self):
        if self.dst == None:
            return
        data = "%-08s%s \x00" % (self.owner[:8], self.frm[0:1])
        self.s.sendto(data, (self.dst, DEXTRA_PORT))
        self.dst = None
        self.__log(True, "DISCN", "%s:%s -> %s" % \
            (self.owner, self.frm, self.remote), data)

    def toggle_debug(self):
        if self.dbg: self.dbg = False
        else: self.dbg = True

    def __send_silence(self, loss):
        # prepare dxtra header
        # band2 and band3: frommod 'A'..'D' [1] .. [4]
        to = ord(self.to) - 64
        frm = ord(self.frm) - 64
        sess1 = 0xde
        sess2 = 0xad
        dxhdr = "DSVT%c%c%c%c%c%c%c%c%c%c%c" % \
            (0x10,0x00,0x00,0x00,0x20,0x00,to,frm,sess1,sess2,0x80)
        # prepare dstar header
        #dshdr = "%c%c%c%-07s%c%-07sG%-08s%-08s%-04s%c%c" % \
        #    (0x00,0x00,0x00,self.remote,self.to,self.remote,"CQCQCQ",self.owner,
        #    "TEST",0x00,0x00)
        dshdr = "%c%c%c%-07s%c%-07sG%-08s%-08s%-04s%c%c" % \
            (0x00,0x00,0x00,self.remote,self.to,self.remote,"IT",self.owner,
            "TEST",0x00,0x00)
        # prepare dstar voice packets
        p = "\x4e\x8d\x32\x88\x26\x1a\x3f\x61\xe8"
        sync = "\x55\x2d\x16"
        term = "\x55\x55\x55\x55\xc8\x7a"
        # FIXME fix session and crc
        # FIXME no gps this is always the last part
        scrambler_slow = "\x70\x4f\x93"
        fixed_slow = "@ABC"
        fillup = "\x16\x29\xf5"
        packets = [p + sync]
        for i in range(20):
            packets.append(p + scrambler_slow)
        end1 = p + term[:3]
        end2 = term[3:] + '\x00'*9 # *4 + "\x25\x1a\xc6" ??
        # send dxtra header
        data = dxhdr + dshdr
        self.s.sendto(data, (self.dst, DEXTRA_PORT))
        self.__log(True, "HEADR", "", data)

        # send packet with proper counts
        c = 0
        thr = loss/100
        while self.send:
            data = dxhdr[:-1] + "%c" % c + packets[c]
            if random.random() > thr:
                self.s.sendto(data, (self.dst, DEXTRA_PORT))
                self.__log(True, "VOICE", "", data)
            c = c + 1
            if c == 0x15:
                c = 0
            time.sleep(0.02)
        data = dxhdr[:-1] + "%c" % c + end1
        self.s.sendto(data, (self.dst, DEXTRA_PORT))
        self.__log(True, "VOICE", "", data)
        c = c + 1
        if c == 0x15:
            c = 0
        c |= 0x40
        data = dxhdr[:-1] + "%c" % c + end2
        self.s.sendto(data, (self.dst, DEXTRA_PORT))
        self.__log(True, "VOICE", "", data)

    def send_silence(self, loss=0):
        if self.send:
            return
        self.send = True
        t = threading.Thread(target=self.__send_silence, args=(loss,))
        t.start()

    def stop_sending(self):
        self.send = False

    def send_timed_silence(self, loss=0):
        pass

    def send_heartbeat(self):
        data = "%-08s\x00" % self.owner[:8]
        self.s.sendto(data, (self.dst, DEXTRA_PORT))
        if self.dbg:
            self.__log(True, "HRTBT", "", data)

    def parse_dextra_header(self, data):
        pre = data[0:4]
        if pre != "DSVT":
            self.__log(False, "[ERROR]", "PRE is wrong", data)
        if ord(data[4]) == 0x10:
            typ = "HDR"
        else:
            typ = "VOD"
        flagx1, flagx2, flagx3, flagx4 = data[5], data[6], data[7], data[8]
        bands1, bands2, bands3 = data[9], data[10], data[11]
        session1, session2 = data[12], data[13] 
        count = data[14]
        o = "[TY %s][FL %02x%02x%02x%02x]" % \
            (typ, ord(flagx1), ord(flagx2), ord(flagx3), ord(flagx4))
        o += "[BA %02x%02x%02x]" % \
            (ord(bands1), ord(bands2), ord(bands3))
        o += "[SE %02x%02x]" % (ord(session1), ord(session2))
        co = ord(count)
        o += "[CO %02x]" % co
        if (co & 0x1f) != self.nextco:
            o += "%s[ERR]%s" % (ANSI_COLOR_RED, ANSI_COLOR_RESET)
        if co == 0x80:
            o += "[HDR]"
            self.nextco = 0x00
        elif (co & 0x40) == 0x40:
            o += "%s[END]%s" % (ANSI_COLOR_GREEN, ANSI_COLOR_RESET)
        else:
            self.nextco = co + 1
            if self.nextco == 0x15:
                self.nextco = 0x00
        return o

    def parse(self, data):
        l = len(data)
        if l == DEXTRA_LINKACK_SIZE:
            if data[10:13] == 'ACK':
                self.__log(False, "LKACK", "", data)
            else:
                self.__log(False, "LKNAK", "", data)
                self.dst = None
        elif l == DEXTRA_HEARTBT_SIZE:
            if self.dbg:
                self.__log(False, "HRTBT", "", data)
        elif l == DEXTRA_HEADER_SIZE:
            # DEXTRA HDR
            o = self.parse_dextra_header(data)
            # DSTAR HDR
            flag1, flag2, flag3 = data[15], data[16], data[17]
            DestRPT = data[18:26]
            DepaRPT = data[26:34]
            YoCall = data[34:42]
            MyCall1 = data[42:50]
            MyCall2 = data[50:54]
            o += "\n     [DSHDR][FL %02d%02d%02d]" % \
                (ord(flag1), ord(flag2), ord(flag3))
            o += "[DST %s][DEP %s][YO %s]\n       [MY1 %s][MY2 %s]" % \
                (DestRPT, DepaRPT, YoCall, MyCall1, MyCall2)
            self.__log(False, "DXHDR", o, data)
            # FIXME check crc
            crc = data[54:56]
        elif l == DEXTRA_FRAMEP_SIZE:
            # DEXTRA HDR
            o = self.parse_dextra_header(data)
            vo = "%02x%02x%02x%02x%02x%02x%02x%02x%02x" % \
                tuple(map(lambda x: ord(x),data[15:24]))
            xtra = "%02x%02x%02x" % tuple(map(lambda x: ord(x),data[24:27]))
            o += "\n     [%s][%s]" % (vo, xtra)
            if xtra == "552d16":
                o += "%s[SYNC]%s" % (ANSI_COLOR_GREEN, ANSI_COLOR_RESET)
            elif xtra == "555555":
                o += "%s[TERM1]%s" % (ANSI_COLOR_GREEN, ANSI_COLOR_RESET)
            if vo[:6] == "55c87a":
                o += "%s[TERM2]%s" % (ANSI_COLOR_GREEN, ANSI_COLOR_RESET)
            if vo == "4e8d3288261a3f61e8":
                o += "%s[SILENCE]%s" % (ANSI_COLOR_YELLOW, ANSI_COLOR_RESET)
            # DSTAR HDR
            self.__log(False, "DXVOI", o, data)

    def stop(self):
        self.running = False

    def main_loop(self):
        ctr = 0
        while self.running:
            try:
                data, addr = self.s.recvfrom(1024)
                self.parse(data)
                raise socket.timeout
            except socket.timeout:
                if ((ctr + 5) <= time.time()) and self.dst:
                    self.send_heartbeat()
                    ctr = time.time()

def main(args):
    host = None
    owner = None
    frm = 'A'
    to = 'B'
    dbg = False
    remote = None
    while True:
        try:
            fl = args.pop(0)
        except IndexError:
            break
        if fl == '-h':
            try:
                host = args.pop(0)
            except IndexError:
                print "Missing argument to -h"
                return 1
        elif fl == '-w':
            try:
                owner = args.pop(0).upper()
            except IndexError:
                print "Missing argument to -w"
                return 1
        elif fl == '-f':
            try:
                frm = args.pop(0).upper()
            except IndexError:
                print "Missing argument to -f"
                return 1
        elif fl == '-t':
            try:
                to = args.pop(0).upper()
            except IndexError:
                print "Missing argument to -t"
                return 1
        elif fl == '-r':
            try:
                remote = args.pop(0).upper()
            except IndexError:
                print "Missing argument to -r"
                return 1
        elif fl == '-d':
            dbg = True

    if host == None:
        print "host is necessary."
        return 1
    elif owner == None:
        print "owner is necessary."
        return 1

    if remote == None:
        remote = host

    c = XRFClient(dbg)
    if not c.connect(host, to, owner, frm=frm, remote=remote):
        return
    c.start()
    while True:
        try:
            command = raw_input("> ")
            cs = command.split()
            if (len(cs) == 0) or (cs[0] == 'h') or (cs[0] == '?'):
                print HELP
            elif cs[0] == 'co':
                c.connect(host, to, owner, frm, remote=remote)
            elif cs[0] == 'de':
                c.toggle_debug()
            elif cs[0] == 'di':
                c.disconnect()
            elif cs[0] == 'tx':
                try:
                    loss = float(cs[1])
                    if (loss < 0) or (loss > 100):
                        print "loss is a percentage.. ignored."
                        raise IndexError
                except IndexError:
                    loss = 0
                print "TX, loss=%.2f" % loss
                c.send_silence(loss)
            elif cs[0] == 'txq':
                c.stop_sending()
            elif cs[0] == 'q':
                c.stop_sending()
                time.sleep(0.1)
                raise EOFError
            else:
                print "..what?"
        except KeyboardInterrupt:
            print
        except EOFError:
            c.stop()
            print "OK, exiting.."
            c.join()
            break

if __name__ == '__main__':
    import sys
    if len(sys.argv) == 1:
        print USAGE % sys.argv[0]
        sys.exit(1)
    main(sys.argv[1:])

